'use strict';
var Cmd = require('../src/commands').cmd,
Room = require('../src/rooms').room,
Character = require('../src/character').character,
World = require('../src/world').world;

module.exports = {
	name: 'Glowscale Waters',
	id: 'bayou',
	type: 'rural',
	levels: 'All',
	description: 'The swamps of Glowscale Waters.',
	reloads: 0,
	created: '',
	saved: '',
	author: 'Blue',
	messages: [
		{msg: 'Dragonflies hover over the river waters.'},
		{msg: 'The glowing laterns of the streets make you feel dizzy and lost.'}
	],
	respawnOn: 8,
	rooms: [
		{
			id: '1',
			title: 'Glowscale Docks',
			light: true,
			area: 'bayou',
			content: 'You are standing on a river port. The heat and humidity of this land is like nothing you have ever felt. You are dripping with moisture. A wide river winds off into the distance, and the land around you is cramped with trees. The air hums with the buzz of insects.',
			outdoors: false,
			exits: [
				{
					cmd: 'west',
					id: '2'
				}
			],
			events: [{
			    "id": "My Puppet Mon Amie",
			    "moveMod": 0,
			    "description": 'The river is bare of boats and crafts right now, and the sun is weakly casting the last rays of sunset over the water. You are despairing about how you are ever going to get out of here, when you realise there is someone standing next to you. You jump in surprise and step back, then look up to the creature. It\'s an enormously tall alligator, his belly a huge round ball so big it could hold a horse. His scales are a deep, dark green that is almost black, but they are coated in stripes of paint that glows a strange and shimmering pink and green in the darkness. He must weight over a ton, yet he moved so quietly that you didn\'t notice him at all. </p>He brushes his waistcoat, and dips his top hat politely. "Mon chéri, I thought I heard the lamentations of the lost. It calls to me, you see", he says. His voice is smooth and friendly, not at all what you would expect from such a terrifying monster of a reptile. "You are lost, and looking to travel. I can tell from your eyes, ma petite. Alas, chacun voit midi à sa porte. If you help me, I will help you, yes? I have much work that needs doing as I travel north. Perhaps you could join me?"</p>You don\'t see many other options, but something about the gator gives you a feeling of supernatural dread. Do you take him up on his offer?',
			    "repeatable": true,
			    "effect": function(player) {
			    	player.confirmStep = {
		    			yes: 'The gator smiles when you agree. It is a slow smile, and it takes a very long time for his muzzle to stretch all the way down the many rows of his enormous teeth. "I am so glad we could come to an arrangement. Now, look into my eyes. Un petit peu plus...oiu, just like that", he says. You don\'t remember actually lifting your head to look into his eyes, but you are definitely doing so. They are beautiful eyes. Glowing rings of fluorescent pink and green, pulsing and swirling in an endless swirl of colour and feeling. The rings of light seem to be washing over you, almost like they are surrounding you and binding you up. Your body moves forward, and you seem to be falling, falling. Deeper and deeper into that light, the rings forming a tunnel for you to fall forever. </p>You are distantly aware that you are walking along the docks. You can feel yourself lifting boxes and placing them in a long boat that is painted in those same glowing colours. There are other people too, moving with jerky unnatural movements as they help to load up the cargo. Your body strains with the weight, and is exhausted by the amount of cargo that needs to be hauled and tied down, but it\'s a very distant feeling. It\'s like you are just watching these events on a screen, not like you are really the ones doing them. Even when you find yourself tying up struggling people, locking them in chains, and forcing them onto the boat, you can\'t do anything about it. You are trapped inside your own head, watching a movie of these events, and the projectionist is that unstoppable, glowing alligator.</p>Once all the cargo is loaded, you step onto the boat, and feel it rock in the water. You and the other workers line up in a row, and suddenly the alligator is there. He looks over you all.</p>"Oui oui, now I did say I would take you all north. And I promise I will. Every one of you will be there when I step off this boat." he says, before walking along the row of stiff, entranced workers. He stops just in front of you, then opens his enormous maw, and slowly slides it down over the worker ahead of you in line. The human stands obediently and lifelessly as the alligator swallows him up, before the gator lifts his head up and gulps down the human\'s legs. He burps, and says "More or less." You cannot even yell or react. Your body is totally under his control. </p></p>For days, you work on the boat, cleaning and washing the deck, checking the sails, dusting the rooms. Each day, the alligator lines you all up, and devours another worker whole. Every time it happens, you know you have a higher chance of being next. By the end of the week, it is only you and one other person left. The gator\'s belly has become enormous from his many live meals, and it jiggles and bounces when he walks. The boat docks at a small port, and the alligator makes you both unload the cargo and tie it up. Your body is aching all over from having to do the work with just the two of you. You are sure that if you weren\'t being manipulated like a puppet, you would collapse. </p>When it is done, the alligator stands you both before him, and looks down at you with his gleaming eyes. From one to the other, grinning as he chooses...you. His scaly finger points to you, and he sits down, drawing you closer...before he says "Rub my belly, s\'il vous plaît."</p>Your body leans forward, and you rub and stroke his heavy gut. You are staring only at his soft belly scales, so you don\'t see him eating the other worker. You can hear it though, and feel the man\'s struggles as he goes down. The gator chuckles, and ruffles your hair. "Travel north for a day and night, mon chéri. There you will find your next stop, and there I will free you from your bindings. Until next we meet..." he whispers. You stand, turn, and march like a zombie up the road, leaving the gator behind. Only hours later, when you finally collapse in front of a small dusty town, do you regain control of yourself. You shiver for a moment, resolving never again to encounter that nightmarish reptile.',
		    			no: 'The gator shakes his head sadly. "You will find it difficult to leave here without me, mon chéri. Still, il n’y a pas plus sourd que celui qui ne veut pas entendre. Well, the best of luck to you. Au revoir", he says. You blink, and the gator is gone. Into the water, perhaps? He moves quicker and more quietly than seems possible.',
		    			yesEffect: function(player) {
		    				var roomObj = World.getRoomObject(player.area, player.roomid);
		    				
		    				var respawnRoom = World.getRoomObject('frontier', '1');
		    				
		    				Room.removePlayer(roomObj, player);
		    			
		    				player.position = 'standing';
		    				
		    				respawnRoom.playersInRoom.push(player);

		    				player.roomid = respawnRoom.id;
		    				player.area = respawnRoom.area;
		    			}
			    	};
			    }
			}],
			playersInRoom: [],
			monsters: [],
			items: [],
			beforeEnter: function(roomObj, fromRoom, target) {
				return true;
			},
			onEnter: function(roomObj, target) {
				
			}
		},{
			id: '2',
			title: 'Glowscale Streets',
			light: true,
			area: 'savannah',
			content: 'These streets twist like snakes between tall and dark houses. The only light comes from the multicoloured lanterns that dangle from the eaves.',
			outdoors: false,
			exits: [
				{
					cmd: 'east',
					id: '1'
				}
			],
			events: [],
			playersInRoom: [],
			monsters: [],
			items: [],
			beforeEnter: function(roomObj, fromRoom, target, player) {
				World.msgPlayer(player, {
					msg: 'At the end of the port, you can see colourful lanterns casting light of many colours across the streets of the town. This is Glowscale Waters, home of alligators, zombies, and strange magic. When you try to walk into town, however, you somehow keep getting lost amongst the narrow streets, and you always end up back at the docks.',
					styleClass: 'error'
				});
				return false;
			},
			onEnter: function(roomObj, target) {
				
			}
		}]
};

