'use strict';
var Cmd = require('../src/commands').cmd,
Room = require('../src/rooms').room,
Character = require('../src/character').character,
World = require('../src/world').world;

module.exports = {
	name: 'Overworld',
	id: 'overworld',
	type: 'macro',
	levels: 'All',
	description: 'The world as seen by giants.',
	reloads: 0,
	created: '',
	saved: '',
	author: 'Blue',
	messages: [
		{msg: 'The creatures beneath you look like ants.'},
		{msg: 'A cloud wafts against your cheek.'}
	],
	respawnOn: 8,
	rooms: [
		{
			id: '1',
			title: 'Far Above Dormaus',
			light: true,
			area: 'overworld',
			content: 'The town of Dormaus is just a tiny cluster of little colours around your feet and toes. It would be so easy to just lift one foot and crush the entire town in a single step. For some reason, however, you find yourself unwilling to do so.',
			outdoors: false,
			overworld: {"area": "dormaus", "roomid": "1"},
			exits: [
				{
					cmd: 'south',
					id: '2'
				}
			],
			events: [],
			playersInRoom: [],
			monsters: [],
			items: [],
			beforeEnter: function(roomObj, fromRoom, target) {
				return true;
			},
			onEnter: function(roomObj, target) {
				
			}
		},
		{
			id: '2',
			title: 'Above A Farm',
			area: 'overworld',
			light: true,
			overworld: {"area": "farm", "roomid": "1"},
			content: 'There is a tiny little farm beneath your feet. Your footprints leave massive grooves in the tiny fields, and animals scurry around your toes.',
			outdoors: false,
			exits: [
				{
					cmd: 'north',
					id: '1'
				}
			],
			playersInRoom: [],
			monsters: [],
			items: []
		},
		{
			id: '3',
			title: 'Inside a Belly',
			area: 'overworld',
			light: true,
			content: 'You are trapped inside a warm, dark belly. You have been devoured by some beast, and if you do not escape, you will surely be added to their belly fat forever.',
			outdoors: false,
			exits: [
			],
			playersInRoom: [],
			monsters: [],
			events: [{
			    "id": "Digestion",
			    "moveMod": 0,
			    "description": 'You struggle around inside your captor\'s belly, trying to escape your fate of becoming dinner. The longer you spend in here, the harder it is to keep fighting. It\'s just so comfortable, and warm. It would be so easy to fall asleep, and just become chub forever. Do you keep struggling?',
			    "repeatable": true,
			    "effect": function(player) {
			    	player.confirmStep = {
		    			yes: 'You can\'t just turn into blubber! You keep kicking and fighting, and you hear your predator gurgling and grumbling. You give one last kick as strong as you can, and you feel yourself being spat back up, until you land with a thump in a puddle of saliva back in the town of Dormaus.',
		    			no: 'Would it be so bad to be food? To give nutrition and nourishment to the big handsome beast that devoured you? Your struggles weaken as you start to lie back and relax. You can feel your body becoming soft and malleable as you are gradually absorbed. You close your eyes and drift off, the belly around you gurgling and squishing as it sucks you up, draining everything away from you. You feel the body of the predator all around you as you become a part of him, nothing but an extra layer of fat on his body.',
		    			yesEffect: function(player) {
		    				var roomObj = World.getRoomObject(player.area, player.roomid);
		    				
		    				var respawnRoom = World.getRoomObject(player.recall.area, player.recall.roomid);
		    				
		    				Room.removePlayer(roomObj, player);
		    			
		    				player.position = 'standing';
		    				
		    				respawnRoom.playersInRoom.push(player);

		    				player.roomid = respawnRoom.id;
		    				player.area = respawnRoom.area;
		    				player.chp = 1;
		    				player.cmana = 1;
		    				player.cmv = 7;
		    			},
		    			noEffect: function(player) {
		    				player.description = "The remains of a digested " + player.sex + ' ' + player.race + ' once called ' + player.name + ' is here inside this stomach.';
		    				player.trapped = "You were eaten and digested, and are now just a layer of blubber on " + player.predator + "\'s belly.";
		    			}
			    	};
			    }
			}],
			items: []
		}
	]
};

