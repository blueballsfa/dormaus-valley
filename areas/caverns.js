'use strict';
var Cmd = require('../src/commands').cmd,
Room = require('../src/rooms').room,
Character = require('../src/character').character,
World = require('../src/world').world;

module.exports = {
	name: 'Geo Caverns',
	id: 'caverns',
	type: 'cave',
	levels: 'All',
	description: 'The southern caves.',
	reloads: 0,
	created: '',
	saved: '',
	author: 'Blue',
	messages: [
		{msg: 'The sound of dripping water echoes through the stony caverns.'},
		{msg: 'Glowing rocks shine in the walls for a moment, then fade again.'}
	],
	respawnOn: 8,
	rooms: [
		{
			id: '1',
			title: 'Cave Entrance',
			light: true,
			area: 'caverns',
			content: 'The light from outside barely reaches into the darkness of these caverns. From deep within, the sound of water echoes, and the air is as cold as ice.',
			outdoors: false,
			exits: [
				{
					cmd: 'south',
					id: '3'
				},{
					cmd: 'east',
					id: '2'
				},{
					cmd: 'west',
					id: '3',
					area: 'farm'
				}
			],
			events: [{
			    "id": "Slime TF F",
			    "moveMod": 0,
			    "description": 'Something drips from the ceiling, making a quiet plop sound. You turn to look, and for a moment see the shine of something green. There is another drip, and you look for the source. You find a strange sight – a patch of glistening, thick green slime, clinging to the ceiling. It shines in the dim light, and smells very odd. A sort of musky, sexual scent, like very thick cum. Do you get closer?',
			    "repeatable": true,
			    "valid": function(player, command, fn) {
			    	if (player.race != "slime" && player.sex == "female") {
			    		return true;
			    	} else {
			    		return false;
			    	}
			    },
			    "effect": function(player) {
			    	player.confirmStep = {
		    			yes: 'You walk closer, and reach your hand up to gently touch the bulbous, thick part of the slime at the center. It is very soft and sticky, and it oozes along your finger. You withdraw it, and the slime stretches when you pull your hand back. When you shake it, hoping that it will fall off, it instead just spreads further, slowly moving along your hand and engulfing it completely in shiny green slime.</p>You start to feel concerned, and try to wipe the slime off with your other hand, but it just spreads to there too. It feels weird on your skin, like its clinging to you and growing warm. When you flex your hands, it covers your fingers perfectly, like your whole hand is made of green goo. In fact, you think you can see through your own palm! With trembling hesitation, you slowly push your finger against your hand. Slowly, your goo finger moves through your flesh, and out the other size. Your hand is completely slime!</p>All the while, the slime has been crawling up your arms. You feel wet and messy, and you shudder as it starts to cover your chest. The sexual stink of the slime is starting to turn you on, but your pussy is acting oddly. Instead of getting warm and wet, it\'s just leaking enormous amounts of green goo. It spreads and coats your body, and becomes a sensitive, slippery hole inside you, desperate to be filled with slimy messy things. It feels strangely much more sensitive and pleasurable than your old one!</p>You start to drool, your saliva thick and salty and musky. It\'s soaking your chin and chest. You feel something like butterflies in your stomach, as your insides start to melt and turn to goo. Your heartbeat stops, your lungs no longer need to breathe. Your chest is a big pear-shaped shiny ball of slime. Your legs feel weak and wobbly, and they start to sink and melt, turning into an indistinct blob. You twist and adjust yourself, rising up on the blob in a shape sort of like a slug with a goopy human torso atop it.</p>The slime from your mouth starts to crawl around your head, sinking into your flesh, covering you completely. You close your eyes, and when you reopen them, everything is slightly green-tinted. Your feel two goo antennae pop and grow from the top of your head, and you grin a dumb, happy smile as your brain becomes just more sticky smelly happy slime.',
		    			no: 'You wisely leave the slime to its simple existence.',
		    			yesEffect: function(player) {
		    				player.description = 'This adventurer is a huge slime creature. Sort of a slime-slug-centaur, they have a sluglike lower body made of green goo, with a humanoid torso atop it, made completely of see-through slime. Their head has two antennae, and they have a stupid happy grin on their face.';
		    				player.race = "slime";
		    				player.size.value = 3;
		    			}
			    	};
			    }
			},{
			    "id": "Slime TF M",
			    "moveMod": 0,
			    "description": 'Something drips from the ceiling, making a quiet plop sound. You turn to look, and for a moment see the shine of something green. There is another drip, and you look for the source. You find a strange sight – a patch of glistening, thick green slime, clinging to the ceiling. It shines in the dim light, and smells very odd. A sort of musky, sexual scent, like very thick cum. Do you get closer?',
			    "repeatable": true,
			    "valid": function(player, command, fn) {
			    	if (player.race != "slime" && player.sex == "male") {
			    		return true;
			    	} else {
			    		return false;
			    	}
			    },
			    "effect": function(player) {
			    	player.confirmStep = {
		    			yes: 'You walk closer, and reach your hand up to gently touch the bulbous, thick part of the slime at the center. It is very soft and sticky, and it oozes along your finger. You withdraw it, and the slime stretches when you pull your hand back. When you shake it, hoping that it will fall off, it instead just spreads further, slowly moving along your hand and engulfing it completely in shiny green slime.</p>You start to feel concerned, and try to wipe the slime off with your other hand, but it just spreads to there too. It feels weird on your skin, like its clinging to you and growing warm. When you flex your hands, it covers your fingers perfectly, like your whole hand is made of green goo. In fact, you think you can see through your own palm! With trembling hesitation, you slowly push your finger against your hand. Slowly, your goo finger moves through your flesh, and out the other size. Your hand is completely slime!</p>All the while, the slime has been crawling up your arms. You feel wet and messy, and you shudder as it starts to cover your chest. The sexual stink of the slime is starting to turn you on, but your cock is acting oddly. Instead of getting hard, it\'s just leaking enormous amounts of pre. You look down and smear the pre, only to see that it is thick, green slime. Your cock shrinks as it gushes slime, but the slime starts to stand up and rise into a glistening, bouncy green pillar. You squeeze the strange goo-cock, and gasp with pleasure.</p>It feels so much better than your old dick did! As you pump it, your human dick melts away into the slime. </p>You start to drool, your saliva thick and salty and musky. It\'s soaking your chin and chest. You feel something like butterflies in your stomach, as your insides start to melt and turn to goo. Your heartbeat stops, your lungs no longer need to breathe. Your chest is a big pear-shaped shiny ball of slime. Your legs feel weak and wobbly, and they start to sink and melt, turning into an indistinct blob. You twist and adjust yourself, rising up on the blob in a shape sort of like a slug with a goopy human torso atop it.</p>The slime from your mouth starts to crawl around your head, sinking into your flesh, covering you completely. You close your eyes, and when you reopen them, everything is slightly green-tinted. Your feel two goo antennae pop and grow from the top of your head, and you grin a dumb, happy smile as your brain becomes just more sticky smelly happy slime.',
		    			no: 'You wisely leave the slime to its simple existence.',
		    			yesEffect: function(player) {
		    				player.description = 'This adventurer is a huge slime creature. Sort of a slime-slug-centaur, they have a sluglike lower body made of green goo, with a humanoid torso atop it, made completely of see-through slime. Their head has two antennae, and they have a stupid happy grin on their face.';
		    				player.race = "slime";
		    				player.size.value = 3;
		    			}
			    	};
			    }
			},{
			    "id": "Naga Guest: Male",
			    "moveMod": 0,
			    "description": 'As you explore the cold caverns, you notice a dark crevice not visible from the entrance. When you try to focus your eyes to look inside, you see a flash of something almost golden and feel an intense desire to walk into the darkness. Do you follow your instincts and walk into the dark? (Guest event from CJMPinger)',
			    "repeatable": true,
			    "valid": function(player, command, fn) {
			    	if (player.race == "naga" || player.sex == "female") {
			    		return false;
			    	} else {
			    		return true;
			    	}
			    },
			    "effect": function(player) {
			    	player.confirmStep = {
		    			yes: 'You start stepping forward into the cave almost mechanically, your legs moving on their own. When you get about halfway, you freeze. You stand there stiff as a board. Out of the darkness, slithers a large serpentine creature. As soon as you see it you feel an immense calmness. The serpent like creature in front of you resembles a cobra with black, almost light absorbing scales, and brightly reflective golden scales adorning into a curving, complex pattern flowing from the wide hood down to the end of the long body. In all ways it resembles an extremely large snake, except its “top half” which resembles a muscular human torso with large muscular arms. Below the torso, emerging from a scaly slit, are two large reptilian erections, each dripping with precum. But the only thing you truly notice is his enrapturing eyes. They are reptilian, with specks of gold, black, and purple around the slitted iris. You began to lose yourself in the depths of those eyes.</p>As you stare blankly into his eyes, a voice begins to speak in your head. “You are sssafe with me brother…” Before your mind barely even begins to comprehend what you heard, the Naga breaks eye contact and moves extremely fast, beginning to wrap you tight in his coils. When you feel the scales slide along your skin, you shudder as intense pleasure courses through you. Inside your mind, you hear more words reinforcing what you feel. “Thisss isss where you truly belong, sssibling. Wrapped sssafely in my ssstrong, protective coilsss…” You don’t even feel it when both of his cocks enter your asshole, or when his fangs bite into your shoulder injecting some sort of venom, leaving no pain and only a faint wound.</p>While wrapped in his coils, your body begins to change unawares to you. Your legs start stretching out much longer than anatomically possible. From the bite wound, the skin starts turning into smooth dark scales, the muscles underneath bulking up and becoming stronger. Your head starts reforming into a snake like visage with slitted eyes and a large hood. Below as the scales begins to reach the crotch, your dick, hard with lust, recedes into a newly formed slit. Seconds after it fully receded back a hemipenes slides out, more sensitive than before, reacting to the sliding scales against it with spurts of cum. The last of the changes to fall on your body, as the scales advance along the legs, is the merging of the legs. Slowly the legs blend together, until finally there is only a long prehensile tail covered in smooth scales.</p>While this all happens, the larger Naga continues pumping his hemipenes into you, shifting his coils as needed to let the changes continue. Inside your mind, you hear him speak more and more until your thoughts completely mirror his. “We are good sssiblingsss…” “We are perfect beingsss…” “Perfect ssspecimensss…” After about an hour of the larger Naga rutting inside of you, you feel him climax, and inside you feel happy. Warm cum flows out of you, and you lay exhausted upon the ground. The last thing you see before you fall asleep, is the hooded head of the other Naga looking down at you, smiling.</p>Hours later you awake, in the same cave before, but instead alone and cold in the dark. Your thoughts are back to being your own, but deep within you, you feel a loneliness of your brother abandoning you. The only signs that he was here that remained was the mess of dried, sticky cum on you and the ground, and your new scaly body. You try to lift yourself up and walk, or rather slither, out from this hiding place. It will take time to get used to this new body.',
		    			no: 'You stand there for quite a few minutes, staring into the cave. It takes all of your will to not walk forward into the dark. As you stare, the glittering seems to get closer, almost cautiously. Eventually you hear a loud clang from behind you and you awake from your daze. You immediately turn around and run as far away from the cave as you can till you are out of breath and away from whatever was in those dark depths.',
		    			yesEffect: function(player) {
		    				player.description = "This adventurer is a muscular naga, a powerful human with the lower body of a mighty snake. Their snake half is covered in beautiful, smooth scales.";
		    				player.race = "naga";
		    				Character.resize(player, 3);
		    			}
			    	};
			    }
			},{
			    "id": "Naga Guest: Female",
			    "moveMod": 0,
			    "description": 'As you explore the cold caverns, you notice a dark crevice not visible from the entrance. When you try to focus your eyes to look inside, you see a flash of something almost golden and feel an intense desire to walk into the darkness. Do you follow your instincts and walk into the dark? (Guest event from CJMPinger)',
			    "repeatable": true,
			    "valid": function(player, command, fn) {
			    	if (player.race == "naga" || player.sex == "male") {
			    		return false;
			    	} else {
			    		return true;
			    	}
			    },
			    "effect": function(player) {
			    	player.confirmStep = {
		    			yes: 'You start stepping forward into the cave almost mechanically, your legs moving on their own. When you get about halfway, you freeze. You stand there stiff as a board. Out of the darkness, slithers a large serpentine creature. As soon as you see it you feel an immense calmness. The serpent like creature in front of you resembles a cobra with black, almost light absorbing scales, and brightly reflective golden scales adorning into a curving, complex pattern flowing from the wide hood down to the end of the long body. In all ways it resembles an extremely large snake, except its “top half” which resembles a muscular human torso with large muscular arms. Below the torso, emerging from a scaly slit, are two large reptilian erections, each dripping with precum. But the only thing you truly notice is his enrapturing eyes. They are reptilian, with specks of gold, black, and purple around the slitted iris. You began to lose yourself in the depths of those eyes.</p>As you stare blankly into his eyes, a voice begins to speak in your head. “You are sssafe with me sssissster…” Before your mind barely even begins to comprehend what you heard, the Naga breaks eye contact and moves extremely fast, beginning to wrap you tight in his coils. When you feel the scales slide along your skin, you shudder as intense pleasure courses through you. Inside your mind, you hear more words reinforcing what you feel. “Thisss isss where you truly belong, sssibling. Wrapped sssafely in my ssstrong, protective coilsss…” You don’t even feel it when both of his cocks enter your asshole, or when his fangs bite into your shoulder injecting some sort of venom, leaving no pain and only a faint wound.</p>While wrapped in his coils, your body begins to change unawares to you. Your legs start stretching out much longer than anatomically possible. From the bite wound, the skin starts turning into smooth dark scales, the muscles underneath bulking up and becoming stronger. Your head starts reforming into a snake like visage with slitted eyes and a large hood. Below as the scales begins to reach the crotch, your pussy, dripping with a lust, changes. It becomes thinner and more sensitive, reacting to the scales rubbing against it with cum flowing out onto the ground. The last of the changes to fall on your body, as the scales advance along the legs, is the merging of the legs. Slowly the legs blend together, until finally there is only a long prehensile tail covered in smooth scales.</p>While this all happens, the larger Naga continues pumping his hemipenes into you, shifting his coils as needed to let the changes continue. Inside your mind, you hear him speak more and more until your thoughts completely mirror his. “We are good sssiblingsss…” “We are perfect beingsss…” “Perfect ssspecimensss…” After about an hour of the larger Naga rutting inside of you, you feel him climax, and inside you feel happy. Warm cum flows out of you, and you lay exhausted upon the ground. The last thing you see before you fall asleep, is the hooded head of the other Naga looking down at you, smiling.</p>Hours later you awake, in the same cave before, but instead alone and cold in the dark. Your thoughts are back to being your own, but deep within you, you feel a loneliness of your brother abandoning you. The only signs that he was here that remained was the mess of dried, sticky cum on you and the ground, and your new scaly body. You try to lift yourself up and walk, or rather slither, out from this hiding place. It will take time to get used to this new body.',
		    			no: 'You stand there for quite a few minutes, staring into the cave. It takes all of your will to not walk forward into the dark. As you stare, the glittering seems to get closer, almost cautiously. Eventually you hear a loud clang from behind you and you awake from your daze. You immediately turn around and run as far away from the cave as you can till you are out of breath and away from whatever was in those dark depths.',
		    			yesEffect: function(player) {
		    				player.description = "This adventurer is a muscular naga, a powerful human with the lower body of a mighty snake. Their snake half is covered in beautiful, smooth scales.";
		    				player.race = "naga";
		    				Character.resize(player, 3);
		    			}
			    	};
			    }
			}],
			playersInRoom: [],
			monsters: [],
			items: [],
			beforeEnter: function(roomObj, fromRoom, target, player) {
				if (player.level < 5) {
					World.msgPlayer(player, {
						msg: 'You cannot pass the web of holy symbols, they push you back with a mysterious force. Perhaps if you were more experienced?',
						styleClass: 'error'
					});
					return false
				} else {
					return true;
				}
			},
			onEnter: function(roomObj, target) {
				
			}
		},
		{
			id: '2',
			title: 'Quartz Cavern',
			area: 'caverns',
			light: true,
			content: 'This cavern opens out to reveal thick pieces of quartz sticking out from the rock walls. Glittering white crystals stud the tall ceiling, making what little light that is here refract strangely.',
			outdoors: false,
			exits: [
				{
					cmd: 'east',
					id: '4'
				}, {
					cmd: 'west',
					id: '1'
				}, {
				    cmd: 'south',
				    id: '3'
				}
			],
			playersInRoom: [],
			monsters: [],
			events: [{
			    "id": "Crystal Drone",
			    "moveMod": 0,
			    "description": 'The crystal cavern is strange to stand in. The way the light refracts and bounces from the countless glittering crystals is sort of disorienting. You stare a little longer than you intended to, and when you turn to leave, the exit doesn\'t seem to be where you thought it was. It takes you several minutes to re-orient yourself and find the exit, and by then, you are starting to get distracted by the glimmering crystals again. Do you want to relax and watch the shiny lights?',
			    "repeatable": true,
			    "valid": function(player, command, fn) {
			    	if (player.race == "crystal drone") {
			    		return false;
			    	} else {
			    		return true;
			    	}
			    },
			    "effect": function(player) {
			    	player.confirmStep = {
		    			yes: 'You loosen your guard a little and look around, watching the lights twinkle and shimmer and shine all around you. It\'s quite calming and relaxing, in a strange way. Your breathing starts to grow slower and stiller, as your body feels like it\'s somehow stiffening. Your limbs harden and stay firm in their positions, losing the natural twitching and shifting of a normal body. You feel your feet becoming more comfortable on the rocky and spiky cavern floor. It no longer feels awkward and jagged, but instead feels natural to let your own skin harden and toughen against it.</p>The glimmering lights seem to reflect and bounce from your skin, which itself is starting to gleam and shimmer. Your flesh hardens and toughens, the bends and curves of a normal body starting to be replaced by sharp edges and flat, smooth planes. A glow emerges from deep within you, leaving your skin with a gleaming crystalline green shimmer. You reach towards your chest with unnatural jerky motions, and feel the hard, rocky edges of the shape there. There is no heartbeat or rise of breath. </p>Your torso is changing inside, organs being replaced with more shiny, slightly see-through crystal. You lift your hands and see your fingers lengthen, the tips becoming jagged crystal claws. The last of your skin and flesh is replaced, with a twinkling of lights, and becomes more beautiful crystal. You flex your toes, and feel them grow longer and stronger. Perfect for moving in this rocky cavern, your feet shift into jagged crystal talons.  Your crystal body takes on the suggestion of muscle and strength, becoming thicker in the limbs, and you feel a strange pleasure inside you when your rear starts to grow a new crystal. It makes a strange sound like the edge of a wineglass being touched when it grows, and it thickens and bends with sharp angles as it forms a sort of thick, lizardlike tail.</p>Your body is so cold and still and beautiful. A strange feeling is rising in your chest, one of lifeless, emotionless coldness. It tingles as it moves up into your head, and you feel your mind starting to grow slower and more calculating. Like a silicon crystal computer, your emotions fade away and leave only empathy-free logic. Your face starts to smooth over, your eyes and nose and mouth all turning into a smooth, beak-like crystal with no features or difference at all. Two horn-crystals just from the featureless shape of your head, and you feel satisfied and content with your body. This is how you were meant to be. You are a crystal drone.',
		    			no: 'You rub your eyes and squint, managing to block out enough of the light that you can slowly edge your way to the exit.',
		    			yesEffect: function(player) {
		    				player.description = "This adventurer is a strange creature, made entirely of slightly green, translucent crystal. Their jagged body is shaped roughly like a lizard person, but their head is just a featureless crystal with two large horns and no eyes or facial features at all. When they are not moving, you would swear they are just a lifeless statue.";
		    				player.race = "crystal drone";
		    				player.size.value = 3;
		    			}
			    	};
			    }
			},{
			    "id": "Crystal Prison",
			    "moveMod": 0,
			    "description": 'As you walk past one of the larger white crystals in this room, you swear for a moment you saw a weird reflection, like a person. You turn to look behind you, but there is no one there. When you look back into the crystal and squint, you start to make out what looks like a canine warrior, mouthing silently to you as a reflection in the gleaming crystal. Do you want to try to speak with the ghostly image?',
			    "repeatable": true,
			    "effect": function(player) {
			    	player.confirmStep = {
		    			yes: 'You step closer to the crystal, and try to talk to the person inside. Or on the surface, or whatever they are. The crystal itself seems hollow, yet at just the right angle you can almost make someone out, like an optical illusion. The canine mouths something to you, and you press your hands on the crystal and lean in to see if you can hear him. Suddenly, there is a bizarre sensation of falling that completely overtakes your body. You seem to almost collapse forward, out of your own flesh and down. You tumble through a shimmering, dizzying labyrinth of crystal reflections, and find yourself trapped, unable to move, and staring out into the very cave you were just standing in. Your body is pinned at the sides, encased at all angles, and you cannot move or twitch or even breathe.</p>From what you can see in the cave, your body is still there, your own hands pressed against your crystal prison. But then it blinks, and steps back, and shudders before a grin spreads across its face. "Woah! Fuck, it feels so good to be able to move again! But hey, I used to be a dog! What gives?" it says. Your face is bearing an expression totally unlike you would normally wear, and your voice is a gruff one that sounds like you might imagine that burly dog warrior would sound. </p>You see your body touch its face, stroke between its legs, and whatever spirit possesses it gradually realises what happened. It steps back from the crystal, and grins wider. "Oops! Guess you\'re stuck in my place, friend. Oh well, I\'m sure some other idiot will be along eventually to swap with you!" it says, before quickly leaving this cursed cave.',
		    			no: 'You step away from the crystal, and the reflection or shade or whatever it was disappears from your sight.',
		    			yesEffect: function(player) {
		    				player.description = 'For a moment, you think you see the reflection of a ' + player.sex + ' ' + player.race + ' once called ' + player.name + ' in one of the crystals.';
		    				player.trapped = 'You are trapped inside a crystal, unable to move and only able to look out into the cave beyond. You are not even visible to other people except at the right angle, having become nothing but a cursed reflection.'
		    			}
			    	};
			    }
			},{
			    "id": "Wolf Swap",
			    "moveMod": 0,
			    "description": 'You wait helplessly within the crystal, your only hope being that someone might come and free you with a magic spell. Or so you think. You hear someone walking through the caves, and they pass in front of you. It\'s an adventurer, a tall and heavy-set wolf. His thick fur is dark, almost black, and very shaggy and fluffy. His face is scruffy and unkempt, and his eyes glow yellow. He\'s wearing a leather vest and huge boots, and smoking a cigarette that lights up the dark cave. His rank fur looks smelly and dirty, and his belly is bulging out from under his shirt. Do you want to try and grab his attention? [Warning: If you bodyswap in the crystal, you will lose all of your old equipment and gold, as you will be in a completely new body]',
			    "repeatable": true,
			    "trapevent": true,
			    "effect": function(player) {
			    	player.confirmStep = {
		    			yes: 'You try to wriggle and shout out to the wolf, but your image just silently mouths in the reflection of your crystal prison. It\'s enough to get his attention, though. He blinks, and looks at you with a curious glare. "What the fuck?" he says, in a deep and growling voice. He leans forward, and presses one huge dirty paw-hand on the crystal surface.</p>At that moment, you feel a strange rush, like you\'re falling yet moving higher and higher. You burst out from the crystal like coming up for air from the ocean, and stumble backwards. You feel so heavy and strange. Your mouth tastes ashy and nasty from the cigarette you\'re smoking, but you can\'t seem to stop. Your face is long and scruffy, with thick and huge teeth. You touch them with your tongue, and carefully touch your new face with your rough, clawed hands. You feel your pointy ears, and look down at your chubby, hairy belly. You flex your massive toes inside your heavy leather boots, and wag your thick, shaggy wolf tail. You have taken the wolf\'s body! For a moment, you see him shouting at you in the crystal, but you lose the angle, and can\'t seem to find him again. You move forward, momentarily wanting to help, but then your head aches. </p>God you need a smoke. You feel so horny all of a sudden, like you\'ve been trapped and unable to fap for ages. You drop your beloved leather jacket, and kick off your huge boots, letting your smelly and filthy massive wolf paws flex. Fuck yeah that feels good. You stroke and tease your thick, sweaty cock for a while, until you pant and drool, then blow a hot load all over your shaggy chest. That feels way better. The pleasure of your orgasm flows through your big dirty body, and your consciousness combines with the brain in your head, the brain of a big dirty horny wolf man. That\'s what you always were, right?',
		    			no: 'You let the wolf walk past without calling to him further. He doesn\'t deserve to be trapped here.',
		    			yesEffect: function(player) {
		    				player.description = "This adventurer is a tall, manly wolf. He has a hairy muscle gut, thick arms covered in scruffy fur, and a shaggy muzzle with enormous yellow fangs. He stinks of smoke and sweat. His long muzzle is twisted into a perverted grin, showing off his huge yellow fangs.";
		    				player.race = "wolf";
		    				player.size.value = 4;
		    				player.trapped = "";
		    				player.gold = 0;
		    				var wolfboots = {
		        					name: 'Filthy boots', 
		        					displayName: 'Filthy boots',
		        					short: 'filthy boots',
		        					long: 'There are some filthy boots here. They are heavy and thick. The tips are metal-plated, and the insides are sweaty and humid with a rank, manly stench of unwashed wolf paws.' ,
		        					description: 'There are some filthy boots here. They are heavy and thick. The tips are metal-plated, and the insides are sweaty and humid with a rank, manly stench of unwashed wolf paws.',
		        					area: 'dormaus',
		        					affects: [],
		        					id: 'wolfboots', 
		        					level: 1,
		        					itemType: 'armor',
		        					material: 'leather', 
		        					ac: 2,
		        					value: 50,
		        					weight: 1,
		        					slot: 'feet',
		        					equipped: false
	        				};
		    				var wolfjacket = {
		        					name: 'Leather jacket', 
		        					displayName: 'Leather jacket',
		        					short: 'leather jacket',
		        					description: 'There is a leather jacket here. It is heavy and thick. The zipper at the front is broken, so it hangs open all the time. The sleeves are sewn with biker patches. It stinks of smoke.',
		        					long: 'There is a leather jacket here. It is heavy and thick. The zipper at the front is broken, so it hangs open all the time. The sleeves are sewn with biker patches. It stinks of smoke.',
		        					area: 'dormaus',
		        					affects: [],
		        					id: 'wolfjacket', 
		        					level: 1,
		        					itemType: 'armor',
		        					material: 'leather', 
		        					ac: 2,
		        					value: 50,
		        					weight: 1,
		        					slot: 'body',
		        					equipped: false
	        				};
		    				var wolfcigarette = {
		        					name: 'Smelly cigarette', 
		        					displayName: 'Smelly cigarette',
		        					short: 'smelly cigarette',
		        					description: 'There is a smelly cigarette here, slightly-bent and stinking of smoke. It is magical, and never seems to burn out, but it stains your teeth yellow.',
		        					long: 'There is a smelly cigarette here, slightly-bent and stinking of smoke. It is magical, and never seems to burn out, but it stains your teeth yellow.',
		        					area: 'dormaus',
		        					affects: [],
		        					id: 'wolfcig', 
		        					level: 1,
		        					itemType: 'armor',
		        					material: 'paper', 
		        					ac: 2,
		        					value: 50,
		        					weight: 1,
		        					slot: 'head',
		        					equipped: false
	        				};
		    				player.items = [wolfboots, wolfjacket, wolfcigarette];
		    			}
			    	};
			    }
			}],
			items: []
		},
		{
			id: '3',
			title: 'Wet Cavern',
			area: 'caverns',
			light: true,
			content: 'This part of the cavern is lower than the rest, following a path down into the earth. It stops at a black, inky pool of water, the rest of the cavern flooded and inaccessible. The water is still as death.',
			outdoors: false,
			exits: [
				{
					cmd: 'north',
					id: '1'
				}
			],
			events: [{
			    "id": "Illithid TF",
			    "moveMod": 0,
			    "description": 'The darkness of these caverns is very oppressive, and even more so standing next to this inky-black water. It doesn\'t move at all, making it look almost like an endless pit into oblivion. You turn to leave, and hear the plip-splash of the water as you go. Hold on though, still water doesn\'t make noises?</p>You turn around, and stumble as you see a glistening, writhing form emerging from the depth. Water cascades from a long, thick black tentacle, followed by more of them, curling and twining together.</p>As you try to back away, one of the tentacles lashes forward with blinding speed. It coils around your leg, the flesh squishy yet with a firm, inescapable strength. The skin of it is cold and slimy, and it wraps slowly around your leg as it drags you towards the water. You struggle, but the stones here are wet and slippery, and your motions only make you slide further down. Another tentacle grabs your other leg, pulling you in faster. The tentacles seem endless, a forest of powerful limbs dripping with water and slime.</p>Surely this creature will be deadly if you cannot escape. Do you give in?',
			    "repeatable": true,
			    "valid": function(player, command, fn) {
			    	if (player.race == "illithid") {
			    		return false;
			    	} else {
			    		return true;
			    	}
			    },
			    "effect": function(player) {
			    	player.confirmStep = {
		    			yes: 'The crawling tentacles writhe around your legs, then begin to coil around your waist as well. Your body is constricted all around by the cold and slimy limbs of this strange creature. Slime soaks your skin, making your flesh tingle and itch. The tentacle wraps around your neck, squeezes just enough to make you struggle to breathe, and then suddenly plunges deep into your gasping mouth. You feel its bulging flesh as it forces its way into your throat, deeper and deeper. You are helpless to fight it, even when you feel your feet sink into the ice-cold water. If you could, you would take a deep breath, but the tentacle prevents that. In one more sudden jolt, you are plunged into the blackness of the dark water.</p>You can see nothing, hear nothing. All you can feel is the tentacles as they touch and stroke your flesh. One of them slides between your buttcheeks, then its firm tip presses against your hole. You squirm as it enters you, filling you bit by bit, stretching your insides. The parts of your skin exposed to the water begin to go numb, and icy cold penetrates deep into your body.</p>You start to realise that you haven\'t taken a breath for several minutes. Your body doesn\'t seem to need it. This pitch-black water is somehow sustaining you. The coldness sinks deeper and deeper, filling you, washing away the heat and warmth of your beating heart and human flesh. Some other warmth inside you, some fundamental human kindness and joy, is slowly frozen over, and replaced by a cruel, inhuman, emotionless chill.</p>Your skin feels slimy and damp. The colour of it begins to change, becoming a flawless jet-black that glistens wetly. Your muscles harden and bulge, thickening and growing with incredible strength. Your fingers and toes grow longer and stronger, before hard and razor-sharp claws emerge from the tips. Between your digits, a thin, dark webbing forms, perfect for moving in the water.</p>Your hair starts to fall out, all over, leaving you slimy and cold and bald on every inch of your form. Your head bulges, growing round and large, as your mind races with new intelligence and knowledge. Your eyes glow a deep yellow, illuminating the water, where you see an infinite forest of writhing, thrashing tentacles. The tentacle in your mouth emerges, and you stretch your jaw as something seems to form on your face. Thick, fleshy points, growing long, and strong, and moving like fingers at your will. A set of huge grasping black tentacles, almost like a beard covering your lower face. You flex your new body, and swim to the surface. There, you emerge from the depth, a monstrous and evil aquatic being. A tentacled, terrible illithid.',
		    			no: 'You grab one tentacle and heave with all your strength. It budges slightly, just enough for you to pull on your leg and slip it free from the slimy mass. You twist and crawl, fleeing as fast as you can.',
		    			yesEffect: function(player) {
		    				player.description = 'This adventurer is an inhumanly tall and muscular being, their skin pitch black and cold. Their hands and feet are webbed and clawed, and their head is large and bulbous, with a set of powerful tentacles covering their mouth and hanging down like a beard. Their yellow eyes glow with an evil, heartless expression.';
		    				player.race = "illithid";
		    				player.size.value = 4;
		    			}
			    	};
			    }
			}],
			playersInRoom: [],
			monsters: [],
			items: []
		},
        {
            id: '4',
            title: 'Dark Cavern',
            area: 'caverns',
			light: true,
            content: 'The caverns are pitch-black here. It\'s impossible to see even inches in front of yourself. Even with artificial light, it seems to sputter and die barely feet from your body. The air is so cold that it hurts.',
            outdoors: false,
            exits: [
                {
                    cmd: 'west',
                    id: '2'
                },{
                    cmd: 'south',
                    id: '5'
                },{
                    cmd: 'east',
                    id: '3'
                }
            ],
            events: [{
			    "id": "Grue Vore",
			    "moveMod": 0,
			    "description": 'The darkness of this cavern makes it very difficult to navigate. It\'s so quiet, and cold. You move carefully, to avoid pits, and then you feel a tingling on the back of your neck. Dread shivers down your spine. Something is moving in the darkness. It was just for a moment, but you felt something slither past. For some reason, you feel a thought in your head. If you stay here, in the dark, you are likely to be eaten by a grue. Do you stay here?',
			    "repeatable": true,
			    "effect": function(player) {
			    	player.confirmStep = {
		    			yes: 'Grues are strange and mysterious creatures. They exist only in absolute darkness, and can never be seen. They are indestructible, and deadly, and always hungry. And one is right next to you.</p>Some sort of enormous paw or talon pins you down to the floor. You can\'t see what it is, but you feel the gigantic claws, as thick as your upper arm. You feel the sharp points pressing down on your belly, and the thick, heavy fur of the beast that covers its foot. You smell a musty, beastly stink surrounding you, as the weight on top of you pushes down harder. Something leans in, and you feel the hot breath of a massive creature washing over you. Drool drips from gigantic fangs, and the heat of a terrible beast washes down over you from above. You feet its tongue slide out, and lick slowly up your chest. It wraps around you like a tentacle, warm and wet. You are constricted and squeezed, as the tongue licks between your legs, up your rear, tasting every inch of you. You are drawn inescapably into the maw of the creature, and it closes its mouth around you. The darkness within its maw is no less pitch-black than the cave itself, but it is warm and wet and slippery.</p>You struggle helplessly as you are swallowed. Your body constricted on all sides, dragged down into the stomach of a terrifying beast of myth and darkness. You never stood a chance.',
		    			no: 'You wisely run as fast as you can. The feeling of dread does not pass until you feel the safety of light shining on you.',
		    			yesEffect: function(player) {
		    				var roomObj = World.getRoomObject(player.area, player.roomid);
		    				
		    				var respawnRoom = World.getRoomObject('overworld', '3');
		    				
		    				Room.removePlayer(roomObj, player);
		    			
		    				player.position = 'standing';
		    				
		    				player.predator = 'a grue';
		    				
		    				respawnRoom.playersInRoom.push(player);

		    				player.roomid = respawnRoom.id;
		    				player.area = respawnRoom.area;
		    				player.chp = 1;
		    				player.cmana = 1;
		    				player.cmv = 7;
		    			}
			    	};
			    }
			}],
            playersInRoom: [],
            monsters: [],
            items: []
        },{
            id: '5',
            title: 'Grave of the Dragon',
            area: 'caverns',
			light: true,
            content: 'This is the deepest part of the caverns. Here, far from the light of the sun, an ancient beast lies dead. The skeleton of a gigantic dragon is coiled in these dark, cold caverns, with an enormous rusty, broken sword sticking out from the center of its skull. The air is so cold that it\s almost impossible to breathe, and the darkness is suffocating.',
            outdoors: false,
            exits: [
                {
                    cmd: 'west',
                    id: '4'
                }
            ],
            playersInRoom: [],
            monsters: [{
				name: 'The Dead Dragon',
				level: 15,
				short: 'Dead',
				long: 'Something about the dead dragon fills you with a feeling of power and fear.',
				description: 'The dead dragon is enormous. His skeletal form coils around the walls, twisted and spiralling, and only the arms and head are even visible to you. The rest is hidden by the dark depths of the cave, down a chasm you cannot reach.',
				inName: 'The Dead Dragon',
				race: 'dragon',
				id: 51,
				area: 'caverns',
				weight: 245,
				diceNum: 2,
				diceSides: 8,
				diceMod: 5,
				meleeRes: 10,
				str: 16,
				position: 'standing',
				attackType: 'breathes on',
				damRoll: 10,
				hitRoll: 10,
				ac: 20,
				gold: 1000,
				items: [{
					name: 'Mastery Blood',
					short: 'a vial of Mastery Blood',
					long: 'A vial of Mastery Blood' ,
					area: 'caverns',
					id: 'masteryblood',
					level: 1,
					itemType: 'bottle',
					material: 'blood', 
					weight: 0,
					slot: '',
					value: 495,
					equipped: false,
					spawn: 3,
					onDrink: function(player, roomObj, item) {
						var masteryspell = {
					            "id": "mastery",
					            "display": "Mastery",
					            "mod": 0,
					            "train": 100,
					            "type": "spell",
					            "wait": 0,
					            "learned": true,
					            "prerequisites": {
					                "level": 1
					            }
					        };
					    	Character.addSkill(player, masteryspell);
						
						World.msgPlayer(player, {
							msg: 'You feel a dark, sick power filling your belly. It feels like cold tendrils squeezing at your soul, making you cruel and dark inside. You now know the secrets of the spell of Mastery!',
							styleClass: 'cmd-drop blue'
						});
					}
				}],
				merchant: true,
				wanderCheck: 38,
				itemType: 'mob',
				runOnAliveWhenEmpty: false,
				behaviors: [],
				topics: {
					name: '"I once was, but am no more. My name was lost, with my power."',
				    job: '"I am greatest of the dead. A memory of a terrible betrayal."',
				    bye: 'There is no response from the cold and lifeless skeleton.',
				    cock: '"I have only bones now, and not the type you are interested in, mortal."',
					paws: '"My feet are bones, deep in the mists of the land of the dead."',
					betrayal: "Once I was mighty, but my brothers tricked me, took me here, and ate of my flesh and of my soul. Their sin cursed this place for all eternity.",
					blood: "The blood of ages drips, cold, and powerful, in the depths of the earth. I have mastery of it, and I can share it with you, for a price.",
					price: "Gold is meaningless to me, but it is a way to prove you have earned the right to drink the blood of ages.",
					mastery: "The spell of mastery is old, dark magic. Forbidden now, but I remember the old ways. It takes the precious gift of free will from mortal souls.",
					orochi: "That name stirs something in me. I knew it once.",
					hunger: "I sometimes remember hunger, just for a moment. It would be foolish for any mortal to indulge me in it.",
					hellbind: "Hellbind heart is a cruel spell, and only those with cruelty in their hearts can cast it. It curses one to be dragged down to Tartarus, the land of the dead deep below me, in which I am forever half-trapped."
				}
			}],
            items: [],
			events: [{
			    "id": "The Hunger",
			    "moveMod": 0,
			    "description": 'The sword embedded deeply in the massive dragon skull calls out to you. Though it is ancient and rusty, it was clearly created with great craftsmanship. In the past it was probably worth a fortune. You take hold of the hilt, and experimentally tug to see if it can come loose. It slides up slightly, and you feel a terrible burning feeling run down your fingers. It feels like they are submerged in ice water, and your insides feel cold and dark, with an alien and monstrous hunger and cruelty. Do you keep pulling on the sword?',
			    "repeatable": true,
			    "effect": function(player) {
			    	player.confirmStep = {
		    			yes: 'You keep pulling on the sword, telling yourself that the coldness is just from the strain, and from holding onto metal in such a chilly place. The more you pull, the more the coldness spreads. Your whole body is freezing now, but you are not shivering. Your limbs are stiff and still, and your insides feel somehow dark and corrupted. When you feel your face twisting into a wicked snarl, you start to panic. You try to push the sword back in, but your arms are moving by themselves. They pull it out faster and faster, and darkness overtakes you. You can no longer feel your arms, or your legs. Your vision turns red, then black, as an evil presence overtakes you.</p>For moments, and glimpses, you see things. Brief periods before you slip back into the darkness. In one, you are holding someone down under you. Your hands are gripped firmly around their neck as they struggle and yell. In another, you can only see red, and you feel yourself lunge forward, your teeth biting through something that oozes warmth all over your chest.</p>Much, much later, you awaken in the cold cave. Your body is sticky, and you are covered in dark stains. There is a nasty iron-like taste in your mouth, and your fingertips look red and wet. Much later, when you are walking past town, you see a missing-persons poster with a face that looks strangely familiar.',
		    			no: 'You wisely return the sword to its position. The coldness withdraws from your arms, though you still shiver with the chill in this dark place.'
			    	};
			    }
			},{
			    "id": "orochi",
			    "moveMod": 0,
			    "description": "When you touch the monolith, you feel a shudder of energy flow through your arm. For a moment, you can't move at all. Trapped against the stone, you feel something moving up behind you. Something dark, and massive. A claw slides against your back, and a hissing, sibilant voice whispers in your ear. 'Not many visit my altar anymore. You deserve a reward...a special reward.' The voice seems to sink into your mind, fill you, and the presence itself moves forward, and sinks into your body. You feel wrong. This body is so small. So weak, so...single. You were legion once. Then the feeling fades, but it leaves something behind. You have learned a dark ritual: the skill OROCHI. Warning: if you use this spell, you will be forced to share your account with another player as a two-headed monster! This is NOT REVERSIBLE.",
			    "repeatable": false,
			    "valid": function(player, command, fn) {
			    	return false;
			    },
			    "effect": function(player) {
			    	var orochi = {
			            "id": "orochi",
			            "display": "Orochi",
			            "mod": 0,
			            "train": 100,
			            "type": "spell",
			            "wait": 0,
			            "learned": true,
			            "prerequisites": {
			                "level": 1
			            }
			        };
			    	Character.addSkill(player, orochi);
			    }
			}]
        }
	]
};

