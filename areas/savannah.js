'use strict';
var Cmd = require('../src/commands').cmd,
Room = require('../src/rooms').room,
Character = require('../src/character').character,
World = require('../src/world').world;

module.exports = {
	name: 'Makarna',
	id: 'savannah',
	type: 'rural',
	levels: 'All',
	description: 'The savannah of Makarna.',
	reloads: 0,
	created: '',
	saved: '',
	author: 'Blue',
	messages: [
		{msg: 'Insects buzz in the air.'},
		{msg: 'The musky scent of lions wafts past you.'}
	],
	respawnOn: 8,
	rooms: [
		{
			id: '1',
			title: 'The Harem of Makarna',
			light: true,
			area: 'savannah',
			content: 'This is a beautiful wide open room, dominated by a tinkling fountain in the center. Water is a valuable resource, and uncommon to see in this hot and arid land. The walls and floor are polished stone, but colourful rugs, blankets and cushions cover most of it. The far wall is open to the air, with only thin  curtains to block out some sun for comfort, and from the balcony therein, much of the city is visible far below.',
			outdoors: false,
			exits: [
				{
					cmd: 'east',
					id: '2'
				}
			],
			events: [{
			    "id": "Learn Heat",
			    "moveMod": 0,
			    "description": "It is a quiet day in the harem. The king has not called for you, and you are lounging with some of the lionesses, eating fruit from silver platters that a worker left for you. Another worker enters, a tall human with bronze skin, and he places another platter at the other side of the room for some of the other girls. When he leans down, one of the lionesses stands and strokes his back, and he shivers with delight. Your nostrils pick up an unusual scent, one that makes you think of sex and lust, but it does not affect you like your king\'s mind-crushing musk does. The human, however, seems very affected. He is panting, and shaking, his face red. The lioness leads him over to some cushions, and lies down as she spreads her legs. The human is helpless to resist, and he lunges down, before shoving his cock into her pussy and beginning to thrust and pound like a man possessed. The more his pleasure grows, the more his body changes. His skin is covered by a growing pelt of golden fur. His hair thickens and grows out into a wild mane around his head. His muscles bulge, and his feet flex out into huge clawed paws. When he climaxes, he lets out a beastly roar, and a tufted tail thrashes from his rear. He pulls free from the lioness, and stumbles out of the room, leaving behind his clothing.</p>Watching this, you realise that your own changed body has the same power that woman displayed. You can focus on your neediness and arousal, and make others desperate to use and fuck you. You have learned the Heat spell, which will attempt to turn any target female into a male, and any target male into a lion.",
			    "repeatable": false,
			    "valid": function(player, command, fn) {
			    	return false;
			    },
			    "effect": function(player) {
			    	
			    }
			},{
			    "id": "Serve Lion Cock",
			    "moveMod": 0,
			    "description": 'You are lounging by the fountain, trailing your fingers in the cool water, when a guard enters and taps his spear gently against the ground. He points to you and two other slaves. "The king requests your presence", he says. The three of you purr in unison, thinking about your beloved and sexy master. You follow the guard through long and winding corridors, and up the stairs to your king\'s chambers. The guard is more talkactive than most, and he points to the golden symbols in each step. "Each of these is the name of a king of Makarna", he says. "The king demanded we engrave them, to show that he is above them all, and that they were just stepping stones to his power". He looks a little uncomfortable, but you have soon reached the top, and he leads you through into the bedchambers.</p>The king is lying naked in his bed, which is made from pillows and cushions, with curtains surrounding it. He purrs in a deep rumbling voice as the three of you crawl into the bed with him. The first slave is instructed to rub and massage his feet. The second is to rub his shoulders. You get the most prestigious position – to service your king\'s mighty sceptre. You eagerly crawl over his powerful furry body, and lower your muzzle to the tall and erect shaft standing firm between his legs. You lick the sides, and nuzzle the shaft, teasing and stroking it gently at first. Months of training have taught you exactly how your king likes it, and you would not dare to do it any differently. You lick slowly around the head, and tease the base with your fingers, until he growls and begins to leak slippery, clear precum. That is your signal to lower your head onto his cock and take it into your mouth. You taste his pre in your throat as you start to suck him, and then you slide your face back and forth, starting slow but increasing in speed when his cock twitches and throbs.</p> You feel him moving and shifting, his heart pounding and his leg kicking as his pleasure grows. You grip the base of his cock and start to pump it, while also sucking the upper half of his shaft. That sends him over the edge, and he roars as his cock pulses and spews cum over your tongue and into your throat. While he cums, you pull your head away, allowing the last few ropes of hot white cum to spray onto your face and chest. He grins and pulls the three of you up close to him, and you snuggle and rub together for hours before you return to the harem.',
			    "repeatable": true
			},{
			    "id": "Whispers in the Night",
			    "moveMod": 0,
			    "description": 'It is late at night when you are stirred from your sleep by clanks and whispers outside. Curious, you creep over to the doorway, and listen in. Two voices are speaking in hushed whispers, one old and rough, and one younger and deep. "The situation is getting worse, Rufo. The hyenas are skirmishing with our soldiers every week, and tigers are building up to something, I\'m sure of it. Every time I see that jungle of theirs my fur stands on end", the younger man says. "You have to trust in Leonar\'s judgement. He may not be his father, but he is our king and we must show him allegiance", says the older man. "Sometimes I wish you were the king instead, Rufo", mutters the younger man. There is a thump, and a grunt of pain. "Never let me hear you say that again, boy! Get back to your post", the older man snarls. You hear the clanking of someone leaving, then a resigned sigh from the older male. "I\'m not letting another good man be charged for treason", he says to himself. There is nothing further, and you sneak back to your bed.',
			    "repeatable": true
			},{
			    "id": "Serve Lion Feet",
			    "moveMod": 0,
			    "description": 'You are brushing your fur with a silver comb by the fountain when you feel someone tap your shoulder. You look up to see a lion guard, who nods to you politely. "Excuse me, but the king requests your presence. He has been walking in the gardens, and his feet are tired", he says. You feel a rush of pleasure at being called to serve your king, and follow the guard with a spring in your step. He takes you up to the highest part of the pyramid, where the very floors are inlaid with shining marble, and through the bronze double doors to the king\'s bedchambers.</p>He is sitting on a chair by a mirror made of black obsidian, while he grooms his shaggy muzzle and thick mane. The mighty lion glances at you, then lifts his feet up and places them on a soft footstool. You kneel at his feet, knowing your place. Each paw is long and wide, with dark brown pawpads and strong, flat toes. The fur is soft, but currently musky and warm with sweat. Not that you mind, you exist to serve your king after all. You take one of his feet in your hands, and begin to rub and stroke it, massaging your thumbs up through the sole and taking time to rub small circles in each tough pad. The king grins and looks over to a guard in the corner. "See this? Found this one in a little town when I was visiting overseas. Used to be an adventurer, now sitting here under my feet, doing anything I say", the king says smugly. </p>The guard nods, and your king spreads his toes before pressing one foot onto your face. The warmth from it washes over you, and his powerful musk makes you shudder. "Lick the sweat from my feet, slave", he says. You respond instantly, not caring at all about where he\'s been walking or how it will taste. Your tongue licks salty sweat from your master\'s pawpads, as you obey his every whim. "One day the whole world will be under my feet where it belongs", he says. The guard offers no comment. Once you have licked the lion\'s pawpads until they are glistening and clean, he makes you do the other one as well. Much later, you return to the harem with your face covered in your beloved king\'s rank and sweaty stink.',
			    "repeatable": true
			},{
			    "id": "Free Will?",
			    "moveMod": 0,
			    "description": 'There is some sort of commotion going on outside. You watch it idly from the balcony, as a group flanked by guards approaches the palace. You like to watch the people far below go about their lives from your viewpoint up here, as you are allowed to leave the harem only when your king calls you up to his bedroom to service him. A clanking from the hallway draws your attention, and you turn to see an armored lion enter the room. The soldiers of Makarna sometimes like to wear ornate breastplates and helmets, but never anything to cover their musky cocks, as a point of pride. This one is very tall, with brutal scarring across his face. He looks around, then points to you. "Pretty one, the king requires your attention", he says. Obediently, you follow him, but when you turn to head up to the king\'s room, he gently takes your arm and leads you aside. "Not today, no. The king requires your attention in the throne room", he says.</p>You have never been there before, so this is very surprising. You remain quiet and follow the muscular soldier as he leads you down several staircases and into a long and tall chamber.</p>The throne room consists of a long red carpet, leading through stone columns, up to a huge golden throne. On that throne, lounging with one of his bare paws over the curved arm, is the gorgeous body of your muscular king. He\'s wearing a gleaming crown, and there is a red velvet robe draped around his shoulders.</p>Of course, you immediately rush to your king\'s side, and take your place at his feet. Your body moves almost without any thought on your part at all. The long training and brainwashing has worked well to make you completely unable to think of anything but serving your beloved lion. Without really paying attention to you, he idly motions you with one finger, a gesture that you recognise to mean he wants to be worshipped. You kneel beside the throne and lift your head to your king\'s crotch, where you gently nuzzle his furry golden balls. Your muzzle opens and you lick one, lifting the hefty musky sack with your tongue, while you taste and smell your master\'s incredible body.</p>You are so distracted by your service that you barely notice the people entering the throne room. It\'s a band of hyena-people, dressed in thick armor made from pelts and steel, adorned with spikes and chains that give them a savage and brutal appearance.</p>The lead hyena, the tallest and most muscular, walks up and removes their helmet, to reveal a scarred but definitely female face. "King Leonar, your soldiers have been trespassing again. You know the southern plains are ours", she says. You realise this is the first time you\'ve heard your king\'s name. It never seemed important, only his pleasure was important. He strokes your head with a smug grin as he responds. "All the plains of Makarna belong to the lions. You are permitted to remain in your filthy encampments only by my good grace", he says. The hyena grips the hilt of a sword at her waist, and the lion guards surrounding her clank in unison as they grip their spears. She pulls her hand away, and speaks through gritted teeth. "Not all of us are content to sit at your feet and do as we are told. This isn\'t over, Leonar", she snarls. You blink as your king stands up and walks off, leaving you craving his musk. The hyena looks down at you with disdain. "Do you have no self-respect? You don\'t need to be someone else\'s toy", she says. Do you listen to her words?',
			    "repeatable": true,
			    "valid": function(player, command, fn) {
			    	if (player.master == "Leonar") {
			    		return true;
			    	} else {
			    		return false;
			    	}
			    },
			    "effect": function(player) {
			    	player.confirmStep = {
		    			yes: 'Some spark of independance flickers within you. A faint memory of your previous life, exploring wherever you wished and doing as you please, not being trapped in a palace as a toy. The hyena smirks a little as she sees the beginnings of defiance in you. "Maybe there\'s hope for this country after all, if even his most brainwashed slaves are not completely lost", she says. After she and the other hyenas are guided away by guards, the original guard returns and leads you back to the harem. He looks disappointed, and annoyed. You ask him what is wrong, and he is surprised to see a harem slave speaking for themselves. "Leonar is...a difficult king. His father was just and kind. I hope that one day my king will be as great as he was", he says. He then bows, and leaves you be.',
		    			no: 'You have no need for willpower! You belong to your king. It doesn\'t matter how he acts or what you get in return. You are just filled with joy and arousal when you make him happy. You exist solely to serve him. Ignoring the hyena, you nuzzle the edge of the throne and wait for your king to return. She snorts with disgust at your submissiveness, and leaves. After she and the other hyenas are guided away by guards, the original guard returns and leads you back to the harem. He looks disappointed by something.',
		    			yesEffect: function(player) {
		    				player.master = null;
		    			}
			    	};
			    }
			},{
			    "id": "Escape From Makarna",
			    "moveMod": 0,
			    "description": 'You are sleeping soundly, the cool night air brushing across your back, when you are woken by a rough hand on your shoulder. You look up to see the scarred and weathered face of the guard who took you to the throne room. He motions you to be quiet, and leads you over to the door. "You have some spirit left in you. I can tell. Do you want to be free of this place?", he asks. You are conflicted. Do you want to try to escape?',
			    "repeatable": true,
			    "valid": function(player, command, fn) {
			    	if (player.master == "Leonar") {
			    		return false;
			    	} else {
			    		return true;
			    	}
			    },
			    "effect": function(player) {
			    	player.confirmStep = {
		    			yes: 'You nod. You would be lying if you said you hadn\'t enjoyed being here, at least a little, but you do not want to stay here forever. The old lion hands you a robe, and helps you put it on. Clothes feel strange and unfamiliar after so long as a naked slave. He pulls the hood over your head, and starts to walk you down through the corridor. "If anyone asks, I am your father, Rufo", he whispers to you. "You are a kitchen servant, and you fell asleep in the pantry, so I am taking you home", he continues. You nod, and shiver with nerves every time you hear a guard nearby. Rufo knows the shifts well, however, and he sneaks you past each guard without being seen. </p>It feels like an eternity, and then suddenly, you are outside, in the night air of the city. It has been so long since you felt it that you almost stop to breathe it in, but Rufo hurries you along. The streets are quiet at night, with the market stalls empty and the shop doors bolted shut. You don\'t know where the old lion is taking you until you smell water and fish, and your feet step out onto the wooden planks of some docks. There is a boat being loaded with cargo, and he takes you up to the gangplank. "This is as far as I can take you. I heard you are from a town across the sea. This boat will take you down the great river and is heading out there. I hope it leads you home", he says. </p>You don\'t know if it is just the effects of your harem-slave conditioning, but in the silvery light of the moon, Rufo is very handsome. Despite the many scars that have left his face rugged and torn, and the evident age showing in the grey streaks of his mane, he is still a powerful and strong lion man. You lean up and kiss him on the cheek, and he strokes your arm for a moment. "I will treasure that", he says with a smile. He turns and leaves, and you quickly climb onto the boat, unsure of where it will take you.',
		    			no: 'You shake your head. The old lion\'s eyes look a little sad. "I suppose this is for the best. I can be glad that you are happy, at least", he says. You return to your bed, and dream of your perfect, beloved king, using you like a toy.',
		    			yesEffect: function(player) {
		    				var roomObj = World.getRoomObject(player.area, player.roomid);
		    				
		    				var respawnRoom = World.getRoomObject('ocean', '1');
		    				
		    				Room.removePlayer(roomObj, player);
		    			
		    				player.position = 'standing';
		    				
		    				respawnRoom.playersInRoom.push(player);

		    				player.roomid = respawnRoom.id;
		    				player.area = respawnRoom.area;
		    			}
			    	};
			    }
			}],
			playersInRoom: [],
			monsters: [],
			items: [],
			beforeEnter: function(roomObj, fromRoom, target) {
				return true;
			},
			onEnter: function(roomObj, target) {
				
			}
		},{
			id: '2',
			title: 'The Halls of Makarna Palace',
			light: true,
			area: 'savannah',
			content: 'These halls are blessedly cool in this scorching hot country. The floors are smooth polished stone, and the yellow stone walls are decorated with gold leaf.',
			outdoors: false,
			exits: [
				{
					cmd: 'west',
					id: '1'
				}
			],
			events: [],
			playersInRoom: [],
			monsters: [],
			items: [],
			beforeEnter: function(roomObj, fromRoom, target, player) {
				World.msgPlayer(player, {
					msg: 'You walk to the door and try to move through, but there is a tall lion beyond, his face and chest scarred thoroughly. He gently blocks your path with a spear. "You must stay here, beautiful one", he says.',
					styleClass: 'error'
				});
				return false;
			},
			onEnter: function(roomObj, target) {
				
			}
		}]
};

