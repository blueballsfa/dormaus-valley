'use strict';
var Cmd = require('../src/commands').cmd,
Room = require('../src/rooms').room,
Character = require('../src/character').character,
World = require('../src/world').world;

module.exports = {
	name: 'High Seas',
	id: 'ocean',
	type: 'water',
	levels: 'All',
	description: 'The high seas.',
	reloads: 0,
	created: '',
	saved: '',
	author: 'Blue',
	messages: [
		{msg: 'Salt spray splashes on your face.'},
		{msg: 'You hear the distant breaching of a whale.'}
	],
	respawnOn: 8,
	rooms: [
		{
			id: '1',
			title: 'Cabin in the Jolly Wolfpits',
			light: true,
			area: 'ocean',
			content: 'This cabin is cramped and damp, and it sways constantly from the motion of the sea. A tiny porthole looks out onto the endless waves, and several hammocks hang from the ceiling.',
			outdoors: false,
			exits: [
				{
					cmd: 'up',
					id: '2'
				}
			],
			events: [],
			playersInRoom: [],
			monsters: [],
			items: [],
			beforeEnter: function(roomObj, fromRoom, target) {
				return true;
			},
			onEnter: function(roomObj, target) {
				
			}
		},{
			id: '2',
			title: 'The Deck of the Jolly Wolfpits',
			light: true,
			area: 'ocean',
			content: 'From here, the ocean seems to go on forever. The horizon is just a line in the infinite distance. The waves lift and crash, and the boat heaves along them. The sails above you are filled with wind, and you are making good time.',
			outdoors: false,
			exits: [
				{
					cmd: 'down',
					id: '1'
				}
			],
			events: [{
			    "id": "Musky Pirates",
			    "moveMod": 0,
			    "description": 'No matter how long you wait, the ship never seems to be any further along. You know you are moving quickly, and the wind is good, but the endless ocean makes distance impossible to guess. You need something to pass the time.</p>You are contemplating this when your nose prickles from a salty, sweaty and overpowering stink. You turn to see a sailor leaning on the side of the deck next to you. He\'s a wolf, his fur thick and shaggy, with big paws and a long but chubby snout, going with his round belly that peeks out from under his striped shirt. He stinks with a rough and dirty odor. He opens his muzzle and lolls his tongue happily at you. "Lookin\' for a good time? I can keep ya busy, cutie!" he barks.',
			    "repeatable": true,
			    "effect": function(player) {
			    	player.confirmStep = {
		    			yes: 'The chubby wolf makes a short, happy howling sound, before grabbing you and hugging you firmly. Your face is shoved into his soft belly, and the stink of his unwashed fur washes over you. His large hands stroke over your back and down your sides, before he presses his muzzle against your neck, chest and crotch. His sniffing, nuzzling muzzle is ticklish and affectionate. You feel his wet nose slide down your belly, savouring your scent before you jump a little when he shoves his face firmly into your crotch. His long tongue slides out and licks up your thighs and around your crotch, and his hands roll you over so that he can sniff your rear. His affections are so eager that you barely have time to respond. You just grunt when you feel his cold nose slide along your tailhole, and then shudder when his expert tongue licks your rear and teases your hole.</p>You are pressed down on the wooden deck, the chubby wolf pressing down atop you as he licks up and down your ass. You squirm when his tongue curls and presses against your rear, and then you moan and shudder when you feel his tongue slip briefly inside you. Once you are thoroughly lubricated and slippery, you feel his thick cock sliding between your cheeks. He starts to hump and slide it up and down, hotdogging you while his fat belly bounces on your back. You hear his claws scraping against the wooden planks when he starts to get more excited, and then he howls a long, happy sound. You feel his hot ropes of cum spray along your back, in thick and messy spurts that drip down your sides.</p></p>For the rest of the trip, the dirty and smelly wolf sailors are all over you. You are woken up in the morning with a wolf licking your feet, his tail wagging. You will be eating dinner when one will offer you to eat his ass or suck his fat cock. Several times when walking along the deck, a wolf will grab you and shove your face deep into his damp sweaty armpit, or beg you to rub and jiggle his fat belly. While you are getting an eager butt-licking from one of the wolves, he admits to you that the whole crew are actually pirates. They never actually hurt anyone, but a few armpit-shoves tend to knock out most opponents with musk overdose.</p>Eventually, after you are well aquainted with the pirates and their various unique musks, you reach your destination. The ship travels up a river, and the pirates drop you off at the docks before carrying on with their journey. A whole gang of chubby pirates waves you off as they leave, their tails wagging, and you realise you will miss your new friends.',
		    			no: 'You decline, but for the rest of the trip, he and the other dirty sailors are constantly propositioning you. There will be a wolf at your feet in the morning, licking you awake, one pressing his belly up against your back when you look out from the deck, and even the captain tries to get you to come to his cabin. During this time, you begin to realise that these smelly men are actually pirates. Despite that, they at least stop when you say no. It seems the pirate code is real after all. Still, you are quite tired of dealing with them by the time you reach your destination. </p>The ship travels up a river, and they drop you off at the docks before carrying on with their journey. A whole gang of chubby pirates waves you off as they leave, and you realise you have gotten a little fond of them, despite their goofy behaviour.',
		    			yesEffect: function(player) {
		    				var roomObj = World.getRoomObject(player.area, player.roomid);
		    				
		    				var respawnRoom = World.getRoomObject('bayou', '1');
		    				
		    				Room.removePlayer(roomObj, player);
		    			
		    				player.position = 'standing';
		    				
		    				respawnRoom.playersInRoom.push(player);

		    				player.roomid = respawnRoom.id;
		    				player.area = respawnRoom.area;
		    			},
		    			noEffect: function(player) {
		    				var roomObj = World.getRoomObject(player.area, player.roomid);
		    				
		    				var respawnRoom = World.getRoomObject('bayou', '1');
		    				
		    				Room.removePlayer(roomObj, player);
		    			
		    				player.position = 'standing';
		    				
		    				respawnRoom.playersInRoom.push(player);

		    				player.roomid = respawnRoom.id;
		    				player.area = respawnRoom.area;
		    			}
			    	};
			    }
			}],
			playersInRoom: [],
			monsters: [],
			items: [],
			beforeEnter: function(roomObj, fromRoom, target) {
				return true;
			},
			onEnter: function(roomObj, target) {
				
			}
		}]
};

