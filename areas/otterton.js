'use strict';
var Cmd = require('../src/commands').cmd,
Room = require('../src/rooms').room,
Character = require('../src/character').character,
World = require('../src/world').world;

module.exports = {
	name: 'Southern Valley',
	id: 'otterton',
	type: 'valley',
	levels: 'All',
	description: 'South of the valley.',
	reloads: 0,
	created: '',
	saved: '',
	author: 'Blue',
	messages: [
		{msg: 'The air is fresh and clean.'},
		{msg: 'You hear the rustling of animals all around you.'}
	],
	respawnOn: 8,
	rooms: [{
        id: '1',
        title: 'Top of the Valley',
        area: 'otterton',
		light: true,
        content: 'From this high point, you can see down the valley as it flows south. The edges of the valley, swaying with grass, raise up either side up of you to the tall bare stony peaks of the mountains. You can see the shining ribbon of a river flowing down to the distant sea.',
        outdoors: false,
        exits: [
            {
                cmd: 'south',
                id: '2'
            },{
				cmd: 'north',
				id: '5',
				area: 'farm'
			}
        ],
        playersInRoom: [],
        events: [{
		    "id": "Fox hunting",
		    "moveMod": 0,
		    "description": '"Woah there! Heel, boys!", a voice cries out behind you. You turn to look, and see an adventurer struggling to hold on to the leashes of two enormous hunting dogs. They look at you and wag their tails, and the panting man gives you a tired smile. "Oh hey there! These two are magic hunting dogs, guaranteed to find me a fox if I keep them out long enough!", he says eagerly. You see his ears start to rise up, growing pointy and moving atop his head, while his hair starts to turn orange from the root up. You nod as he describes where he got the dogs from, and the two dogs grin in a very sinister way as they stand either side of him. His face pushes out into a vulpine muzzle, and his pants start to fall as a long, thick and bushy fox tail forms behind him. The two dogs start growling, and only then does he realise what\'s happening. Yipping in terror, he falls to all fours and runs off, the two dogs chasing after him.',
		    "repeatable": true
		},{
		    "id": "Cowswap",
		    "moveMod": 0,
		    "description": 'You pass by a farmer, pulling a cow along with a rope tied around its neck. He\'s talking to the cow, assuring her that she will go to a good home once he sells her at market, and apologising for not being able to take her back home. The cow just moos in a grumpy, annoyed sort of way, until finally the farmer sighs. "Fine fine, but I did promise a cow would be at the market for sale", he says. You watch as he unbuttons his overalls and drops them, then pulls off his large, muddy boots. He drops his shirt, and kicks off his underwear, revealing his full naked, hairy, muscular body.</p></p>He then removes the rope from the cow\'s neck and takes off the cowbell she is wearing, before putting it on his own neck. Immediately, his cock and balls start to bulge and swell. His balls in particular inflate massively, the skin turning pink as they jiggle between his legs. Bulges form on the flesh, before they inflate out into milk-leaking teats. The farmer moos loudly, and small horns start to emerge from his hairline. White and brown spotted fur grows all over his body, and he rolls over onto all fours as his hands and feet stiffen and harden into cloven hooves.</p></p>Meanwhile, the cow is shrinking and rising up onto two feet. She adjusts her stance, as she becomes a thick and strong anthro cow, before pulling the farmer\'s overalls on, with her udder bulging from a gap in the front. She then walks off with her new cow on its lead.',
		    "repeatable": true
		},{
		    "id": "Mud drake TF",
		    "moveMod": 0,
		    "description": 'You hear a rumbling behind you, and step out of the road to allow a cart to pass by. It\'s piled high with goods, and being pulled along by a strange beast. It\'s a fat reptilian creature, its four legs low to the ground, and its body stocky and strong. It smells strongly of a musky barnyard stink, with its brown splotchy body covered in thick hide instead of scales. The vehicle stops next to you, and the big lizard sniffs around before panting and showing off a long, dripping tongue. </p></p>"Hey there!", a voice calls, and you look up to see a slender rabbit grinning at you. "Headed down to Otterton? It\'s a long and boring trip, but maybe I could help you out with that." he says. He points to his lizard beast, and the creature sniffs you curiously. "I could do with a second puller. It\'s easy work, and you won\'t be doing much thinking. When I get to Otterton, I\'ll turn ya back and pay ten gold. What do you say?", the rabbit says. You look down at the muscular, smelly beast of burden, and weigh your options. Do you want to try becoming one?',
		    "repeatable": true,
		    "effect": function(player) {
		    	player.confirmStep = {
	    			yes: 'You weigh your options for a moment, but then agree to the rabbit\'s proposal. He seems pretty shocked, but then he grins and bounces on his seat. "Woah, two folks really want to become smelly mud-drakes? Well, I ain\'t complainin!", he says. "Easiest way to do this is let the big dummy here spread his change onto ya", the bunny explains. He gets you to stand on all fours, your hands and feet sinking slightly into the damp mud on the road. The big brown lizard starts to get interested, and he bumps his huge scaly head against yours. His thick musk washes over you, as he clumsily and stupidly rubs and nuzzles you. His hot breath is damp and humid, and his huge tongue drools as he starts to lick at your face. His saliva is thick and slimy, and it makes your skin tingle. Your jaw starts to ache, as it stretches and pushes at your lips. You feel your teeth thickening and swelling, jutting out as they transform into massive fangs. Your tongue flops from your mouth as it grows, becoming heavy and wet. </p></p>You feel your body starting to grow heavier and stronger, as muscles grow and bulge under your skin. You posture shifts, your arms and legs becoming more bow-legged, and your hands and feet spreading out in the mud as they turn into thick, wide clawed feet. You feel your fingers and toes swelling and bulging, before you shudder at the feeling of wide, dull claws emerging from them and digging into the mud. Your body sweats heavily, the sweat dripping from your chest into the mud as you are surrounded by a smelly, raunchy barnyard stink. Your new aroma makes the other mud drake curious, and he clambers on top of you, his head nuzzling and poking your back as he crawls over your face. You look up and see his soft, scaly balls, and thick sheath-covered cock hanging in front of you. Your long wet tongue licks out instinctively as you taste him, savouring the beast\'s scent. You get a strange instinctive feeling of his health and emotions, like ball-licking is a form of weird communication between members of the kind of beast you are becoming.</p></p>The other mud-drake licks your rear, and you sway it gently as your tail starts to grow in. It thickens quickly, growing dull spikes and turning a muddy, earthy brown and red. It flops into the dirt behind you, before growing thicker and longer until it is almost the same size as the rest of your body. Your skin thickens and toughens, turning splotchy with the same brown-red markings as your tail.</p></p>As your changes complete, you feel your head growing foggy and slow. You blink lazily, your thoughts sluggish and stupid as your brain shrinks, and you take on the dump, complacent mind of a domesticated beast. You are still faintly clinging to your humanity, but it\'s like you\'re incredibly drunk. The rabbit pushes the other mud-drake off you, and then starts to fasten a halter around your chest. "You still in there, bud? I hear it\'s a pretty nice feeling, having an animal brain for a while. Don\'t get too addicted though. The other guy asked me to make him go full-beast when we get to Otterton, and sell him off. I\'ll just put this ribbon on your halter so I know which of you to turn back later", he says. He carefully wraps a little red ribbon around your leather straps, and you lick it curiously. He then climbs back onto his wagon, and shakes the reins. You and your partner walk forward, your strong legs easily pulling the small wagon as you do your duty.</p></p>The day goes by easily as you pull your load and do as you are told. Your simple mind doesn\'t get bored, it just feels happy to enjoy the sun and the feel of the mud beneath your claws. When the wagon master tells you you are doing a good job, you are proud, and you wag your tail. You are looking forward to doing lots more labour and being a good domesticated drake. It\'s only when you eventually see to the town of Otterton, your destination, that you remember that you\'re supposed to be turning back to normal soon. Your slow brain considers your options for hours as you walk down the valley towards the town. Finally, you remember the ribbon. If you wanted to, you could pull it off your bridle with your flexible tongue and shove it into the side of your partner\'s harness instead. Let him turn human and get paid while you become a completely feral, stupid happy beast. Do you want to switch the ribbons?',
	    			no: 'You say no, and the rabbit sighs. "Fair enough, fair enough. I was surprised enough when this guy agreed to be one, to be honest. Well, see you around", he says, before shaking his reins and making the large beast walk off, dragging the cart behind it.',
	    			yesEffect: function(player) {
	    				player.secondaryConfirm = true;
	    				player.confirmStep = {
    		    			yes: 'For a dumb mud-drake, this act of subterfuge is incredibly smart. You are strangely excited to think about how, once you are fully transformed, you will be far too stupid to think of something like this. You lick at the ribbon, slowly unwinding it, before pushing it over to your partner\'s harness and tucking it into the strap against his neck. You then just pull your cart slowly and happily, and trundle into the plaza at the center of Otterton. Your tongue waves in the air as you taste the salty sea breeze. The rabbit hops down, and checks both your harnesses. "Huh, good job I put this here, I almost got you two mixed up", he says. You watch happily as he unbuckles the other drake, and starts rubbing his scaly head. </p></p>The drake slowly stands up, his body reshaping as scales turn to flesh. His claws become feet, his tail pulls up and disappears, and soon a very dazed and confused burly human is standing there next to the bunny. The rabbit doesn\'t seem to remember what you looked like, because he just hands over ten gold to the human. "Here you go, as agreed", the bunny says. The human just stares at the gold and nods with slight confusion. Then, the bunny unstraps you, and pets your big scaly head. You drool happily as he pulls you along by a leash around your neck, leading you to a wooden pole at the side of the plaza. He wraps the leash around it, then kneels down and lifts your huge, smelly head. "Ok, here goes. One total brain-wipe, as promised. You\'re gonna make me a nice profit, big guy. Earth-drakes are pretty expensive", he says. You feel happy to know you\'ll be a good sale. He starts to rub and massage your thick neck, and stroke the sides of your head. You feel your brain slowing down more, your thoughts dropping away as you blink stupidly. Your memories and identity fizzle and sink to the back of your mind, then disappear. You lose understanding and sentience, forgetting concepts and ideas. You don\'t know what the things around you are called anymore. You can\'t comprehend language or thoughts. You just sniff and drool instinctively, as your brain shrinks to the tiny pea-sized mind of a dumb smelly lizard.</p></p>The furry soft thing pats your head again and makes noises at you. You drool some more, and watch him walk away. The sun feels nice. You like the breeze. This is good. Good. Happy. Drool.',
    		    			no: 'You cling onto enough of your old identity to remember that you don\'t want to be a beast forever, even if it is very relaxing. You just pull your cart slowly and happily, and trundle into the plaza at the center of Otterton. Your tongue waves in the air as you taste the salty sea breeze. The rabbit hops down and checks your ribbon, then unbuckles you from your harness. You\'re not sure how he does it, but he rubs your head in such a way that you find yourself standing up, your tail receding and your body returning to its former self. You rub your head, still dazed and dopey, as he presses ten gold into your hand. "Thanks for the help, friend. I\'ll see you around!", he says, before leading the other mud-drake off for sale.',
    		    			yesEffect: function(player) {
    		    				var roomObj = World.getRoomObject(player.area, player.roomid);
    		    				
    		    				player.description = 'A huge quadrupedal brown lizard creature is here. It stinks like a barnyard, and drools with a dumb happy expression. It looks a bit like a ' + player.sex + ' ' + player.race + ' once called ' + player.name;
    		    				player.trapped = 'You don\'t want to do anything but sit and wait here. Things walk past and there\'s nice smells. The sun feels nice. You are a happy dumb lizard beast.'
    		    				
    		    				var respawnRoom = World.getRoomObject('otterton', '6');
    		    				
    		    				Room.removePlayer(roomObj, player);
    		    			
    		    				player.position = 'standing';
    		    				
    		    				respawnRoom.playersInRoom.push(player);

    		    				player.roomid = respawnRoom.id;
    		    				player.area = respawnRoom.area;
    		    			},
    		    			noEffect: function(player) {
    		    				var roomObj = World.getRoomObject(player.area, player.roomid);
    		    				
    		    				player.gold += 10;
    		    				
    		    				var respawnRoom = World.getRoomObject('otterton', '6');
    		    				
    		    				Room.removePlayer(roomObj, player);
    		    			
    		    				player.position = 'standing';
    		    				
    		    				respawnRoom.playersInRoom.push(player);

    		    				player.roomid = respawnRoom.id;
    		    				player.area = respawnRoom.area;
    		    			}
    			    	};
	    			}
		    	};
		    }
		}],
        monsters: [],
        items: []
    },{
        id: '2',
        title: 'Down the Valley',
        area: 'otterton',
		light: true,
        content: 'You are walking through the natural beauty of the grassy valley. The land rises to the north, leading to a distant farm, a golden smudge of growing wheat. To the south, it winds slowly towards the sea.',
        outdoors: false,
        exits: [
            {
                cmd: 'north',
                id: '1'
            },{
                cmd: 'south',
                id: '3'
            }
        ],
        playersInRoom: [],
        events: [{
		    "id": "Squirrel boy",
		    "moveMod": 0,
		    "description": 'There are several trees growing in the lush valley either side of the road, and you see that one of them has scattered nuts all around its base. You wonder why they haven\'t been gathered up, until you see an adventurer walk up and start collecting them. The more he gathers, the more he seems to change. He shrinks in his clothes, his pants falling around his ankles, and his shirt hanging loose. His teeth stick out in the front, and a large bushy tail starts to grow from his rear. When he\'s gathered all the ones he can find, he wriggles free from his clothes, then tries to climb up the tree to get more. His hands and feet become small furry clawed paws, and he chitters as he makes his way up to the branches, leaving his nuts abandoned. When he gets to the top, he\'s just a cute, fuzzy little squirrel.',
		    "repeatable": true
		},{
		    "id": "Grass snake",
		    "moveMod": 0,
		    "description": 'You see two adventurers discussing the dangers of the area as they walk down the road. You trail a little behind them to listen in. One of them, a magician, is explaining to her warrior friend that he should stay on the path to avoid grass snakes. "Pshaw! Grass snakes? Corgo fears no little slithery serpent!", he roars. He then jumps into the grass and swings at it with his sword, while the sorceror sighs in annoyance. Behind Corgo, a grassy hillock that you just assumed was part of the landscape starts to rise up. It uncoils, standing as a massive snake twenty feet tall and covered not with scales, but with a pelt of grass that blends into the land around it. Corgo slowly turns around when the sun is blotted out, only for the snake to open its maw and unleash a coiling vine-like tongue, which wraps him up and drags him up into the beast\'s mouth. The snake then returns to its slumber, totally concealed. A few moments later, a new flower pops up from its grassy hide, the same color as Corgo\'s tanned skin.',
		    "repeatable": true
		},{
		    "id": "Donkey suck",
		    "moveMod": 0,
		    "description": 'As you walk down the road, you hear the merry sound of a string instrument, a rapid fiddle tune that makes you bob your head. You soon discover the source of the music, as there is a donkey man sitting under a tree by the side of the road as he plays his music on a beautiful violin. You can\'t help but notice he is wearing no pants, so his huge black-and-pink donkey cock is just resting on the grass, thick and heavy. Your eyes are drawn to it the longer you listen to his song, and you are finding it tough to think straight. Do you want to listen longer?',
		    "repeatable": true,
		    "effect": function(player) {
		    	player.confirmStep = {
	    			yes: 'The donkey is handsome and cute, with his scruffy muzzle and long furry ears. If you didn\'t like donkeys before, you seem to find yourself loving them now. The longer you listen, the more his furry body looks enticing and masculine. His thick, powerful hooves. His cute tufted tail. Most importantly, though, his enormous, manly, musky cock. You are on your knees before you realise what is happening, the music filling your head and pulling you like a puppet on strings. </p></p>You press your nose to the donkey\'s huge black-skinned balls, and inhale his manly aroma. Then you start to lick and kiss his shaft, tasting his sweat and his warm flesh. You open your mouth wide as you take his cock into your throat, as deep as you can go. Your lips tease against the sensitive ring of flesh that encircles the middle of his splotchy donkey cock, and he soon brays with pleasure, the sound briefly interrupting the hypnotic song. Your mind returns to you just at the same time as a rush of thick, sticky and messy donkey cum rushes into your throat. You pull your face away, only for his cock to twitch and bob, and then spray a smelly splatter of cum all over your face and chest. The music quickly starts up again, and you grin stupidly as you stand up and walk away, allowing the cum to stay on your face and remind you of that gorgeous, wonderful donkey man.',
	    			no: 'You drag your eyes away from the handsome donkey, and walk away quickly with his music flowing through your mind.'
		    	};
		    }
		}],
        monsters: [],
        items: []
    },{
        id: '3',
        title: 'Beside the River',
        area: 'otterton',
		light: true,
        content: 'You are standing beside the bend of a river, whos babbling waters flow clear and fast. The river comes down from the mountains to the east and flows down, before bending as it hits the bottom of the valley, then flowing down towards the sea.',
        outdoors: false,
        exits: [
        	{
                cmd: 'north',
                id: '2'
            },{
                cmd: 'south',
                id: '4'
            }
        ],
        playersInRoom: [],
        events: [{
		    "id": "Cursed pole",
		    "moveMod": 0,
		    "description": 'You notice a boat floating lazily on the river, just out of reach. Inside it, a human is relaxing with a strange fishing pole. It doesn\'t look like he\'s getting many bites though. You see a ripple in the water, and suddenly an otter\'s head pops up from the depths, holding a fish in his teeth. The otter\'s eyes are swirling and spinning with colours, as he climbs into the boat and drops the fish at the human\'s feet. The human then swishes his multicoloured pole, and the otter dives back into the water to fetch him more. It looks like the pole is magic, and is hypnotising the otter into serving the fisher\'s desires.</p></p>However, there seems to be a catch with the magic. With every extra fish his otter slave brings him, the human starts to change. He kicks off his boots, and flexes his toes as they form dark webs between them. Sleek brown fur starts to grow all over his body, and he pushes down his pants, not seeming to notice the long thick rudder tail forming from his rear. Whiskers sprout from his face, and his ears become round and furry. When the otter returns to add another fish to the pile, the fisherman\'s own eyes start to glow and swirl, and he dives into the water, before nuzzling and kissing his former slave. The two of them swim away, with the fisherman leaving his boat, his pole, and his life behind.',
		    "repeatable": true
		},{
		    "id": "Cursed wishfish",
		    "moveMod": 0,
		    "description": 'You see a man standing in the water among the reeds, with a fishing pole in his hands. He is pulling and relaxing his line, making a colourful little lure bob and bounce on the water. Suddenly, the lure sinks with a splash, and he quickly tugs on his pole and draws in his prize. You see him reach into the water and pull out an enormous trout, with gleaming, glittering golden scales. As the fish wriggles and thrashes in his hands, it speaks aloud. "Please free me sir, and I will grant you any one wish!", it cries. The fisherman grins widely. "I got you at last, magic fish! I wish for the perfect lover who will satisfy all my needs!", he replies. </p></p>He then tosses the fish back, and rubs his hands eagerly. But as soon as the fish hits the water, the fisherman starts to change. His hands get thinner, and spread out into fins, while his skin starts to shine and glimmer. It cracks into scales, each golden and shiny, and he starts to gasp for air. He struggles into the water, his clothing falling off as his legs fuse together, with his feet turning into another fin. Finally he struggles free of his clothes and splashes into the water, as his body shrinks and transforms into a strong, streamlined golden trout. He bubbles with confusion in the river, until the other fish returns, and rubs against him. The two of them swim off, with the former human\'s eyes slowly turning dark and fishlike as he swims. It seems that someone certainly got their perfect lover.',
		    "repeatable": true
		},{
		    "id": "Lizard feet play",
		    "moveMod": 0,
		    "description": 'As you walk along the glittering river, you see another adventurer sit down on the bank. He is a tall green lizard, with firm rugged scales on his back and softer, pale yellow scales down his belly. He sighs, his long thick tail swaying on the grass, and starts to tug on his large brown boots. They come off with some difficulty, revealing wide and powerful scaly feet, which he gently rests in the flowing water. He stretches and splays his toes, and looks very relieved to be giving them a rest. Do you want to go over and help him with those?',
		    "repeatable": true,
		    "effect": function(player) {
		    	player.confirmStep = {
	    			yes: 'You take a seat on the grass bank next to the lizard, and he smiles at you with his short but thick muzzle. He\'s not wearing a shirt, and sweat glistens on his muscular chest. "Hey there. Just wanderin\' down the valley, and thought I\'d take a break. Walkin\' all day in these boots has left my talons achin\'", he says, before lifting one of his large feet from the river and stretching it. Before he can return it to the water, you take hold of his ankle and lift his foot onto your lap. He blinks in surprise, but then grins. "Oooh, I see what this is", he says, and turns to rest his back on a tree trunk before placing both of his dripping wet feet in your lap.</p></p>Each of the lizard\'s feet has three large, chubby toes, tipped with thick black claws. The tops of them are firm and rough with green scales, but the undersides are soft and warm. You rub your fingers through his soft soles, squeezing and rubbing your thumbs into them to relax and calm him. He growls and shudders a little with pleasure, and when you begin to rub and squeeze his toes, he reaches his hand into his pants and starts to stroke himself. You rub and massage him more, your fingers moving between his large toes, around the sides, and up and down the underside of his feet. You then lift his foot to your face, and press your mouth into it, feeling his toes curl around your head and his warm, damp foot cover your mouth and nose. </p></p>Pulling his foot down a little, you suck on one of his clawed toes, feeling the blunt black claw tease along your tongue. Your own tongue then slides between his toes, where his foot is warmest. Your face and crotch are soaked with the water from the river, but it was worth it for the experience. The lizard shudders, and lifts both of his feet to press them on your cheeks, trapping you in a warm prison of musky lizard paw. When he finishes panting, he pulls his hand free from his pants to show it is dripping with cum. "Wow, thanks. The people here sure are friendly. You can lick my feet any time, friend!", he says, before collecting his things and leaving.',
	    			no: 'You carry on with your journey, not being particularly interested in the lizard or his tired feet.'
		    	};
		    }
		}],
        monsters: [],
        items: []
    },{
        id: '4',
        title: 'Windy Path',
        area: 'otterton',
		light: true,
        content: 'The road here weaves through some tall, grassy boulders, presumably rolled down into the valley by some geographic process. They stand like silent sentinels, and in the cool shade of the path between them, lots of birds flutter and caw. A strong wind blows through the stones, making a whistling noise as it moves.',
        outdoors: false,
        exits: [
        	{
                cmd: 'north',
                id: '3'
            },{
                cmd: 'south',
                id: '5'
            }
        ],
        playersInRoom: [],
        events: [{
		    "id": "Unlucky crow",
		    "moveMod": 0,
		    "description": 'You see someone sitting on one of the smaller rocks, tossing seeds to the crows that cluster around these stones. They hop around, shoving eachother out of the way to get to the food. A few of them surround him, and one lands on his shoulder, hoping to get easier access to the seeds. He holds some up in his palm, and a crow pecks at the little pile. He doesn\'t seem to notice as with every peck the crow makes, his hand starts to grow little black pinfeathers, that spread across his skin and stretch out into full glossy crow\'s feathers. He starts to shrink, his clothes becoming baggy and loose, and his boots fall off to reveal that his feet are stretching out into long, thin grey talons. </p></p>When he gets small enough, the crows start to shove against him, trying to take the seeds from him directly. He shoves them back, and caws as his face morphs and hardens into a beak. Soon he is just another crow, fighting over the abandoned bag of seed.',
		    "repeatable": true
		},{
		    "id": "Horny wyvern",
		    "moveMod": 0,
		    "description": 'You walk between two large boulders, forming a sort of corridor with a gap above from where you can see the sky. Just as you are halfway through the gap, you hear a roar high above you, and see a dark shape in the sky lunging down. With a heavy THUMP, an enormous wyvern lands on the boulders, one of its enormous clawed feet on each rock. You can look up and see its underside, with its powerful scaly feet and enormous wing-arms. Atop its long and powerful neck, the dragonlike face of the beast screeches and roars. It kneels down, and you are surprised to see a slit between its legs open up, before an enormous and heavy black cock slides out from it. It flops between the two rocks and hangs in front of you, easily the size of your entire body. Do you wish to service the feral beast?',
		    "repeatable": true,
		    "effect": function(player) {
		    	player.confirmStep = {
	    			yes: 'You reach up and hold onto the underside of the enormous shaft, before starting to rub and squeeze against the warm flesh. The wyverns roars become lower, happier rumbles, and it humps slowly back and forth, causing its cock to rub and bounce against you. Seeing how much it likes that, you get behind the huge shaft and wrap your arms around it, then start to slide yourself up and down, using your whole body to masturbate the beast. The wyvern leans down more to make it easier for you, and the tip of its ridged and pointed cock starts to ooze a powerful-smelling flow of precum. Its humping and thrusting, and the warmth of its hard cock, rapidly makes you sticky and sweaty. The cock starts to bob and twitch powerfully after you continue to rub, and one particularly powerful flex causes you to be thrown off, leaving you lying on the ground beneath the mighty beast. At that moment, it roars and stretches out its wings in pleasure, before its massive cock unloads a powerful blast of cum all over you. The force and quantity of its orgasm drenches you in seed, as it cums over and over, splattering you from head to toe in smelly wyvern cum.</p></p>The monster pants and cranes its neck for a moment, before flapping its enormous wings and flying off. As you wipe the cum off yourself, you notice the stains on the rocks around you. It seems this creature, unable to masturbate without arms of its own, has discovered that this particular spot is a good way to get some pleasure from the horny adventurers that pass by.',
	    			no: 'The wyvern growls and stomps, but allows you to duck under its cock and leave. You look back and see it waiting as it sniffs the air and stares along the road, hoping for a more amenable assistant.'
		    	};
		    }
		},{
		    "id": "Highwaycoon",
		    "moveMod": 0,
		    "description": '"Halt!", a voice cries out, and you stop in your tracks. Looking up, you see a raccoon-man standing on one of the tall boulders, with a small crossbow in his hand. He grins, and tosses a dagger in his other hand. "Look what I caught! Another dorky adventurer, ready to get fleeced. This rural shit suits me!", he says. He then waves the crossbow at you. "Twenty gold, and I spare your life. Or if you want, you can pay me by sucking down a mouthful of raccoon spunk!", he says. His face is stretched into a crooked grin as he sheathes his dagger and then gropes his bulging crotch through his leather breeches. Do you wish to suck off the raccoon (type yes) or would you rather pay his bounty (type no)?',
		    "repeatable": true,
		    "valid": function(player, command, fn) {
		    	if (player.gold >= 20) {
		    		return true;
		    	} else {
		    		return false;
		    	}
		    },
		    "effect": function(player) {
		    	player.confirmStep = {
	    			yes: 'You reluctantly ask to suck the raccoon\'s cock, rather than pay up your hard-earned cash. He theatrically places his hand to his ear. "What was that? Say it again, and say please this time", he says. Grinding your teeth, you ask him again to let you suck his cock, with a sarcastic please at the end. He chuckles, and hops down from his rock. He keeps the crossbow bolt pointed at your head as he makes you kneel down, and walks closer to you. His long bare paws are dirty, and his fur is scruffy and unkempt. He starts to grope his bulge again, before lowering his pants to allow his cock to flop out into the air. </p></p>For a scrawny guy it\'s well-sized, the shaft hard and thick and the tip bulging as it oozes pre. Well aware of the deadly crossbow pointed at your skull, you lean forward and take the raccoon\'s shaft into your mouth. It\'s a little salty and dirty, and you can taste him along your tongue as you bob your head back and forth. His thick fluffy crotch is musky and warm, and he mocks and belittles you as you blow him. "Yeah, suck harder cumslut. You love my fat cock don\'t you?", he says, along with plenty of other slurs and insults. Finally he grips your head with his free hand and moans, as his cock pulses and unloads its hot, gooey load into your throat. He holds you down on it for a while afterwards, enjoying himself as he savours your humiliation. Then he backs off, gives you a wink, and hops back up onto his rock and out of sight before you can get revenge.',
	    			no: 'You snarl at the raccoon, but still take some gold from your pouch and toss it up to him. He counts each coin carefully, still with his crossbow pointed at you. "Thanks a bunch, sucker. Come again any time!", he says mockingly, before hopping backwards out of sight over the rocks.',
	    			noEffect: function(player) {
	    				player.gold -= 20;
	    			}
		    	};
		    }
		}],
        monsters: [],
        items: []
    },{
        id: '5',
        title: 'Hill Above Town',
        area: 'otterton',
		light: true,
        content: 'You are standing on a hill just north of a small port town. The ocean breeze blows easily over the grassy land, and it makes the air fresh and brisk. A path weaves through a small, unfinished stone wall, and leads down to the town proper.',
        outdoors: false,
        exits: [
        	{
                cmd: 'north',
                id: '4'
            },{
                cmd: 'south',
                id: '6'
            }
        ],
        playersInRoom: [],
        events: [{
		    "id": "Brick in the wall",
		    "moveMod": 0,
		    "description": 'You see a heavy-set beaver, working on the stone wall that is adjacent to the road. He lifts a huge stone brick and slots it carefully against a few others, then sighs and wipes the sweat from his forehead. He nods to you. "Howdy there, young feller. Just workin\' on the old wall here. Gonna take a long time though, we need a lot of volunteers to help with gettin\' the raw materials!" he says. You see a human adventurer jog up the hill and wave to the beaver. "Ah, here\'s one of our volunteers now!", the beaver says. </p></p>Just before the human can ask what he needs to do, the beaver grabs hold of his in his powerful arms and starts to squeeze and shove him against the wall. You watch with surprise as the struggling human\'s body is mashed and kneaded like clay. The beaver pulls at the human\'s clothes and peels them away, then rolls and kneads the blobby pink flesh some more. The adventurer\'s muffled complaints go quiet, and his features meld and blend into just a pink mass. The beaver then pats his material, straightening it into a block, before lifting his tail and sitting his bare furry ass on top of his "volunteer". You see the pink of the human\'s body slowly begin to darken and roughen, as it turns slowly into cold grainy stone. The beaver notices your surprise at the effects of his rump, and nods to you. "My granpappy was a catoblepas, so my stink tends to turn folks to stone. Pretty handy!", he says. You politely leave, once again surprised and slightly alarmed at the strange inhabitants of this land.',
		    "repeatable": true
		},{
		    "id": "Unlucky ptero",
		    "moveMod": 0,
		    "description": 'You see a man standing on top of the wall, reading from some sort of scroll. He notices you looking, and bows to you. He\'s wearing a waistcoat and has a short sword strapped to his hip. "Greetings friend! You are fortunate indeed, for you are about to witness a marvel of magical engineering! Behold, as I, the great Count Aptalon, gives birth to the magical spell of flight!" he cries flamboyantly. He then reads quickly from the scroll, the words complex and echoing. The sound seems to surround him, and you start to see sigils and runes glow as they spiral around his body. Slowly, but visibly, he begins to rise into the air and float! He laughs triumphantly, but his joy is short-lived. </p></p>His body starts to bulge and ripple strangely, and you see the front of his boots stretch out, before they split open, revealing his growing feet. His toes are stretching out into three enormous talons, covered in green scales. He blinks with shock, then cries out with surprise as his sleeves are torn open by skin growing from his chest to his arms. It forms a thick webbing, one which is replicated by the web that is growing between his fingers. Each finger stretches out, becoming thick and long, as they transform into massive scaly wings. His pants burst open at the back, allowing the growth of a muscular scaly tail. "Wh-what\'s happening? My head...I...need...food...want...mate. Eat. Mate!" he says, his voice becoming more and more guttural and beastlike. </p></p>Finally, his words devolve completely into feral screeching. His eyes turn completely black, like a shark\'s, before his hair falls out and his head stretches out into a beak-like scaly reptilian face. The new pterodactyl lands on the wall, clutching it with its huge feet, before screeching and flying away.',
		    "repeatable": true
		},{
		    "id": "Unlucky gold",
		    "moveMod": 0,
		    "description": 'You see a man digging in the hill with a large shovel. The mud caked on his boots, and the sweat dripping down his back, indicates he\'s been working on it for a while. You ask him what he\'s digging for, and he looks up from his small trench. "Oh, hey there. According to legend, there\'s a barrow in this here hill. If you can find it, you get incredible treasure!" he says. He slams his shovel hard into the ground, and there is a loud, echoing thud. With great excitement, he drops his shovel and falls to his knees, scooping the mud away with his hands to reveal a bricked-up stone construction. He heaves at one of the stones, and it slides free gradually. There is a rush of air from within, and you hear a booming voice say "YOU HAVE FOUND THE TOMB OF BRANDT. NAME YOUR WISH AND LET IT BE GRANTED!"</p></p>The man eagerly claps his hands together. "I wish for wealth and treasure!", he shouts. Immediately, a wisp of something floats up from the hole. It\'s like gold dust, swirling in the air. It surrounds the man, and starts to stick to him all over. "Huh? What\'s happening?", he says. His fingers start to stiffen and harden, locking into place as the flesh shines and gleams. A glowing golden hue covers his hands, before his fingers start to break apart into small round objects. Golden coins fall from his hands, and topple into the black hole below him. The gold spreads up his arms and onto his chest, along with the stiffness. He gurgles and moans, unable to move or speak. His arms fall off, collapsing into more golden coins, and he falls forward onto the hole. </p></p>Gradually, the golden hue spreads across his entire body, turning his legs and face into stiff, shiny metal. It then crumbles and falls apart into featureless coins, which roll and slide into the hole. Then, the stone he pulled away replaces itself, and the soil crawls back over to cover it. Soon there is no indication that anything happened at all.',
		    "repeatable": true
		}],
        monsters: [],
        items: []
    },{
        id: '6',
        title: 'Otterton Village',
        area: 'otterton',
		light: true,
        content: 'Otterton is a tiny and quaint village, existing mostly just to service the little-used port at the southern end. A few brightly-painted shops and stands surround a cicular plaza that makes up the town center, with some residential cottages down one road. The cry of seagulls and salty sea air makes the atmosphere bright and welcoming.',
        outdoors: false,
        exits: [
        	{
                cmd: 'north',
                id: '5'
            },{
                cmd: 'west',
                id: '7'
            }
        ],
        playersInRoom: [],
        events: [{
		    "id": "Rat stocks",
		    "moveMod": 0,
		    "description": 'You notice that at one end of the plaza is a raised platform, on which is a pair of old-fashioned stocks. They\'re a little odd though, as they have two sets of holes each, and are low to the ground. Just as you are wondering what they are for, you see a white rat struggling as he is dragged along by a burly otter wearing a police shirt. "It\'s stocks for you, criminal. A few hours in here will teach you not to pilfer!", the otter says. He pulls up the stocks and forces the rat to bend over. His pink paws are forced into one set of holes, before his arms are placed in the other, and the whole contraption is locked up tight. The rat struggles and curses, but he\'s unable to escape. The otter then leaves him to his fate.</p></p>It occurs to you that the helpless rat couldn\'t do anything about it if you wanted to play with his exposed pink paws. Do you want to have some fun?',
		    "repeatable": true,
		    "effect": function(player) {
		    	player.confirmStep = {
	    			yes: 'You walk up to the rat, and he looks up at you. "You here to help me out? All I did was steal some bread...and a few little rings and jewels, no one woulda missed em!", he says. You ignore his pleas, and sit down in front of his exposed paws. You take one of his feet into your hands and start to rub and stroke the warm sole, making his clawed toes wiggle and stretch. "H-hey! Whaddya doin\'?!", he cries out, but his struggling doesn\'t prevent you from having your fun. You massage your thumbs into his firm soles, and squeeze and tease his long toes, before stroking and tickling him, making him laugh and wheeze and struggle. </p></p>You press your face to his foot, sniffing it before taking a lick. Your tongue slides up and down his sole, pressing against the softer more padded parts, and slipping between his toes, making him laugh even more ticklishly. Once his feet are shiny and wet with your saliva, you press his warm paw to your crotch, and start to hump and grind against it. He blushes and squirms, but his wriggling just makes you enjoy it more. By the time you are done, he is red-faced and embarrassed, and when you walk away he calls you a weirdo, in a bashful mumble that makes you suspect he may have enjoyed himself more than he let on.',
	    			no: 'You leave the rat to his punishment, though you can hear his cursing and struggling from anywhere in the plaza.'
		    	};
		    }
		},{
		    "id": "Unlucky postbox",
		    "moveMod": 0,
		    "description": 'You see a muscular beaver berating a short, scrawny weasel who is carrying a messenger bag. "You idiot! This is the wrong mail, AGAIN! Can you do nothing right?!", the beaver roars. The weasel offers a meek retort, but the beaver has clearly had enough. He picks up the shorter male by his sides and carries him to a wooden pole by the side of the street. He then rams him down roughly, and the weasel yelps loudly as the polished pole is forced up his tailhole and deep inside him. The beaver pulls off the postal worker\'s bag and starts to pull out envelopes, which he then shoves into the weasel\'s mouth. The more he shoves in, the more the weasel starts to change. </p></p>His slender body starts to compress and spread out, inflating and becoming boxy and strange. His hair smooths out, turning into two blocky slopes atop his head. His limbs start to melt into his torso, including his long fuzzy tail. His fur flattens and smooths out, as the brown and tan becomes a shiny artificial red. His struggles slow and finally cease, while his face disappears into his body and leaves only the square hole of his new mouth. As he stiffens and transforms into wood, he is left as nothing more than a shiny new postbox. "There, now you can actually be useful!", the beaver says, before leaving the weasel to his fate.',
		    "repeatable": true
		},{
		    "id": "Fish TF",
		    "moveMod": 0,
		    "description": 'You see a dejected-looking otter walking away from the port, with one of his hands in the large pockets of his overalls, and the other holding a fishing pole. He\'s tall and muscular, with firm athletic muscle under his soft fur. You ask him what\'s wrong, and he tells you that he was really looking forward to eating some fish today, but just couldn\'t catch anything. He then looks you up and down and smiles a bit, and you notice one of his fangs is crooked, and sticks up a bit from his lower jaw. "Hey, you could help me out with that if you want?" Do you want to assist the otter?',
		    "repeatable": true,
		    "valid": function(player, command, fn) {
		    	if (player.race == "fishman") {
		    		return false;
		    	} else {
		    		return true;
		    	}
		    },
		    "effect": function(player) {
		    	player.confirmStep = {
	    			yes: 'The otter\'s eyes gleam as he takes your hand and pulls you down to the port. You assume he\'s going to get you to help him with fishing, and you follow him as he walks down some steps carved into the side of the quay, and down into the water. He\'s standing crotch-deep in a the shallow protrusion from the dock, and you follow him very carefully, your legs quickly getting soaked in the cold ocean water. You look around, and open your mouth to ask him what you need to do, when he suddenly leans forward and kisses you. His whiskers tickle your cheeks, and his furry muzzle bumps and rubs against you as his tongue rolls and licks around your own. His webbed hands grip your sides as he pulls you in close, and you can\'t help but kiss him back. As you do, your body starts to feel strange. </p></p>The water against your feet, previously cold and offputting, begins to feel comfortable and familiar. Your body cools down, and your flesh feels clammy and chilly, but in a strange way it feels good. The otter rubs his hands across your chest and your head, brushing at you, and then pulls his face away. You blink and blush, before you begin to realise something is different. You reach up to your head and feel cold, smooth skin all over your skull. Your body is completely hairless and smooth, with strange patterns in your skin almost like scales. The otter kisses your cheek, and you wriggle as your ears start to stretch and grow. They flare out and bend backwards, becoming thin and webbed as they transform into strange fins. Your face pushes out in the front, your nose and mouth becoming a sort of streamlined muzzle or beak. The otter takes your hand and kisses it, and you feel your fingers grow long and strong, before webbing grows between them. He lets himself fall back into the water, then tugs on your foot. You fall on your butt as he kisses and licks your feet, making them stretch out longer too. Webbing covers your toes as your skin becomes a greyish-blue, and you feel strange slits forming on your neck. The otter lifts your legs and pushes his head between them, where he slips his furry muzzle under your cheeks and kisses your rear. You feel your tailbone grow and shift, thickening and stretching out as it rapidly packs on flesh and muscle. It grows into a thick, long scaly protrusion, tipped with a fin, like the back-half of a trout.</p></p>Finally, the otter presses his face to your crotch. He looks up at you with a cheeky, lustful expression, as he starts to kiss and lick between your legs. You lean back and squirm, your tail and feet thrashing in the water. Your genitals feel bizarre, somehow warm and needy, yet also cold and scaly. You pant, looking down to see the otter\'s tongue lapping and licking at a strange, scaly slit between your legs. You barely have time to be shocked by your new anatomy though, because the pleasure of his ticklish whiskers and naughty tongue have driven you over the edge. Your slit erupts with white cum that fogs into the water like a cloud. The otter shoves his muzzle against you, licking and swallowing your aquatic fluids eagerly, while his tail thrashes behind him. He then leaves you panting and floating in the water, and wipes his muzzle before he winks at you. "Thanks, I told you I wanted to \'eat\' some fish!" he says, and chuckles as he swims away.',
	    			no: 'You tell the otter you can\'t help him right now, and his smile falls. "That\'s alright, I understand", he says, before walking off into the streets.',
	    			yesEffect: function(player) {
	    				player.description = 'This adventurer is a fishlike creature, with smooth scaly grey-and-blue skin. They have a long and thick tail like the rear of a fish, and their hands and feet are long and webbed. Their head has fins where the ears would be, and a set of frilly fins protrudes from their spine.';
	    				player.race = "fishman";
	    				player.size.value = 3;
						player.size.display = 'medium-sized';
	    			}
		    	};
		    }
		}],
        monsters: [],
        items: []
    },{
            id: '7',
            title: 'Witch\'s Peak Bluff',
            area: 'farm',
			light: true,
            content: 'West of the village, the coast bends up into a cliff that overlooks the crashing foamy waves below. The grass sways in a powerful wind, and the salty spray of foam manages to reach even to the cliff heights. In the distant horizon, ships float on the waters.',
            outdoors: false,
            exits: [
                {
                    cmd: 'east',
                    id: '6'
                }
            ],
            playersInRoom: [],
            events: [{
			    "id": "Brogulls",
			    "moveMod": 0,
			    "description": 'As you walk along the cliff, you hear a distant sound of bird calls coming from below. You peek along the edge of the cliff, and see that down on the sandy shore to the distance, there are two white shapes moving. Careful observation reveals that they are two seagull-men, one tall and muscular, and the other thinner and shorter. They are kissing and nuzzling eachother with their beaks, and as you watch, they lie on the sand together as the muscular one mounts the smaller seagull\'s ass. Part of you wishes you were a bit closer. It looks like quite a touching scene.',
			    "repeatable": true
			},{
			    "id": "Goblins",
			    "moveMod": 0,
			    "description": 'There is a strange sound coming from the cliffs this evening. Sort of like chanting and primitive yelling. You see the flicker of a campfire, and walk over to it. Dancing around the fire is a group of short, green-skinned humanoids with piggish faces and large pointed ears. They are jabbering at eachother and eating roasted meat, their bodies covered only by loincloths and by splatters of mud. One of them turns and snorts when you get close, and yells some sort of command. Before you can react, two of the goblins lunge at you and knock you down. You struggle under them, but despite their tiny size, they are too strong to resist. The lead goblin, who\'s skin is more of a rusty orange-red shade, walks up between your legs and sniffs you. He grins, and you blush as you realise that something about the goblins\' sweaty, dirty stink is turning you on. He moves up to your head, and straddles your chest with his short legs, before pulling away his loincloth to reveal a fat green cock that looks disproportionately large on his short body. He rams it forward, shoving it into your mouth and forcing you to suck on the sweaty green flesh. The other two goblins, excited by this, start to rub their own cocks all over your skin. The excitement seems too much for them because it isn\'t long before they are spraying thick, sticky cum all over your legs and chest. The leader finishes not long after, his cum tasting bitter and hot as it floods into your throat. They back off after that, and your mind is filled for a moment with a desire to strip down and dance around the fire. To become dumb and horny and dirty, just a stupid minion, waiting for a real monster to use you and control you. You manage to shake it off, and quickly flee before the feelings return.',
			    "repeatable": true
			},{
			    "id": "Bat TF",
			    "moveMod": 0,
			    "valid": function(player, command, fn) {
			    	if (player.race == "bat") {
			    		return false;
			    	} else {
			    		return true;
			    	}
			    },
			    "description": 'You can hear some sort of squeaking, and smell an odd musky scent coming from the edge of the cliff. You walk over and peek down to see caves that you had not noticed before. The sun is starting to set, and as the light dims, some shapes poke their heads out from the caves. They have flat pink leaf-shaped noses, tall pointy ears, and huge skin-covered wings at their sides. They are preparing to leave as the sun sets. Do you want to wait with them?',
			    "repeatable": true,
			    "effect": function(player) {
			    	player.confirmStep = {
		    			yes: 'There is a chorus of squeaks from the caves as the last of the sun\'s rays disappear over the horizon. With a flutter of wingbeats, the bat-people emerge from the cave and fly up to the top of the cliff. One of them notices you, and squeaks a command to his companions. They flutter down and land on the grass around you. Their long feet grip on the grassy soil, as they fold their wings like massive leather cloaks at their back. The tallest of them is still only about four feet tall, and he walks awkwardly over to you, clearly more comfortable in the air. He squeaks at you. Despite his odd leaf-nose, he\'s surprisingly cute. The other two bats have moved to your sides, and they are nuzzling and sniffing your sides. You feel strangely comforted by their closeness. You can imagine cuddling up against them, hanging from a cave ceiling in a furry huddle. Your fingers start to grow longer and thinner as they nuzzle you, and the skin between them stretches out into a thin fleshy web that reaches up to your fingertips. You flex and stretch your new wings experimentally, marvelling at how light you feel. The lead bat moves in closer, and you realise your nose is pressing against his. Your body feels small and light, and you close your eyes and allow him to kiss you with his long, strange tongue. Your nose smushes against his as it flattens and turns to a point. Your ears stretch out atop your head, growing tall and pink and sensitive. Soft black fur flows over your flesh, while your feet stretch out on the ground. Your toes are like long, dextrous fingers. When you open your eyes, and your bat leader steps back, you are a perfect clone of him. His beautiful leaf nose. His black fur and handsome bat face. You never realised how cute and wonderful bats really were. You are not yet ready to join the bats for good, though, and your twin understands. The bats squeak at you, and fly off into the night, leaving you standing on the grass in your new, squeaky form.',
		    			no: 'You probably can\'t trust cave-dwelling bat creatures. You back off, and see their winged shapes taking off into the evening night in the far distance.',
		    			yesEffect: function(player) {
		    				player.description = 'This player is a small and cute anthro fruitbat, with large wings and a flat leaf-shaped nose.';
		    				player.race = "bat";
		    				player.size.value = 2;
							player.size.display = 'small';
		    			}
			    	};
			    }
			},{
			    "id": "Cliff Flowers",
			    "moveMod": 20,
			    "description": "You discover some beautiful white flowers swaying atop the cliff, and take them.",
			    "repeatable": true,
			    "effect": function(player) {
			    	Character.addItem(player, {
						name: 'Widow Tears',
						short: 'a few widow tears flowers',
						long: 'A few widow tears flowers are here' ,
						area: 'farm',
						id: '115',
						level: 1,
						itemType: 'food',
						material: 'vegetable', 
						weight: 0,
						slot: '',
						value: 25,
						equipped: false
					});
			    }
			}],
            monsters: [],
            items: []
        }]
};

