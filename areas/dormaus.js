'use strict';
var Cmd = require('../src/commands').cmd,
Room = require('../src/rooms').room,
Character = require('../src/character').character,
World = require('../src/world').world;

module.exports = {
	name: 'Dormaus',
	id: 'dormaus',
	type: 'city',
	levels: 'All',
	description: 'The basic village.',
	reloads: 0,
	created: '',
	saved: '',
	author: 'Blue',
	messages: [
		{msg: 'The sound of accordion music drifts through the air.'},
		{msg: 'The smell of fresh fruit wafts through the streets.'}
	],
	respawnOn: 8,
	rooms: [
		{
			id: '1',
			title: 'Dormaus Gates',
			light: true,
			area: 'dormaus',
			content: 'The grassy road opens up here, leading to two wooden poles, bedecked with flags. This marks the entrance to the sleepy town of Dormaus, and the smell of bread and sound of lively music flows through the air as you approach.',
			outdoors: false,
			exits: [
				{
					cmd: 'north',
					id: '2'
				},{
					cmd: 'south',
					id: '1',
					area: 'farm'
				}
			],
			playersInRoom: [],
			events: [{
			    "id": "Submissive To Makarna: Female",
			    "moveMod": 0,
			    "description": 'There is a commotion at the gates of town today. A large procession of caravans and horses are set up for some sort of long trip, and muscular lion men are loading up crates and barrels. You can smell something very familiar, something that makes you feel submissive and needy deep inside. You walk over to the largest of the caravans, which is tall and surrounded by curtains and tapestries. You pull at the edge of the curtain, and immediately fall to your knees when you see your king, your lion, lounging inside on a bed of pillows. He is naked, except for his gleaming crown, and two lionesses are serving him, one of them rubbing his footpaw while the other kisses and teases his glorious cock.</p>He sees the collar around your neck, and smiles that handsome, dominant smile. "My slave. You have returned to me, because you know where you belong. You will come with me, give up this foolish life of \'adventuring\', and become my plaything. Serve me, make me happy, and exist for my pleasure. It is what you were made for." Every part of your body is aching to obey your king. Yet if you go with him, you might never return. Not even magic could save you from a life of servitude voluntarily chosen. Do you climb aboard?',
			    "repeatable": true,
			    "valid": function(player, command, fn) {
			    	var haremcollar = Character.hasEquipped(player, "haremcollar");
			    	if (haremcollar && player.race == "lion" && player.domsub == "submissive" && player.sex == "female") {
			    		return true;
			    	} else {
			    		return false;
			    	}
			    },
			    "effect": function(player) {
			    	player.confirmStep = {
		    			yes: 'You place one hand on the side of the wagon, and pull yourself up. When you are halfway up, your king leans forward, places his powerful hands around your chest, and pulls you onto him as easily as he would lift a toy. His body is lying under you, his mighty chest a bed for you to rest your weary head and forget your old life. For a moment, you look back at the town of Dormaus, your life of adventure. Then your king stretches out one magnificent footpaw and adjusts the curtain, and your old life is out of sight.</p>Inside the canopy, your master\'s musk is all-pervasive. Every breath of air is filled with the scent of him. You whimper helplessly as he holds your cheek in one mighty hand, and pulls you forward to kiss him. His powerful tongue slides around inside your mouth, his shaggy muzzle pressing against yours. You belong to him. He can do whatever he wishes to you.</p>You hear the workers finish loading up the wagons, and feel the rumble of the wheels as you start to travel. None of that matters though, the only rumble you care about is the beating of your king\'s mighty chest. He holds you and rolls you over, the other lionesses moving aside to allow a new girl to be taken and used. His weight presses down on you, as inescapable as the heat and musky, predator scent that is crushing at your mind. You arch your back and moan as his thick fur rubs against your chest, and his towering cock slides between your legs. The first thrust is slow, and careful. Inch by inch sliding into you, making you want to cry out for more, to beg to be filled. Your desires do not matter though. Only what your king wants is what matters, and he wants to take you slowly.</p>It feels like an eternity of need, your king\'s throbbing shaft sliding against your sensitive inner walls just fast enough that you shiver and whimper. Finally, he fills you, his cock deep inside you, his balls pressed against your crotch. Inside, you are exactly the right size for him. Your body is made to serve him. He pants and growls above you, his handsome face twisted into a lusty grimace, before he pulls back, and rams forward hard. Again, and again. Harder and faster, the wagon rocking with the force of his thrusts as he slams his cock against your insides. As his motions speed up, so too does his ragged breathing, until he grips your body painfully tight in his clawed hands, and his slavering maw bites down at your shoulder. His fangs press against your skin, not penetrating, but making you know you are mounted, you are owned. You feel his cock pulse and throb, before ropes of hot cum splatter and gush inside you. His hips thrust and rock as he climaxes, filling you up with his fertile seed. When the last of it oozes out into you, he pulls out, and strokes your breasts as another lioness moves over to lick him clean. "Another beautiful toy for my harem", he says with a chuckle, before turning to kiss another lioness and starting to grope her.</p>As you lie panting in bliss, your king moves on to fucking one of the lionesses. You are compelled to nuzzle and rub against him while he takes his pleasure from her. After hours of play, you all snuggle up together at the sides of your king, and settle in to sleep.</p></p>In the morning, you are woken by your king licking and nuzzling your neck, while stroking your breasts in one of his mighty hands. These experiences continue for days. Your king uses every part of you, his cock fills your mouth, your tailhole, everywhere, and his groping hands experience every touch of your skin. When he is not using you, he is pleasuring himself with one  of the lionesses, and you are commanded to kiss his feet or rub his mighty fur. Days pass, travelling in the wagon, and meals are brought to you by the muscular and sweaty lion workers, who gaze at you lustfully. After a few weeks of service, the wagons come to a stop, but you do not leave your master\'s side. The entire canopy-covered section of the wagon is lifted up and carried away, so you never see what is happening outside. You do not need to know that. You are too busy with your muzzle around your king\'s shaft, as he slams his cock deep into your face. The days after that are colder, and take place with swaying and rocking motions. If you were not so distracted by servitude, you would realise you were on a boat.</p></p>Over the following months, the king sometimes leaves his plush cushion-filled cabin. When he does, you are commanded to wait for him, and the lack of his presence fills you with a desperate need to have him back. Your life as an independant adventurer is like a distant dream. Such a long time has passed with your mind washed clean by hypnotic musk, that you have lost all willpower and free thought. Each time your king returns, your thoughts disappear and you are overcome with love and obedience once more.</p></p>The weather begins to warm up, over time, and one day you awaken to find your king is not with you. The cabin is no longer rocking and swaying, and the lack of motion is disorienting after months at sea. You wait loyally for your owner to return, but instead, the curtains part to reveal tall and dirty lion workers. They round up you and the other harem slaves, and place beautiful silver chains on your collars. One of them gives a firm tug on your chains, and you follow him meekly out into the sunlight, for the first time in months.</p></p>The sun is bright and the very air around you is sizzling hot. Your lioness body seems to respond to the heat, and you feel a strange sense of belonging. When your eyes adjust, you see that you are walking down the gangplank into a strange new city. The roads are pounded sand, and the buildings are yellow brick and stone, in the shape of step pyramids. You are led forward through the streets, which are flanked with tall palm trees and wandering people of all races. Most common, though, are the lions. Some on two legs, some on four, and some with four legs but another torso above it, like a leonine centaur. None pay any mind to the procession of naked lionesses on chains. You are led to the tallest and most magnificent ziggurat, the tip of which gleams bronze in the sunlight. You climb many sandy stone steps, until you are high above the city, and from there you can see far into the distance.</p>This new city is large, and busy, completely dwarfing the little town of Dormaus. Surrounding it, as far as the eye can see, are dusty plains, flat and hot. You take it all in, before you are guided into the pyramid. You walk through hallways inlaid with gold and ivory, with so many twists and turns that you are quite lost. Finally, you are led into a wide open room, with a tinkling fountain in the center, and blankets and cushions strewn about. A huge balcony looks down over the city, and you and the other harem slaves are left here as the workers depart.</p>Your life now is one of a harem slave, pampered but trapped in a gilded cage.',
		    			no: 'You back off, holding your breath so as not to breathe in more of that hypnotic manly scent. The lion\'s gleaming teeth shine in a smile in the darkness. "We will be preparing all day, my slave. Return soon. I know you will."',
		    			yesEffect: function(player) {
		    				player.master = "Leonar";
		    				
		    				var roomObj = World.getRoomObject(player.area, player.roomid);
		    				
		    				var respawnRoom = World.getRoomObject('savannah', '1');
		    				
		    				Room.removePlayer(roomObj, player);
		    			
		    				player.position = 'standing';
		    				
		    				respawnRoom.playersInRoom.push(player);

		    				player.roomid = respawnRoom.id;
		    				player.area = respawnRoom.area;
		    			}
			    	};
			    }
			},{
			    "id": "Submissive To Makarna: Male",
			    "moveMod": 0,
			    "description": 'There is a commotion at the gates of town today. A large procession of caravans and horses are set up for some sort of long trip, and muscular lion men are loading up crates and barrels. You can smell something very familiar, something that makes you feel submissive and needy deep inside. You walk over to the largest of the caravans, which is tall and surrounded by curtains and tapestries. You pull at the edge of the curtain, and immediately fall to your knees when you see your king, your lion, lounging inside on a bed of pillows. He is naked, except for his gleaming crown, and two lionesses are serving him, one of them rubbing his footpaw while the other kisses and teases his glorious cock.</p>He sees the collar around your neck, and smiles that handsome, dominant smile. "My slave. You have returned to me, because you know where you belong. You will come with me, give up this foolish life of \'adventuring\', and become my plaything. Serve me, make me happy, and exist for my pleasure. It is what you were made for." Every part of your body is aching to obey your king. Yet if you go with him, you might never return. Not even magic could save you from a life of servitude voluntarily chosen. Do you climb aboard?',
			    "repeatable": true,
			    "valid": function(player, command, fn) {
			    	var haremcollar = Character.hasEquipped(player, "haremcollar");
			    	if (haremcollar && player.race == "lion" && player.domsub == "submissive" && player.sex == "male") {
			    		return true;
			    	} else {
			    		return false;
			    	}
			    },
			    "effect": function(player) {
			    	player.confirmStep = {
		    			yes: 'You place one hand on the side of the wagon, and pull yourself up. When you are halfway up, your king leans forward, places his powerful hands around your chest, and pulls you onto him as easily as he would lift a toy. His body is lying under you, his mighty chest a bed for you to rest your weary head and forget your old life. For a moment, you look back at the town of Dormaus, your life of adventure. Then your king stretches out one magnificent footpaw and adjusts the curtain, and your old life is out of sight.</p>Inside the canopy, your master\'s musk is all-pervasive. Every breath of air is filled with the scent of Him. You whimper helplessly as he holds your cheek in one mighty hand, and pulls you forward to kiss him. His powerful tongue slides around inside your mouth, his shaggy muzzle pressing against yours. You belong to him. He can do whatever he wishes to you.</p>You hear the workers finish loading up the wagons, and feel the rumble of the wheels as you start to travel. None of that matters though, the only rumble you care about is the beating of your king\'s mighty chest. He holds you and rolls you over, the other lionesses moving aside to allow the new slave to be taken and used. His weight presses down on you, as inescapable as the heat and musky, predator scent that is crushing at your mind. You arch your back and moan as his thick fur rubs against your chest, and his towering cock slides between your legs. He twists you around, and you feel his cock against your tender hole. The first thrust is slow, and careful. Inch by inch sliding into you, making you want to cry out for more, to beg to be filled. Your desires do not matter though. Only what your king wants is what matters, and he wants to take you slowly.</p>It feels like an eternity of need, your king\'s throbbing shaft sliding against your stretched insides just fast enough that you shiver and whimper. Finally, he fills you, his cock deep inside you, his balls pressed against your ass. Inside, you are exactly the right size for him. Your body is made to serve him. He pants and growls above you, his handsome face twisted into a lusty grimace, before he pulls back, and rams forward hard. Again, and again. Harder and faster, the wagon rocking with the force of his thrusts as he slams his cock against your insides. As his motions speed up, so too does his ragged breathing, until he grips your body painfully tight in his clawed hands, and his slavering maw bites down at your shoulder. His fangs press against your skin, not penetrating, but making you know you are mounted, you are owned. </p>You feel his cock pulse and throb, before ropes of hot cum splatter and gush inside you. His hips thrust and rock as he climaxes, filling you up with his fertile seed. When the last of it oozes out into you, he pulls out, and strokes your chest as a lioness moves over to lick him clean. "Another beautiful toy for my harem", he says with a chuckle, before turning to kiss a different lioness and starting to grope her.</p>As you lie panting in bliss, your king moves on to fucking one of the lionesses. You are compelled to nuzzle and rub against him while he takes his pleasure from her. After hours of play, you all snuggle up together at the sides of your king, and settle in to sleep.</p></p>In the morning, you are woken by your king licking and nuzzling your neck, while stroking your breasts in one of his mighty hands. These experiences continue for days. Your king uses every part of you, his cock fills your mouth, your tailhole, everywhere, and his groping hands experience every touch of your skin. When he is not using you, he is pleasuring himself with one  of the lionesses, and you are commanded to kiss his feet or rub his mighty fur. Days pass, travelling in the wagon, and meals are brought to you by the muscular and sweaty lion workers, who gaze at you lustfully. After a few weeks of service, the wagons come to a stop, but you do not leave your master\'s side. The entire canopy-covered section of the wagon is lifted up and carried away, so you never see what is happening outside. You do not need to know that. You are too busy with your muzzle around your king\'s shaft, as he slams his cock deep into your face. The days after that are colder, and take place with swaying and rocking motions. If you were not so distracted by servitude, you would realise you were on a boat.</p></p>Over the following months, the king sometimes leaves his plush cushion-filled cabin. When he does, you are commanded to wait for him, and the lack of his presence fills you with a desperate need to have him back. Your life as an independant adventurer is like a distant dream. Such a long time has passed with your mind washed clean by hypnotic musk, that you have lost all willpower and free thought. Each time your king returns, your thoughts disappear and you are overcome with love and obedience once more.</p></p>The weather begins to warm up, over time, and one day you awaken to find your king is not with you. The cabin is no longer rocking and swaying, and the lack of motion is disorienting after months at sea. You wait loyally for your owner to return, but instead, the curtains part to reveal tall and dirty lion workers. They round up you and the other harem slaves, and place beautiful silver chains on your collars. One of them gives a firm tug on your chains, and you follow him meekly out into the sunlight, for the first time in months.</p></p>The sun is bright and the very air around you is sizzling hot. Your lioness body seems to respond to the heat, and you feel a strange sense of belonging. When your eyes adjust, you see that you are walking down the gangplank into a strange new city. The roads are pounded sand, and the buildings are yellow brick and stone, in the shape of step pyramids. You are led forward through the streets, which are flanked with tall palm trees and wandering people of all races. Most common, though, are the lions. Some on two legs, some on four, and some with four legs but another torso above it, like a leonine centaur. None pay any mind to the procession of naked lionesses on chains. You are led to the tallest and most magnificent ziggurat, the tip of which gleams bronze in the sunlight. You climb many sandy stone steps, until you are high above the city, and from there you can see far into the distance.</p>This new city is large, and busy, completely dwarfing the little town of Dormaus. Surrounding it, as far as the eye can see, are dusty plains, flat and hot. You take it all in, before you are guided into the pyramid. You walk through hallways inlaid with gold and ivory, with so many twists and turns that you are quite lost. Finally, you are led into a wide open room, with a tinkling fountain in the center, and blankets and cushions strewn about. A huge balcony looks down over the city, and you and the other harem slaves are left here as the workers depart.</p>Your life now is one of a harem slave, pampered but trapped in a gilded cage.',
		    			no: 'You back off, holding your breath so as not to breathe in more of that hypnotic manly scent. The lion\'s gleaming teeth shine in a smile in the darkness. "We will be preparing all day, my slave. Return soon. I know you will."',
		    			yesEffect: function(player) {
		    				player.master = "Leonar";
		    				
		    				var roomObj = World.getRoomObject(player.area, player.roomid);
		    				
		    				var respawnRoom = World.getRoomObject('savannah', '1');
		    				
		    				Room.removePlayer(roomObj, player);
		    			
		    				player.position = 'standing';
		    				
		    				respawnRoom.playersInRoom.push(player);

		    				player.roomid = respawnRoom.id;
		    				player.area = respawnRoom.area;
		    			}
			    	};
			    }
			},{
			    "id": "Reluctant Horse",
			    "moveMod": 0,
			    "description": 'You are walking through the gates of the town when you hear stomping and whinnying. You look around to see Turner, the horse man, patting the flank of a large quadrupedal horse, who is shifting and stomping with annoyance. Turner speaks gently. "Woah there boy! You agreed ta help me with a month \'a labour! You\'ll get used ta hooves, I promise." It seems this horse used to be an adventurer like yourself. Turner\'s rough hand reaches under the horse\'s belly and starts to stroke him, encouraging the former adventurer\'s sheath to release his heavy, splotched horse cock. He grips and strokes it, and the horse whinnies a little. "See boy? Bein\' a horse ain\'t so bad!" You almost feel a little jealous...',
			    "repeatable": true
			},{
			    "id": "AP Carpenter",
			    "moveMod": 0,
			    "description": 'You hear a racket coming from the flagpoles, and look up to see Mikhail the dog hammering fresh nails into the sign bearing the name of the town. His shirt is off as he works, showing off his strong muscle gut and big, hairy forearms. He shouts some orders to another dog on the other side, who looks very much like him, though younger and skinnier. </p>"Boy, get your head in the game! You\'re gonna make this whole sign fall down." Mikhail sighs as the other dog stumbles, and drops his nails. "Boy, I need a man to do this job right." He clicks his fingers, and the smaller dog growls and shudders. His body grows larger, making his shirt and clothes creak, and he rips his shirt off with arms that are thickening and getting rougher. His soft fur becomes thick and shaggy, and his lithe chest develops hard muscle, which is then softened by a layer of pudge. His face becomes rugged, as his fur forms grey streaks and he grows a bristly beard across his cheeks. </p>He laughs in a deep, mature voice. "Well pops, guess I should call ya brother now!" he says. Mikhail nods, and they get back to work.',
			    "repeatable": true
			},{
			    "id": "Ace Pissing",
			    "moveMod": 0,
			    "description": 'You pass Ace the coyote on the way through town, who\'s boots are coated in thick mud. His leather duster is tattered at the hems, and he grumbles as he strokes a revolver at his side. "Howdy, partner. Just got back from that blasted cave. Slimes in there don\'t give two shits about my bullets, and they ruined my fuckin\' coat." he growls. He spits at the side of the street, then walks over behind a tree by the side of the road. He adjusts his belt, then pulls down his chaps and trousers to pull out a thick and dark-coloured cock with a thick sheath. </p>He sighs as he relieves himself with a stream of steaming piss on the tree\'s roots, then when he is done, grabs his cock and starts to stroke it. He grunts and pants, grinding his boot into the dirt as he jerks off. His furry muzzle hangs open, and he shudders with relief, his cock jerking and spurting strange, sticky green cum that splatters over the tree and glows faintly. Huge amounts of it stream and spurt from his cock, until half the tree is coated in slime. He sees you watching and chuckles. </p>"When bullets don\'t work, I got a few other tricks to get folks outta my way." he says. He winks at you, pulls his pants back up, and heads towards the bar.',
			    "repeatable": true
			},{
			    "id": "Flute's Gift",
			    "moveMod": 0,
			    "description": 'You look around the gentle pastoral road leading up to the town. It seems the road here gets a lot of traffic, and some of said traffic approaches while you\'re waiting around. A slender fox man is hiking up to the gate, his back loaded heavily with a huge backpack, from which various strange trinkets and fabrics are sticking out into the air. When he sees you, he grins a wide vulpine smile and drops his bag.</p>"Well hello there! Let me guess, a new adventurer? What you need is an adventurer\'s kit!" The fox roots around in his bag and pulls out a wrapped package, which he tosses to you. "The name\'s Flute, and helping people out is my second favourite thing to do!" </p>Do you want to accept the package?',
			    "repeatable": true,
			    "effect": function(player) {
			    	player.confirmStep = {
		    			yes: 'You nod happily and take the gift, intending to open it later. But Flute moves forward, grinning, and you blink in shock as he slides down his green pants to reveal a thick, long cock that stands erect in the air. "Great! Since I did a favour for you, now you\'ll do a favour for me."</p>Whether or not you want to, you find yourself being pushed down to your knees. Your palms are tingly and you can\'t seem to resist Flute\'s actions. Was there something in that package? It doesn\'t matter, the only package you can seem to care about is the enormous one between the legs of this handsome fox. </p>Your mouth opens as Flute thrusts forward, sliding his cock over your tongue and deep into your throat. He thrusts in and out, grinning and breathing heavily while your nose bounces against the pale orange fur of his pubes. His clawed hands grip harder on your head as he shudders and moans. </p>You feel his cock pulse, before thick, hot cum floods your mouth. You swallow every drop of his salty fluids, as it is pumped into you in thick and heavy spurts. Flute then pulls out, leaving you craving more. He pulls his pants off, gives you a wink, and leaves. A little while later, you finally recover your senses and manage to get up off the ground.</p>You obtained: a shirt, some panties.',
		    			no: 'The fox raises his eyebrows and shrugs. "Ah well, I\'m sure someone else will love it!" He takes the package from you and heads into town, his tail flicking to and fro as he departs.',
		    			yesEffect: function(player) {
		    				Character.addItem(player, {
		    					name: 'T shirt', 
		    					displayName: 'T shirt',
		    					short: 't shirt',
		    					long: 'There is a cotton t-shirt here. It is white, with red text on the front that says "FLUTE\'S BITCH"' ,
		    					area: 'dormaus',
		    					affects: [],
		    					id: 'fluteslaveshirt', 
		    					level: 1,
		    					itemType: 'armor',
		    					material: 'cotton', 
		    					ac: 1,
		    					value: 20,
		    					weight: 1,
		    					slot: 'body',
		    					equipped: false
		    				});
		    				Character.addItem(player, {
		    					name: 'Panties',
		    					displayName: 'Panties', 
		    					short: 'pair of panties',
		    					long: 'There is a pair of panties here. They are pink with white trim.' ,
		    					area: 'dormaus',
		    					id: 'panties', 
		    					level: 1,
		    					itemType: 'armor',
		    					affects: [],
		    					material: 'cotton', 
		    					ac: 1,
		    					value: 20,
		    					weight: 1,
		    					slot: 'legs',
		    					equipped: false
		    				});
		    			}
			    	};
			    }
			}],
			monsters: [{
				name: 'Lin',
				level: 15,
				short: 'Lin',
				long: 'Lin the tutorial bunny is here, next to a sign that says TYPE "ASK LIN HELP" IN THE TOP FORM FIELD NEAR THE BOTTOM OF THE PAGE.',
				description: 'Lin is a white rabbit, wearing a blue-and-white striped top hat with a jiggling question mark atop it. She is wearing a waistcoat and sitting on a stack of books',
				inName: 'Lin',
				race: 'rabbit',
				id: 51,
				area: 'dormaus',
				weight: 245,
				diceNum: 2,
				diceSides: 8,
				diceMod: 5,
				meleeRes: 10,
				str: 16,
				position: 'standing',
				attackType: 'kick',
				damRoll: 10,
				hitRoll: 10,
				ac: 20,
				gold: 1000,
				merchant: true,
				wanderCheck: 38,
				itemType: 'mob',
				runOnAliveWhenEmpty: false,
				behaviors: [],
				items: [{
					name: 'Tutorial Lollipop',
					short: 'a tutorial lollipop',
					long: 'A tutorial lollipop was left here! Shocking.' ,
					area: 'dormaus',
					id: '413',
					level: 1,
					itemType: 'food',
					material: 'candy', 
					weight: 0,
					slot: '',
					value: 5,
					equipped: false,
					spawn: 3,
					onEat: function(player, roomObj, item) {
						var sparklespell = {
					            "id": "sparkle",
					            "display": "Sparkle",
					            "mod": 0,
					            "train": 100,
					            "type": "spell",
					            "wait": 0,
					            "learned": true,
					            "prerequisites": {
					                "level": 1
					            }
					        };
					    	Character.addSkill(player, sparklespell);
						
						World.msgPlayer(player, {
							msg: 'You feel a strange power flowing through you from your belly up to your mind. You now know the secrets of the Sparkle spell!',
							styleClass: 'cmd-drop blue'
						});
					}
				}],
				topics: {
					help: '"Hello new player! If you ever need to repeat this message, type ASK LIN HELP again in this location. First things first: type ASK LIN and press enter, to see a list of my help topics. When you are done, type ASK LIN BASICS to get more help."',
					basics: '"Ok, so this is a big gay game full of sex and tf and stuff. There might be non-gay stuff later, but Blueballs will need someone else to write that! To move around, either press the N W E S buttons in the top left, or type stuff like "north" into the top box. Try going NORTH and then SOUTH to get back to me, then type ASK LIN TALKING."',
					talking: '"Talking to NPCs like me is with the ASK commands you have been typing. ASK [NAME] will give you a list of topics, then you can do ASK [NAME] [TOPIC] for dialogue. To talk to other human players, type anything you like in the bottom-most form field. Other players in your room will see it. For more commands, type ASK LIN EXPLORING"',
					exploring: '"The main objective of the game is to explore and find sex or transformation scenes. You do this by typing EXPLORE in any room. Each room has different events. Try exploring in this room. In one of the events you will meet a fox named Flute. Say yes to his request for some items, and then move on to the ASK LIN SHOPPING topic! If you do not see him, just keep exploring until you do."',
					shopping: '"Some NPCs have items and can buy or sell stuff. Like me! Try typing SELL PANTIES or SELL SHIRT and I will take those off your hands and give you some money! Do not worry, you can get more by exploring again. Some events are once-only, but this is a repeatable one. When you have gotten some gold, type ASK LIN BUYING"',
					buying: '"To check your gold or other stats, type SCORE or press the SCORE button on the left. Your gold is listed there. I have an item to sell you! Try typing BUY LOLLIPOP. To figure out what an NPC has for sale, try typing LIST [NPC] in their store, or asking them things. The next part of my tutorial can be seen with ASK LIN FOOD"',
					food: '"The lollipop I gave you is food. It has a special effect, too! The effect will wear off if you log out and in before eating it, or wait too long, but you can always just repeat the tutorial to get a new one. Try typing EAT LOLLIPOP. Some stuff is drinks instead - thats the same buy with DRINK [POTION]. Eat your item, then move on to ASK LIN SPELLS"',
					spells: '"If your lollipop was not too old, you should have gotten a message about learning the sparkle spell. Type SKILLS to see your list of spells. You can cast sparkle with CAST SPARKLE. Some spells have a target required - for them type CAST [SPELL] [TARGET]. Saberhagen the cat in town sells several spells! Next up is ASK LIN OTHER"',
					other: '"There are a few other interesting commands. LOOK [THING] gives you a description. Try LOOK LIN. To emote to other players try EMOTE [something]. You can wear stuff with WEAR [ITEM], if you still have those panties. If you are vain, try TITLE [something] to give yourself a personal title! You can fight with KILL [thing] but at the moment it is just an annoyance and has no real effect. There is also GET and DROP for items lying around. There is more stuff if you just type HELP for a full list. Next up ASK LIN STUCK"',
					stuck: '"This game was written by a guy who is into inanimate tf, and part of the fun of that is being stuck and unable to escape from some embarrassing fate. Most of the transformations that make you stuck you can avoid. When you type EXPLORE you may get a confirmation dialogue. Read carefully and type YES or NO to get the event or avoid it. If you do get stuck, you can be freed if someone casts the Rescue spell on you. If you are REALLY stuck, type RESTART to start the game over! Next is ASK LIN EVENTS"',
					events: '"Events are tricky sometimes. They can move you around, give you items, or transform you. Sometimes you may think you have seen all events in an area, but there are sometimes secret ones! For example, exploring in the market has a special event for mice. If you are still unsure what to do, try ASK LIN CONFUSED"',
					confused: '"Sorry to hear that! There is no real objective here other than to explore and experience. Maybe try going north twice to the bar, and explore there to turn into a bear! Or in the south there is a horse who would love to meet you. You can RP with other players too! The most important thing is to masturbate in real life. Up next is ASK LIN CONTRIBUTING"',
					contributing: '"Interested in adding new content to the game? Contact the author at blue34cat.tumblr.com or on Furaffinity as Blueballs. He would love to add your stuff! Next is ASK LIN BYE"',
					name: '"I am Lin, the tutorial bunny! I am here for helping new players who might be confused."',
				    job: '"I sit here and tell people how to play the game! I am technically non canon so I do not have any sex events, though. Unless someone wants to write one for me!"',
				    bye: '"I hope I have been helpful! Have fun and remember, you can restart this tutorial with ASK LIN HELP"',
				    cock: '"It\'s a trap!"',
					paws: '"You might have more luck with the other NPCs. I am non canon!"'
				}
			}],
			items: [],
			beforeEnter: function(roomObj, fromRoom, target) {
				return true;
			},
			onEnter: function(roomObj, target) {
				
			}
		},
		{
			id: '2',
			title: 'Streets of Dormaus',
			area: 'dormaus',
			light: true,
			content: 'The streets of Dormaus are paved with smooth cobblestones, and the buildings around you are charming thatched cottages with colourful flowers blooming from every windowsill, and from the many hanging baskets. You can see a plaza to the west, and a large multi-storey pub to the north. A set of wooden stairs lead up to a second-floor general store.',
			outdoors: false,
			exits: [
				{
					cmd: 'north',
					id: '3'
				}, {
					cmd: 'west',
					id: '7'
				}, {
                    cmd: 'south',
                    id: '1'
                }, {
                	cmd: 'up',
                	id: '5'
                }
				
			],
			playersInRoom: [],
			monsters: [],
			items: [],
			events: [{
			    "id": "Chocobo Merchant",
			    "moveMod": 0,
			    "description": 'One of the merchants travelling through town is riding a strange beast, a huge yellow-feathered bird with a long neck and huge clawed feet. The bird is struggling and panting, and the human merchant in the saddle atop it is getting impatient. You jump with surprise as Flute the fox appears, a wide grin on his face and a glowing green potion in his hand. "Hello there traveller! I see your bird is struggling, well this potion is a rare one guaranteed to recover the stamina and strength of all mounts! Only two hundred gold!" he says. The merchant looks skeptical, but tosses a bag of gold to Flute, instructing him gruffly that he will pay dearly if this is a scam. </p>Flute walks up to the exhausted bird, pats its cheek, and guides the potion to its beak. The effects are rapid – a glow envelops both the bird and his rider, and the bird bucks in the air, throwing the merchant to the ground. The bird\'s feathers start to recede, revealing tanned, smooth skin. His legs shrink down, and his small wings stretch out, feathers turning to fingers and bones forming new muscle. His beak shrinks and softens into a handsome, surprised face. Meanwhile, the merchant squawks and scrabbles on the ground, his boots tearing open to reveal massive talons. His arms bend and shrink, as feathers sprout all over his skin, yellow and soft. His neck stretches up, and his face hardens into a wide, powerful beak. He squawks and yelps, but the naked former-bird just grabs the fallen saddle and straps it to the merchant\'s back. </p>"Many thanks, fox! Here, take the rest of my old master\'s cash. You earned it." he says. Flute catches a bag of gold as the former-bird tosses it, and watches the role-reversed pair trot off to market. "Another satisfied customer", he quips.',
			    "repeatable": true
			},{
			    "id": "Scarecrow Thief",
			    "moveMod": 0,
			    "description": 'There is a new feature outside Mikhail\'s shop today. A grinning, poorly-made scarecrow of sorts, with tattered clothes, and straw sticking out of his fabric \'skin\'. You walk over, and see that under his old hat, the face of the scarecrow is human! He whimpers at you, and you look down to see that the scarecrow\'s pole is sticking out from his rear, where the pants are torn open. </p>His body shudders and shifts, his head becoming round and simplistic, until his face transforms into an unmoving, painted-on smile. Once the scarecrow stops moving, Mikhail emerges from the store and hangs a sign on his neck. It reads \'THIEVES GET PUNISHED\'.',
			    "repeatable": true
			},{
			    "id": "Skunk Perfume",
			    "moveMod": 0,
			    "description": 'There is a market stall set up in the corner of the street today. It\'s an unusual place for a merchant, quite far from the rest of the market square. When you wander over, you see a huge array of colourful and twisty perfume bottles for sale. The merchant is a handsome skunk, stocky and grinning. His thick striped tail sways to and fro, blowing around the mixture of strange scents coming from the bottles and samples. "Gehehe! A new customer! Would you like to sample one of my perfumes? I have scents for all seasons!" he says. Do you want to take a sniff?',
			    "repeatable": true,
			    "effect": function(player) {
			    	player.confirmStep = {
		    			yes: 'What\'s the harm? It\'s not like this town is cursed and full of dangerous sexual scenarios. You let the skunk uncork a bottle and waft it under your nose. It smells very odd. Sort of musky, and earthy, and sweaty in a masculine, humid way. You find yourself drooling and blushing, your body tingling with a desperate need. When the skunk corks up the bottle again, you clutch the edge of his stand. Your mouth is dry and you desperately need more. You reach out for the bottle, but the skunk pulls it away. "Ah ah! Sorry hun, but that one is not for sale right now. I can do you a favour though! You can have some straight from the source." he says. </p>The skunk stands up and drops his pants, before lifting his tail and bending over, showing off his fluffy black-furred rear. Your body moves on its own, as you walk behind the stall and press your face into the skunk\'s warm, musky rear. His tail lowers down over your head as you inhale the sweaty aroma from his fur. You start to kiss and lick his tailhole, nuzzling and making out with it like his butthole is your lover. Your tongue tastes his salty rear, and your lungs are filled with his scent. You remain behind the skunk\'s stall, licking and nuzzling his butt for the entire day, until he finishes selling his goods and packs up to go. He frees you, gives you a flick on the nose, and heads on his way. You lie on the ground for a while longer, slowly coming down from your bizarre rimjob-high, before you finally regain control of your senses.',
		    			no: 'You politely decline, and walk away while wafting your nose from the mixture of scents, which make you dizzy and confused.'
			    	};
			    }
			},{
			    "id": "Tossed From Bar",
			    "moveMod": 0,
			    "description": 'A loud crash rumbles from the pub, and the doors suddenly fly open, as a human is tossed violently through them into the street. Grizz the bear, wearing nothing but an apron, dusts off his hands and growls at the prone, dizzy figure. "No fightin\'." he says, with gruff finality. </p>You look down at the human to see his pants have been torn open, and his ass is leaking thick, sticky cum that stinks of Grizz\'s masculine musk. From the pained but confused expression of pleasure on the barely-conscious man\'s face, you are not sure if he considers his \'punishment\' to be a good thing or not.',
			    "repeatable": true
			},{
			    "id": "Cobble1",
			    "trapevent": true,
			    "moveMod": 0,
			    "description": 'Being just a rock in the road takes some getting used to. People pay you no attention at all. It doesn\'t bother you though – some part of your new form accepts and enjoys just being a part of the scenery, underfoot. The rain washes over you, dries off, and you get to feel the many passers-by step on you. </p>Horses clop across the street, their hooves hard and firm, slipping slightly on your surface then firmly pressing down against you, and grinding you down. Then there are the soft paws of adventuers and merchants. Fluffy bunny soles like a pillow on your face, and harder yet still squishy pawpads holding down on you. It\'s like a lovely, gentle massage. Knowing that each step on you is grinding down a little more of your form and making you closer to being a smooth, ordinary rock, is very exciting.',
			    "repeatable": true
			},{
			    "id": "Cobblestone TF",
			    "moveMod": 0,
			    "description": "As you walk along the cobblestone streets of Dormaus, something catches your eye. Or rather, your foot. You feel a strange shifting under one foot, and you look down to see that the large, flat cobblestone you are stepping on looks unusual. You kneel down, and see to your surprise that the cobblestone has a face! </p>Engraved in the stone is a blank and smiling face, what looks almost like a flattened fox. In fact, on closer inspection, a lot of the stones here have faces. Most are worn from what must be decades or centuries of being stepped on, so they vary from fresh and obvious like this fox, to perfectly smooth like any other stone. </p>You are kneeling down, stroking and touching the stone face, curious and confused, when a shadow falls over you. You look up to see the mayor, smiling with a friendly grin. </p>'Ah, hello my friend! Admiring the stones, are we? Our wonderful town has a special tradition, and even the streets of this little slice of heaven are paved with people who loved this town so much, they wanted to contribute to it as the stones underfoot!' You are taken aback by this, having suspected that these stones may have been people, but still being surprised to hear it confirmed. The mayor continues. </p>'It's a good time, you know. Permanent of course, but you get to be a part of this town, a fundamental piece of the bedrock. Seeing the people coming and going, having the sun shine down on you, serving an important purpose. Would you be interested in joining them?' For such a shocking question, the portly cat says it with nonchalance that makes it sound like he's simply offering you a part-time job! Could you even consider giving up your life of adventure to be a simple cobblestone?"
			    	+" </p>WARNING: This will PERMANENTLY turn your character into a piece of scenery! You will need to make a new character, but your current one will exist in this location to be admired.",
			    "repeatable": true,
			    "effect": function(player) {
			    	player.confirmStep = {
		    			yes: "You can't believe you're doing this. You hesitate, hum, mull over it, but then finally nod. The cat's eyebrows rise, and he chuckles with delight. </p>'Goodness me! I'm so pleased that you love our town so dearly! You do us a great honour. Now just strip down and lay on your back, I will do the rest!' You carefully remove your armor and place down all your belongings. Rocks don't need any of that stuff, of course. The mayor slides them away with one of his footpaws, probably to add to the donation box later. </p>You lie down in the street naked, the bright morning sun beating down on you. You actually feel surprisingly relaxed. Your fear and hesitation about this crazy decision seems to be almost melting away. You cross your arms behind your head and just stare up into the sky, breathing deeply and relaxing. You feel no shyness about lying naked in the street. In fact, you are so relaxed that you don't even notice when you stop breathing. </p>Your skin feels smooth, and firm, and hard. Your body feels slow and heavy. You probably could move, or wriggle, but you don't want to. Your natural twitches and motions slow down, to perfect stillness. Your eyes stare unblinkingly as your skin starts to turn grey, your insides hardening and stiffening. </p>When the mayor lifts one of his furry paws and walks across your belly, you don't react. You feel no pain or discomfort, and your flesh is as hard as stone. He walks over you again, stepping on your face and walking down your chest and off. Each time, you remain hard and firm, but you shrink and flatten a little more. His feet seem to get larger, covering more of your body and blocking out your vision a little more. </p>Over and over, and you just calmly sit and take every step of his feet. You don't know how long this lasts, but eventually, he looks down at you with a smug grin, walks off, and leaves you be. You can feel the other stones around you. You can't move at all, you can only sit and stare. </p></p>The next day, Mayor Maine walks past, and his wide soft orange foot presses right onto your face before he carries on without paying any more attention to you. When the sun sets, and rises, and sets, you realise your life is now one of eternally watching and supporting the people of Dormaus. Particularly the mayor, who makes a point of stepping on you on his way to work every day. You are nothing but a stone.",
		    			no: "You shake your head. Of course you don't want to be a rock! The mayor shrugs, nods to you, and carries on his way. You take one last look at the fox-stone before you leave, and for a moment the expression of happiness and joy on the lifeless rock makes you almost reconsider...",
		    			yesEffect: function(player) {
		    				player.description = "This is a simple cobblestone in the street. The shape of the stone looks a little like a " + player.sex + ' ' + player.race + ' once called ' + player.name;
		    				player.trapped = "You are nothing but a cobblestone on the streets of Dormaus. You are trapped forever, feeling the feet of passers-by on your face and the sun beating down on you, slowly smoothing you out until one day you will be a perfectly normal rock."
		    			}
			    	};
			    }
			},{
			    "id": "Guest Fursuit",
			    "moveMod": 0,
			    "description": 'You find yourself in front of an unfamiliar building, unsure of how you got there and feeling a vague sense of Deja Vu. The street you are on is eerily quiet, not a single other person around you for as far as you can see. All the other buildings around you feeling very empty and devoid of life. The building in front of you resembles some sort of store, with large windows and a glass door with a “We are Open” sign hanging on the inside. When you try to glance through the windows, you find the inside of the store to be quite dim and through the glass you can make out what looks like suits or costumes hanging from some racks. You back up and notice a sign above the door, which you can almost swear was not there before. The sign has text in a comic sans font which says “Fur-Real Costume Shop” and below the text is a picture of a really exaggerated blue cartoon fox winking. Do you decide to enter the shop? [Guest event from CJMPinger]',
			    "repeatable": true,
			    "valid": function(player, command, fn) {
			    	if (player.race == "fursuit") {
			    		return false;
			    	} else {
			    		return true;
			    	}
			    },
			    "effect": function(player) {
			    	player.confirmStep = {
		    			yes: 'You decide to sate your curiosity and enter the unusual shop. When you open the door a bell above you rings, and you walk forward into the dimly lit shop. All around you, you see mascot costumes hanging from the racks. They are really colorful ranging from bright neon blues and reds to dimmer brown and whites. The heads of the costumes appear to be on some shelves on the left side of the store. Their eyes blankly stare at you with perpetual smiles on their faces. You decide to walk around to shop to see if you can find the store owner or a desk, but all you find, other than more of the costumes, is an intricate gilded mirror in the back of the store.</p>After a while of waiting in the store, you decide to leave, confident that no store clerk or owner is going to arrive. You make it all the way across the store and start to open the door when you hear a large crash behind you. When you turn around you see one of the costumes, fallen on the ground. </p>Out of common courtesy you go to put the costume back where it belongs. However when your hand touches the surprisingly soft fur, you instantly have an urge to try the suit on. You pick it up and start to look at it. It is a blue and white furred costume with a tail in the back and attached arm and leg paws. You check a tag on the bottom of the tail and realize that the costume is meant to be an arctic fox. You look around to make sure no one is around and with the suit in your hands you walk toward the mirror. You hesitate for a second, staring into the smooth inside of the suit not sure why you are doing this, but nevertheless you begin to slip on the suit. The inside feels warm and smooth to the touch, and it slides perfectly onto your body. </p>Zipped up, the suit fits perfectly upon your body, almost as if it was made specifically for you. You look in the mirror and move around, seeing how it looks on you. Other than your head, which is contrasting with the rest of your body, you think that you look great. Everything below looks like a larger than life fox. Your “paws” are slightly oversized, your “tail” is extremely bushy and sways almost lifelike when you move, and the white and blue color palette accentuates the suit on your body. </p>You start to lose track of the time as you keep posing and turning with the suit on your body, it becoming an obsession. After a while you decide it is finally time to take off the suit and you attempt to pull down the zipper. As you do, you find that it is stuck and you can’t pull it down. No matter how hard you pull down, it doesn’t budge an inch. You keep trying, slight panic swelling up from within you.</p>Then a calm thought, not entirely your own, enters your mind. “The suit is not complete…” At this you realize what was off all along. You were missing your head. You immediately run to the rack of heads you saw before and search for yours. It doesn’t take long, the right one standing out as bright as day. In the middle of the shelves, it is blue and white, like the rest of your body. The large cartoon eyes and gleaming smile practically telling you to put it on then and there. And so you do. You close your eyes and take the mask off the shelf, slipping it onto your head. It fits perfectly, like the rest of the suit, and slipping it on makes you feel a comfort you never felt before. Unknown to you, the head of the suit merged with the suit’s body, becoming one over your own.</p>At this you finally feel complete and whole, the suit filling the emptiness inside you. Conversely, your body begins to disappear under the suit, becoming a void beneath the cloth of the costume. Your “body” becomes lighter and hollower, just as it always was meant to be. You open your “eyes” and look around the shop. Then you look down at the zipper which confounded you earlier, and pull it down with ease. Inside your suit there is nothing but the inner black lining, and you feel happy. After a while you leave the shop content, but slightly confused why you went to a costume shop since as far as you can remember you can remember, you were always a walking fursuit.',
		    			no: 'You decide that whatever costume this shop is selling you don’t want and walk away. It doesn’t take long for you to get back to the main parts of Dormaus. Right after you arrive into the main area, you realize that you can’t retrace your steps back to the shop, your memory feeling a bit fuzzy. Much later all memories of the encounter are gone from your mind as if it never happened at all.',
		    			yesEffect: function(player) {
		    				player.description = "This adventurer is a living fursuit. Inside they are hollow and lined with fabric, and outside they are a cartoonish white and blue arctic fox with large eyes, paws, and a bushy tail.";
		    				player.race = "fursuit";
		    				player.size.value = 3;
							player.size.display = 'medium-sized';
		    			}
			    	};
			    }
			}]
		},
		{
			id: '3',
			title: 'The Fur and Feather Bar',
			area: 'dormaus',
			light: true,
			content: 'The bar is dim compared to the streets outside, but a roaring fire and many flickering candles gives it a homely and welcoming feeling. A massive dire-bear-skin rug lies next to the fireplace, and behind the bar, a dizzying array of colourful spirits are begging to be tasted. A sign next to the bar reads "Potion of Growth: 40 Gold. Potion of Shrinking: 40 Gold"',
			outdoors: false,
			exits: [
				{
					cmd: 'south',
					id: '2'
				},{
                    cmd: 'up',
                    id: '4'
                }
			],
			playersInRoom: [],
			monsters: [{
				name: 'Grizz',
				level: 15,
				short: 'Grizz',
				long: 'Grizz, the barkeeper, is wiping a glass behind the bar.',
				description: 'Grizz is an enormous musclebound bear, his powerful muscles coated by soft blubber and thick, heavy fur.',
				inName: 'Grizz',
				race: 'bear',
				id: 51,
				area: 'dormaus',
				weight: 245,
				diceNum: 2,
				diceSides: 8,
				diceMod: 5,
				meleeRes: 10,
				str: 16,
				position: 'standing',
				attackType: 'claw',
				damRoll: 10,
				hitRoll: 10,
				ac: 20,
				gold: 1000,
				merchant: true,
				wanderCheck: 38,
				itemType: 'mob',
				runOnAliveWhenEmpty: false,
				behaviors: [],
				items: [{
					name: 'Horse Milk', 
					short: 'a glass of horse milk',
					long: 'A glass of horse milk is here, a creamy, swirling white glass of sticky fluid. It moves so slowly and thickly that you feel slow just looking at it.' ,
					area: 'dormaus',
					id: 'horsemilk',
					level: 1,
					drinks: 6,
					maxDrinks: 6,
					itemType: 'bottle',
					material: 'glass',
					weight: 0,
					affects: [],
					value: 35,
					equipped: false,
					onDrink: function(player, roomObj, bottle) {
						World.msgPlayer(player, {
							msg: 'You swallow the strange creamy milk.',
							styleClass: 'cmd-drop blue'
						});
						Character.decreaseIntelligence(player);
					}
				},{
					name: 'Quickness Fluid', 
					short: 'a glass of quickness fluid',
					long: 'Quickness Fluid is a black, oily drink that gleams like a rainbow. It twitches rapidly with the slightest movement of the glass.' ,
					area: 'dormaus',
					id: 'quickfluid',
					level: 1,
					drinks: 6,
					maxDrinks: 6,
					itemType: 'bottle',
					material: 'glass',
					weight: 0,
					affects: [],
					value: 35,
					equipped: false,
					onDrink: function(player, roomObj, bottle) {
						World.msgPlayer(player, {
							msg: 'You swallow the thin and tingly fluid. It almost feels electrical in your mouth, and it makes your brain zap and tingle.',
							styleClass: 'cmd-drop blue'
						});
						Character.increaseIntelligence(player);
					}
				},{
					name: 'Potion of Growth', 
					short: 'a potion of growth',
					long: 'A swirling potion of growth, that seems to bulge out at its container, was left here.' ,
					area: 'dormaus',
					id: '303',
					level: 1,
					drinks: 6,
					maxDrinks: 6,
					itemType: 'bottle',
					material: 'glass',
					weight: 0,
					affects: [],
					value: 35,
					equipped: false,
					onDrink: function(player, roomObj, bottle) {
						if (player.size.value == 1) {
							Character.resize(player, 2);
							
							World.msgPlayer(player, {
								msg: 'You feel your whole body tensing up, as your insides feel swollen and bubbly. Your tiny body feels more firm as you start to grow, stretching out from your tiny stature and looking in awe as the towering world around you seems so much smaller. You grow to the size of a small fox.',
								styleClass: 'cmd-drop blue'
							});
						} else if (player.size.value == 2) {
							Character.resize(player, 3);
							World.msgPlayer(player, {
								msg: 'You feel your whole body tensing up, as your insides feel swollen and bubbly. Your feet press into the ground as you start to grow, stretching out from your short stature and seeing the world around you shrink to match your size. You grow to the size of a human.',
								styleClass: 'cmd-drop blue'
							});
						} else if (player.size.value == 3) {
							Character.resize(player, 4);
							World.msgPlayer(player, {
								msg: 'You feel your whole body tensing up, as your insides feel swollen and bubbly. You feel powerful and heavy as you start to grow, stretching out from your ordinary stature and becoming taller and taller. Your weight presses into the ground, and the world around you starts to look small and weak, as you grow as large as a minotaur.',
								styleClass: 'cmd-drop blue'
							});
						} else if (player.size.value == 4) {
							Character.resize(player, 5);
							World.msgPlayer(player, {
								msg: 'You feel your whole body tensing up, as your insides feel swollen and bubbly. Your feet leave mighty dents in the ground, and your whole body feels enormous and weighty. The world around you looks like a toy, becoming small and almost unreal. You grow to the mighty height of a dragon, other creatures mere dwarves to you.',
								styleClass: 'cmd-drop blue'
							});
						} else if (player.size.value == 5) {
							if (player.area == "dormaus") {
								Character.resize(player, 6);
								World.msgPlayer(player, {
									msg: 'You feel your whole body tensing up, as your insides feel swollen and bubbly. The world around you shrinks so quickly that it soon looks like a plaything. You could crush entire buildings under one of your enormous feet. Your whole body is enormous and tanklike, a mighty and gigantic towering behemoth! You are too large to even reasonably interact with the antlike creatures beneath you.',
									styleClass: 'cmd-drop blue'
								});
							} else {
								World.msgPlayer(player, {
									msg: 'During your trip, the potion seems to have expired.',
									styleClass: 'cmd-drop blue'
								});
							}
						} else {
							World.msgPlayer(player, {
								msg: 'You feel a bubbling and gurgling in your colossal belly as you sip from the microscopic bottle, but then it fades. You are too large and powerful for the magic of this potion to grow you any further.',
								styleClass: 'cmd-drop blue'
							});
						}
						
					}
				},{
					name: 'Potion of Shrinking', 
					short: 'a potion of shrinking',
					long: 'A swirling potion of shrinking, that seems to clench within its container, was left here.' ,
					area: 'dormaus',
					id: '302',
					level: 1,
					drinks: 6,
					maxDrinks: 6,
					itemType: 'bottle',
					material: 'glass',
					weight: 0,
					affects: [],
					value: 35,
					equipped: false,
					onDrink: function(player, roomObj, bottle) {
						if (player.size.value == 1) {
							World.msgPlayer(player, {
								msg: 'You feel your whole body tensing up, as your insides feel small and light. The sensation then fades. It seems that this potion is too weak to shrink you any smaller.',
								styleClass: 'cmd-drop blue'
							});
						} else if (player.size.value == 2) {
							Character.resize(player, 1);
							World.msgPlayer(player, {
								msg: 'You feel your whole body tensing up, as your insides feel small and light. Your already short body begins to shrink, the world around you growing larger and more intimidating. You squeak as you shrink down to the size of one of your former feet, becoming the size of a tiny mouse.',
								styleClass: 'cmd-drop blue'
							});
						} else if (player.size.value == 3) {
							Character.resize(player, 2);
							World.msgPlayer(player, {
								msg: 'You feel your whole body tensing up, as your insides feel small and light. You feel your limbs getting thinner, and your body shrinking. The world looks large and clunky around you as you drop to half your height, becoming the size of a short fox.',
								styleClass: 'cmd-drop blue'
							});
						} else if (player.size.value == 4) {
							Character.resize(player, 3);
							World.msgPlayer(player, {
								msg: 'You feel your whole body tensing up, as your insides feel small and light. Your hefty body shrinks down, your large form reducing to the size of a mere human, as the world around you seems to be returning to the size designed for someone of this height.',
								styleClass: 'cmd-drop blue'
							});
						} else if (player.size.value == 5) {
							Character.resize(player, 4);
							World.msgPlayer(player, {
								msg: 'You feel your whole body tensing up, as your insides feel small and light. Your mighty and bulky body shrinks down, your huge form shrinking to merely a size that is merely much larger than most of those around you, instead of towering and intimidating.',
								styleClass: 'cmd-drop blue'
							});
						} else {
							Character.resize(player, 5);
							World.msgPlayer(player, {
								msg: 'You feel a bubbling and gurgling in your colossal belly as you sip from the microscopic bottle, and then with a bizarre rushing sensation, you shrink down from your colossal size. The world rushes up to meet you, and you are drawn down from the cloud layer and left as only a massive hulking beast, rather than an impossibly huge monster.',
								styleClass: 'cmd-drop blue'
							});
						}
						
					}
				}],
				topics: {
					name: '"Name\'s Grizz."',
				    job: '"Barkeeper." He stares at you like your question is incredibly stupid.',
				    bye: 'He just grunts at you.',
				    growth: '"Made from dragon cum. S\'a dragon up the mountain. Challenge him to a cum contest. Win every time."',
				    shrinking: '"Made from sweat. Mouse sweat. When they think they\'re gonna get eaten, they sweat. Makes shrinking potions"',
				    horse: '"Horse milk. Get it from the farm down south. Flute brings it up. Makes you a dumbass."',
				    quickness: '"Flute gets it from somewhere. Said it\'s from the future. Makes you a smartass."',
				    cock: 'He stares at you from under his hairy, brutish eyebrows, then reaches one hefty paw under the bar. With a grunt, he lifts up an enormous glistening black sausage of a cock, thicker than your arm. He drops it on the counter, the stink of bear crotch filling the bar, then pulls it back down between his legs.',
					paws: 'The bear grunts, and grabs a stool behind the bar, which he sits on. It creaks like it\s on the verge of snapping in half under his weight. He lifts one of his feet and drops it on the bartop with a heavy THUMP. His enormous foot is bigger than your face, the sole completely covered with a thick, dark black pawpad. His long claws stick out from shaggy fur, and his foot is glistening with dirt and sweat. He pulls it back down and stands up like nothing happened.'
				}
			}],
			events: [{
			    "id": "Orc Absinthe",
			    "moveMod": 0,
			    "description": '"Hello there, by any chance is there any green absinthe, barkeeper?", you hear an adventurer say. He\'s sitting at the bar, and Grizz grunts, before sliding over a tall bottle. The adventurer pours himself a bit, and takes a gulp. He shudders, and clutches his chest. He looks unwell – his skin is taking on a green, grassy tint. His jaw creaks forward, and his lower teeth start to thicken and jut out, becoming big curved tusks. "That was amaz- er, wonde-, errr...GOOD!" he says, in a deeper, slower voice. </p>He drinks more, his body thickening with muscle and the chair under him creaking as he grows taller and heavier. His clothes give way with a RIP, revealing a stacked, muscular green chest, and his boots explode open to show off gigantic green muscular feet. He snorts and grunts. "Me green. Drink good." he says, and Grizz just nods.',
			    "repeatable": true
			},{
			    "id": "Grizz Piss",
			    "moveMod": 0,
			    "description": '“What are ya havin\'”, Grizz grunts. You turn to the barkeeper, not having intended to really buy anything right now, but before you can respond, your curse starts to feel warm and heavy in your chest. Your voice speaks without you meaning to, and you find yourself saying “Piss!”</p>You quickly cover your mouth, but it\'s too late. Grizz just nods, and reaches forward over the bar, before grabbing hold of you and dragging you close to him. </p>He shoves you onto your knees behind the bar, your face next to his heavy and musky cock. He shoves his thumb into your mouth to force it open, then rams his cock forward, and holds your face firmly against his crotch. You are left with your jaw aching around his shaft for a moment, before he starts to grunt, and you feel him relieve himself. His piss is very warm, and it flows directly down your throat and into your belly. You taste only a little at the back of your throat, and it is bitter and acrid. </p>Your stomach seems to almost bulge and swell as the bear releases what feels like pints of urine into you, and your head feels foggy and dizzy. When he finally pulls free, and the last of his piss dribbles down your face, you hiccup and realise you are drunk!',
			    "repeatable": true,
			    "valid": function(player, command, fn) {
			    	if (player.watersports) {
			    		return true;
			    	} else {
			    		return false;
			    	}
			    }
			},{
			    "id": "Age Booze",
			    "moveMod": 0,
			    "description": 'You settle down at the bar, nodding to Grizz and ordering a little something to wet your whistle. The bear goes to fill a glass, a stern growl and furrowed brow show his irritation. "I feel like I just changed this darn keg...be right back!"</p>You watch the bear trundle off go the backroom, scratching his belly as he grumbles in mild irritation. As soon as the door swings shut beside him you notice movement next to you. A spry young looking lion takes the seat next to you. </p>He has a big grin on his face as he makes a quiet, "Shhh..." motion with his finger. His red mane is short and still seems to be filling in around his golden tan fur. The young feline is reaching behind the counter trying to grab a bottle before you get back. You watch in in surprise, not sure if you should call Grizz or not.</p>"YOU!" A thunderous grumble silences the bar, all eyes falling on the thieving cat. His eyes wide with his arm behind the counter, like a kid caught with his paw in the cookie jar. He grins and tries to bolt. </p>Even with the keg on his shoulder the bear moves quick, quicker than you think a bear his size might. He grabs the lion by the scruff of the neck and bends him backwards over the bar. </p>"Don\'t move kid!" He growls, "I\'ve had it up to here with you. The legal age to drink is twenty-one, NOT eighteen! If you want a nip that bad, maybe Ol\' Grizz can help you out with that.”</p>The lion is too scared to move under the watchful eye of the intimidating bear. You can\'t help but remain obediently rooted to your seat as well. The bear taps the keg on his shoulder with one angry fist, twisting the spigot on. Beer starts pouring into the young cat\'s mouth. The bear kicks up the bottle the cat was reaching for and uncorks it as well, pouring and adding it to the stream. Lastly the bear slides his cock into the lion\'s open maw. The two streams of alcohol pour over the bear\'s cock as he humps the lion\'s throat.</p>The lion struggles under Grizz, gagging on the fluids assaulting his throat. His shirt is pulled up due to how he\'s bent. You watch his trim golden belly grow a thick read treasure trail into his pants. The bulge in his trousers thickens and expands, soon the young lion is hard. His body continues to broaden. His chest becoming thicker, he gains a bit of a beer belly as the treasure trail grows thicker and richer. A wet stain forms in the lion\'s bulge, not sure if the drunk kid has wet himself like a drunk, or if he has cum. The scent of booze overwhelms. The thick red treasure trail gets flecks of silver. You look up and see the youthful teenage face of the lion is gone. Now there is a rugged older lion guzzling booze and bear cock like a champ, a full mane and beard on his strong jaw. It\'s a deep red with mature flecks of silver. As the keg taps out, Grizz pulls his cock free. The older lion gives a triumphant belch, looking down in surprise at the man he\'s become. </p>"Don\'t look so smug kid, you still gotta pay your tab!” Grizz gruffs with a smirk. (Guest event by Fahlma)',
			    "repeatable": true
			},{
			    "id": "Drunk Mouse",
			    "moveMod": 0,
			    "description": 'You see a small mouse sitting on the countertop, drinking from a thimble. He\'s leaning against a bowl of nuts and looks very drunk. "Stupid coyote cheated me.", he squeaks, and drowns his sorrows in more tiny quantities of ale. "S\'not all bad, though. Ale is cheap when you\'re this tiny!", he finishes. </p>With a drunken giggle, he stumbles and falls into the nuts, his own furry rodent balls resting atop two walnuts as he falls asleep.',
			    "repeatable": true
			},{
			    "id": "Grizz Blowjob",
			    "moveMod": 0,
			    "description": 'Grizz seems distracted today. He\'s not wiping his usual glass, and he is taking longer than usual to serve customers. Every now and then he grunts and moans, and his shaggy fur is damp with sweat. Someone asks for a bear musk, and he grunts that it\'s all been reserved for a special customer. </p>Curious, you lean over, looking past the bar under Grizz\'s massive furry gut. Sure enough, between his legs is a smaller bear with a huge grin on his face. The bear is bobbing his head back and forth on Grizz\'s enormous cock, and every time Grizz moans and pants, a spurt of cum escapes from the other bear\'s lips. Must be a special offer – straight from the tap. ',
			    "repeatable": true
			},{
			    "id": "Grizz TF",
			    "moveMod": 0,
			    "valid": function(player, command, fn) {
			    	if (player.race == "bear") {
			    		return false;
			    	} else {
			    		return true;
			    	}
			    },
			    "description": 'As you enter the pub, the barkeeper looks over at you. His enormous furry body reaches up to the ceiling, and his deep voice seems to make the building rumble. </p>“Hey, shortie. Special on tonight. Bear Musk. New cocktail. You tryin\'?” The massive bear scratches his furry belly and gives you a dirty leer.',
			    "repeatable": true,
			    "effect": function(player) {
			    	player.confirmStep = {
		    			yes: 'The bear chuckles, then turns around. You hear a grunting and pumping, and he pants, one of his arms propping him up against the wall. Then there\'s a deep growl, and a sticky splashing noise. He turns back around, still panting a bit, and slides a shot glass over to you. It\'s filled with thick creamy goo, which is splattered all over it in a messy dripping load. The goo has a strong, musky and sexual aroma, which reminds you a lot of the huge shaggy smelly bear. </p>He grins widely at you as you gulp it down, swallowing the warm goo in one go. Immediately, you feel hot and flushed all over. Your skin itches as sweat starts to drip down your body, giving you a musky stink that has a deep, beastlike undertone to it, just like the barkeeper. You feel thick, bristly fur growing all over you, especially under your arms and around your crotch. The floorboards under you creak as you grow taller and wider, heavy with swelling muscles under your hairy skin. </p>As you pant and sweat at the bar, the barkeeper bear steps back, showing off that he\'s naked below the waist. Only the countertop was concealing his fat, dark cock and huge musky furry bear balls. He grips his shaft in one massive rough paw, and starts to pump and stroke it right in front of you. Your mouth waters as you watch his huge cockhead dribble and gush with slick precum. After a few minutes of pounding his cock, he growls, spurting a thick white stream of cum into a new shot glass. The dripping cum fills it up and makes a sticky mess all over his hand and the bartop. He then slides it over to you, and you grab and swallow it like a man dying of thirst. </p>With more bear cum in your belly, your changes accelerate. Your chest grows heavy and round, a layer of fat beginning to cover your rock-hard new muscles. Your palms and soles develop a dark, tough padding, and your fingernails transform to thick powerful black claws. Your face grows into a stocky muzzle, the bones creaking as they transform, making your jaw powerful and deadly. The intensity of the changes make you fall to your knees, clutching your furry gut with your clawed hands. </p>The barkeeper walks around and stands in front of you, his brick-thick cock in your face. You cannot resist, and you eagerly open your fanged muzzle to take in his sweaty, musky cock, so that you can suck down his powerful cum straight from the tap. Your furry face pushes against his crotch over and over, his heavy gut resting on your head, until he grabs your head and squashes you in firmly between his legs. Cum is pumped into your throat in messy spurts. </p>The more you drink, the more you change, and by the time the barkeep pulls his cock from your face, you are a huge hairy chubby bear just like him. A few last splatters of cum spray over your muzzle, before the big bear returns to his bar like nothing happened.',
		    			no: "You shake your head, and the huge bear shrugs and returns to wiping a glass with a dirty rag.",
		    			yesEffect: function(player) {
		    				player.description = "This adventurer is a huge, chubby and very furry bear.";
		    				player.race = "bear";
		    				player.size.value = 4;
							player.size.display = 'large';
		    			}
			    	};
			    }
			}],
			size: {value: '4'},
			items: [],
		},
        {
            id: '4',
            title: 'The Fur and Feather Stalls',
            area: 'dormaus',
			light: true,
            content: 'From here, you can look down at the main area of the bar, past a wooden banister. This raised area is up the stairs and slightly darker. Several wooden tables are arranged below the mounted heads of strange beasts.',
            outdoors: false,
            exits: [
                {
                    cmd: 'down',
                    id: '3'
                }
            ],
            playersInRoom: [],
            events: [{
			    "id": "Dog Poker",
			    "moveMod": 0,
			    "description": 'It looks like Ace is playing poker with an entire team of adventurers. A male and female warrior, and a tall mage, are all chatting together and dealing chips while they play. You watch for a moment, and see that the humans\' ears are starting to stretch up, growing pointed and developing fur. Their skin sprouts hairs all over, thickening into pelts. Grey, brown, and orange. They don\'t seem to notice, even as they push forward their chips with hands that are becoming clawed and the palms padded with black pawpads. One of them barks and howls, and you see tails pushing out from the holes in the backs of the chairs and start to wag. </p>In a few minutes, there are four canines all playing poker together, and you are reminded a little of a painting you once saw.',
			    "repeatable": true
			},{
			    "id": "Ace's New Boots",
			    "moveMod": 0,
			    "description": 'Ace\'s boots are on the table again as you walk past, but you notice something a little different. One of his boots is white with black dalmation stripes, and the other is tiger-print. He grins when you notice. "Howdy. Don\'t worry, they had it coming. Don\'t spill beer on a man\'s boots." he says. </p>You see the boots wiggle and twitch slightly. From the rumors you\'ve heard about Ace\'s legendarily rank and smelly paws, you imagine those two are regretting their rudeness. The coyote tosses you something, and you catch a dusty, muddy pair of leather cowboy boots. They are soaked in booze, but the stink of ale is overpowered by a musky, masculine stench of sweat and hard work. It\'s so potent that you feel quite dizzy, though you have a bizarre urge to shove your nose in them and breathe deep. </p>"You can keep these old ones, partner. They were two folks too, once, but after a while they fade and turn to good, brown leather. Perfect for adventurers like us." he says. You thank him for the gift, and leave with mixed feelings about such a \'punishment\'.',
			    "repeatable": true,
			    "effect": function(player) {
    				Character.addItem(player, {
    					name: 'Leather boots', 
    					displayName: 'Leather boots',
    					short: 'leather boots',
    					long: 'There is a pair of very large and extremely smelly leather boots here' ,
    					area: 'dormaus',
    					affects: [],
    					id: '3', 
    					level: 1,
    					itemType: 'armor',
    					material: 'leather', 
    					ac: 2,
    					value: 50,
    					weight: 1,
    					slot: 'feet',
    					equipped: false
    				});
			    }
			},{
			    "id": "Dog Bandit",
			    "moveMod": 0,
			    "description": 'There\'s a new visitor to the bar today. A tall and hairy human, with a scar over his eye and a huge beard. His belly is round and large, but rock hard, and his shirtless torso is covered in thick, dark body hair. He\'s talking with a gruff voice to a thin and scrawny guy, and you catch the end of it as you approach. "Yarr, can\'t have a bandit who falls asleep on duty, lad! Yer gettin\' demoted, permanently." he says. </p>The other man tries to protest, but all that comes out is a loud, deep bark. He covers his face in shock as his mouth and nose pushes forward into a furry muzzle. His ears grow larger and flop down, and brown fur begins to flow over his skin. He barks again, stumbling on his feet as they grow longer, and then falls to all fours. His hands clutch together and shift, forming claws and pads as they become dog\'s paws. His wriggles and struggles cause him to slip free of his clothes, and a furry tail grows from his rear. He whimpers and whines, but the huge bandit grabs him by the scruff of his neck and drags him forward, shoving his sensitive nose into his sweaty crotch. </p>The dog looks shocked for a moment, then his tail starts to wag as his eyes darken into big, brown, ordinary dog eyes. "There. Loyal mutts don\'t fall asleep on duty.", the bandit says. You wonder if his crotch musk has that effect on humans, too.',
			    "repeatable": true
			},{
			    "id": "Ace Piss",
			    "moveMod": 0,
			    "description": 'Ace is shifting uncomfortably in his seat, and glancing over to the bathroom. When he does though, he sees you, and a grin stretches across his muzzle. “Howdy, you are just what I need. How about a game? Loser has to drink the winner\'s piss.” he says in his southern drawl. You try to refuse that absurd gamble, but instead your mouth opens up and you find yourself shouting yes, with obvious desperation. You sit down at the table, shaking slightly, as Ace deals the cards. By the time the river has been played, you can see that you have an almost unbeatable hand. A royal flush! </p>Thinking of flush just makes you think of hot, wet piss, flowing down over your face, however. By the time you shake your thoughts back to normal, you realise you have folded and surrendered the game. Ace chuckles, and reveals that his own hand was worthless trash. “Guess I bluffed ya, partner. Now get under the table, I got me a barrel to empty.” he says.</p></p>You get on your knees and crawl under the table, until your face is next to Ace\'s dirty leather breeches. You unbuckle his belt and pull them down, allowing his thick and warm cock to flop out, where it lands against your face. You inhale his sweaty coyote stink, before opening your mouth and sliding it over his shaft, with your face pressed firmly into his furry crotch. He holds one of his gloved hands on your head, and sighs as he relieves his swollen bladder. You gulp down his hot, steaming piss, every drop of it nasty and sharp on your tongue. </p>He doesn\'t let you move until you drink every bit of it though, using you as his own portable urinal. When he pulls free and buckles his pants back up, you are left with a belly that is gurgling and sloshing with coyote piss.',
			    "repeatable": true,
			    "valid": function(player, command, fn) {
			    	if (player.watersports) {
			    		return true;
			    	} else {
			    		return false;
			    	}
			    }
			},{
			    "id": "Ace Mouse",
			    "moveMod": 0,
			    "description": 'Ace the coyote is sitting at his usual spot, one of his boots up on the table. He has a pack of playing cards fanned out on the stained wood of the table, and he nudges up his cowboy hat as you approach, before giving you a wink. </p>“Howdy partner. Up for a little wager?” he says in his masculine drawl.',
			    "repeatable": true,
			    "valid": function(player, command, fn) {
			    	if (player.race == "mouse") {
			    		return false;
			    	} else {
			    		return true;
			    	}
			    },
			    "effect": function(player) {
			    	player.confirmStep = {
		    			yes: 'You agree, and take a seat opposite from the tall canine. He lowers his boots and deals you some cards, sending them sliding across the table with graceful flicks of his wrist. </p>“Texas hold \'em. I got no need for money, so I think we\'ll bet...height! You lose, you shrink.” The grin on his long muzzle suggests that he knows something you don\'t know. You look at your cards, a two and a five. Naturally, Ace has two aces. Then on the next hand, you have a pair of fours, and Ace gets a straight. </p>Over and over, the coyote gets a perfect high-value hand, and you get trash. With each hand, you shrink a little more, your clothes becoming baggy and loose on your diminished body. </p>When you shrink below three feet tall, your skin starts to tingle and itch as it grows soft white fur. Your ears widen and stretch, becoming round and large atop your head. You squeak with indignation, and have to clamber up onto the table to be able to see the cards. A long tail starts to grow from your rear, pink and furless, and your face pushes out into a pointy little muzzle. </p>You look down at yourself, a cute, naked little white mouse, with small pink paws, a long tail and twitching whiskers, and you kick one of the cards in frustration. You accuse Ace of cheating, and he laughs. </p>“And whatcha\' gonna do about it, partner?” He leans forward, grabs you by the tail, and dangles you over his muzzle as you kick and struggle in the air, helpless in his grip. Just when you fear he might open his mouth and swallow you whole, he tosses you to the floor, and you have no choice but to leave in your tiny new form.',
		    			no: "You politely decline, and the cowboy coyote shrugs and lowers his hat back down.",
		    			yesEffect: function(player) {
		    				player.description = "This adventurer is a tiny white mouse.";
		    				player.race = "mouse";
		    				player.size.value = 1;
							player.size.display = 'tiny';
		    			}
			    	};
			    }
			}],
			size: {value: '4'},
			monsters: [{
				name: 'Ace',
				level: 15,
				short: 'Ace',
				long: 'Ace the coyote is sitting at his usual chair.',
				description: 'Ace is a coyote with a long smirking muzzle. His face is obscured by his cowboy hat, and sometimes he likes to toy and juggle with a gleaming knife.',
				inName: 'Ace',
				race: 'coyote',
				id: 51,
				area: 'dormaus',
				weight: 245,
				diceNum: 2,
				diceSides: 8,
				diceMod: 5,
				meleeRes: 10,
				str: 16,
				position: 'standing',
				attackType: 'claw',
				damRoll: 10,
				hitRoll: 10,
				ac: 20,
				gold: 1000,
				merchant: true,
				wanderCheck: 38,
				itemType: 'mob',
				runOnAliveWhenEmpty: false,
				behaviors: [],
				topics: {
					name: '"Folks round here call me Ace."',
				    job: '"This and that. Not got anythin\' in particular at the moment. Guess you could call me a professional gambler."',
				    bye: 'He gives you a wink as you leave.',
				    cock: 'The coyote chuckles, and one of his gloved hands strokes down his thigh. "You ain\'t seein\' that partner, not unless I lost my clothes in a poker game. And I never lose."',
					paws: 'The coyote\'s boots are crossed atop his table, and he raises an eyebrow at you when you ask. "I don\'t give folks what they want for free, partner. Sides, a wanderin\' guy like me don\'t get a chance to bathe much. You don\'t wanna smell these dogs."'
				}
			}],
            items: []
        },
        {
            id: '5',
            title: 'The Want of a Nail General Store',
            area: 'dormaus',
            light: true,
			size: {value: '4'},
            content: 'The walls and floor of this store are stocked with an assortment of strange odds and ends. Bags of flour sit next to chipped statues, and on the shelves, bottles of vinegar share space with wooden masks and stuffed animals.',
            outdoors: false,
            exits: [
                {
                    cmd: 'down',
                    id: '2'
                },{
                    cmd: 'west',
                    id: '6'
                }
            ],
            playersInRoom: [],
            monsters: [{
				name: 'Mikhail',
				level: 15,
				short: 'Mikhail',
				long: 'Mikhail the shopkeeper is organising his inventory.',
				description: 'Mikhail is an older german-shepherd dog. His fur is dusted with grey hairs, as is his fluffy moustache.',
				inName: 'Mikhail',
				race: 'dog',
				id: 51,
				area: 'dormaus',
				weight: 245,
				diceNum: 2,
				diceSides: 8,
				diceMod: 5,
				meleeRes: 10,
				str: 16,
				position: 'standing',
				attackType: 'claw',
				damRoll: 10,
				hitRoll: 10,
				ac: 20,
				gold: 1000,
				merchant: true,
				wanderCheck: 38,
				itemType: 'mob',
				runOnAliveWhenEmpty: false,
				behaviors: [],
				items: [{
					name: 'Amulet of Dominance', 
					displayName: 'Amulet of Dominance',
					short: 'amulet of dominance',
					long: 'There is an amulet of dominance here, a golden seal that marks the wearer as a powerful dom.' ,
					area: 'dormaus',
					affects: [],
					id: 'amuletofdom', 
					level: 1,
					itemType: 'armor',
					material: 'gold', 
					ac: 2,
					value: 45,
					weight: 1,
					slot: 'head',
					equipped: false,
					onEquip: function(player) {
						player.domsub = "dominant";
					}
				},{
					name: 'Submission Collar', 
					displayName: 'Submission Collar',
					short: 'submission collar',
					long: 'There is a submission collar here, a heavy gleaming collar that marks the wearer as an obedient submissive.' ,
					area: 'dormaus',
					affects: [],
					id: 'subcollar', 
					level: 1,
					itemType: 'armor',
					material: 'silver', 
					ac: 2,
					value: 45,
					weight: 1,
					slot: 'head',
					equipped: false,
					onEquip: function(player) {
						player.domsub = "submissive";
					}
				},{
					name: 'Switch Charm', 
					displayName: 'Switch Charm',
					short: 'switch charm',
					long: 'There is a switch charm here, a metal charm shaped like a flickable switch that marks the wearer as up for both sides of things.' ,
					area: 'dormaus',
					affects: [],
					id: 'switchcharm', 
					level: 1,
					itemType: 'armor',
					material: 'iron', 
					ac: 2,
					value: 5,
					weight: 1,
					slot: 'head',
					equipped: false,
					onEquip: function(player) {
						player.domsub = "switch";
					}
				},{
					name: 'Tasty shirt', 
					displayName: 'Tasty shirt',
					short: 'tasty shirt',
					long: 'There is a cotton t-shirt here, with the word TASTY written across it in pink. It makes the wearer smell like prey.' ,
					area: 'dormaus',
					affects: [],
					id: 'tastyshirt', 
					level: 1,
					itemType: 'armor',
					material: 'cotton', 
					ac: 1,
					value: 5,
					weight: 1,
					slot: 'body',
					equipped: false
				},{
					name: 'Gender lock', 
					displayName: 'Gender lock',
					short: 'gender lock',
					long: 'There is a gender lock bracelet here',
					description: 'This bracelet is shaped like a padlock with male and female symbols on it',
					area: 'dormaus',
					affects: [],
					id: 'genderlock', 
					level: 1,
					itemType: 'armor',
					material: 'steel', 
					ac: 1,
					value: 5,
					weight: 1,
					slot: 'arms',
					equipped: false
				}],
				topics: {
					name: '"My name is Mikhail, kid. Though some call me Daddy."',
				    job: '"I own this little store here. It may not be much, but I am as proud of it as I am of my sons."',
				    bye: '"Travel safely, adventurer. Make sure to wrap up warm and get enough to eat."',
				    cock: 'The old dog smiles and shakes his head. "It\'s quite handsome, I am told. Adventurers these days, so forward!"',
					paws: 'Something about that question makes the dog laugh a little darkly. "Oh, hard-working and musky, I assure you. Though only bad boys get punished with my paws. Keep your hands off the merchandise and we will have no problems."',
					tasty: '"The tasty shirt is the perfect accessory for the adventurer who wants everyone to know they can be eaten at any time."',
					gender: '"The gender lock is a magical bracelet that protects you from gender transformations."',
					dominance: '"That amulet is a very large and showy piece of gear. Perfect for showing off how dominant you are. Try taking it off and on again to enhance the effect."',
					submission: '"That necklace is made of enchanted metal, it makes you feel weak and submissive. Try taking it off and on again to enhance the effect."',
					charm: '"That little lock is a trinket that people use to show they like it both ways. Try taking it off and on again to get the magical effect."'
				}
			}],
            events: [{
			    "id": "Adventurer Underwear",
			    "moveMod": 0,
			    "description": 'You see someone unloading gear and selling it off to Mikhail. The adventurer is carrying an enormous sack of goods, and he slides over a breastplate, boots, swords and many other things. He grabs something from the bottom of the bag and slides it over – a large pair of underwear. There is a face on the front, which struggles and whimpers. "Oh, this one isn\'t quite finished." he says, apologetically. </p>Mikhail just grins and drops his pants, then takes the living underwear and pulls it up over his crotch. His cock and balls bulge out the front of the underwear, and the face changes from terror to a look of lust and arousal. As Mihail strokes and rubs the front of his bulge with his big rough hand, the face slowly fades, the person within accepting their fate and giving in to the dog\'s warmth and musk. "There ya go, all sorted. I\'ll pay extra for these ones." he says. The adventurer leaves with a grin and a bulge in his own pants.',
			    "repeatable": true
			},{
			    "id": "Rubber Bunny",
			    "moveMod": 0,
			    "description": 'There is a rabbit trying to sell something to Mikhail as you come into the shop today. It\'s a large bottle full of shiny pink fluid. Mikhail\'s arms are crossed, though, and he\'s frowning. "Don\'t mess with potions and stuff in this shop, son. Take it to the fox peddler, I only sell equipment and objects. No perishables." he says. The rabbit sighs with disappointment, and turns to leave. His long foot catches on the hook of an umbrella on the floor, and he yelps and trips over. </p>The potion shatters, coating his chest with shiny pink goo. He squirms as he tries to pull it off, but it sticks to his hands too, flowing down them and spreading up his arms and down his torso. His hands are left squeaky, shiny mittens, and his belly becomes round and smooth. It flows over his crotch, spreading around his balls and making his cock harden and stiffen, before the goo solidifies it into what looks like a big rubber dildo. His legs are coated too, and he starts to moan and drool. Goo crawls up his head, spreading over his features and into his mouth, and his face is sealed into an O shape as he stops moving. The bunny lies still, a pink rubber sex doll, waiting to be used. </p>Mikhail nods and strokes his chin. "Now this I can sell", he says with satisfaction.',
			    "repeatable": true
			},{
			    "id": "Father's Day",
			    "moveMod": 0,
			    "description": 'Bonacieux the wolf priest is in the store, sitting behind the counter and chatting to Mikhail like an old friend. They laugh about something, and together they both laugh, and clink a pair of wine glasses together. You walk over, and look down to see that the two old men are pantsless. Kneeling before them is a pair of furry adventurers. A german shepherd, clearly one of Mikhail\'s transformed "sons", is nuzzling and kissing the wolf\'s huge furry grey balls, while a slender and handsome wolf is licking and stroking Mikhail\'s heavy dog cock. </p>The two old men see you looking, and wink at you. "Bony and me like to get together every now and then and trade our boys.", Mikhail says. Bonacieux smiles too. "Mikhail the town dad and Father Bonacieux – we call it Father\'s Day!" he says, and then grips the dog\'s head between his legs and howls as he fills the eager man\'s muzzle with hot, thick wolf cum.',
			    "repeatable": true
			},{
			    "id": "I'm Late!",
			    "moveMod": 0,
			    "description": 'You walk into the store and see an exhausted rabbit clerk hopping around lazily with an armful of merchandise while Mikhail, the german shepherd shopkeeper, stands at the counter and looks down over his grey-streaked mustache to tinker with a watch. Startled, the bunny jumps into air when Mikhail shouts, "When you finish sorting those, get started on that bag of socks thieves keep getting into." </p>The rabbit drops everything into a box with a huff and hops up onto the counter in front of Mikhail and whines, "I’m late already! I was supposed to be off ten minutes ago." </p> The large german shepherd puts down his watch and looks at the little bunny with a warm smile, but you catch a glimpse of a brief glow in his eyes. In his deep voice, Mikhail says, "Well, I\'m sorry about that. It’s been hard keeping track of the time ever since my watch broke."</p>The rabbit smiles up at his boss and nods, "I just have an important date to get to." But he suddenly falls forward on the table in front of Mikhail’s huge muscle gut, with his arms straightening out over his head. The rabbit’s head and chest start squeezing together into a perfect circle as he cries out, "What\'s happening to me?"</p>The shopkeeper grins down at the transforming rabbit and chuckles, "Since keeping track of the time is so important to you, I thought you wouldn\'t mind helping me."</p>You watch intently as the rabbit’s arms and legs fuse together, shifting into a beautiful brown leather band. Mikhail flips him over as the bunny’s head melts into the perfect circle of his former chest and becomes a glass and metal watch face. The rabbit’s nose pokes out at the center and his buck teeth stretch to form the minute and hour hands.</p>The large dog picks up his former employee and wraps him around his burly wrist with a grin. He remarks, "It looks like you\'re going to be very late." You catch a glimpse of the simple watch face on Mikhail’s wrist and the only hint that it was ever anything else is the strange, blinking pair of eyes that flank the spot where the hands meet.</p>Mikhail looks at his watch and looks at you. He smiles and asks, "You don\'t happen to need a job. Do you?" (Guest event from Dccurious)',
			    "repeatable": true
			},{
			    "id": "Daddy Mikhail: Male",
			    "moveMod": 0,
			    "description": 'As you browse the little shop, you start to notice little photographs tucked in between the goods and trinkets. They all contain the tall and distinguished dog shopkeeper, Mikhail, but he\'s with a different younger dog in each image. His stern grey-streaked moustache and silver-dusted fur is contrasted by the youthful energy of the other dogs, all the same breed as him, and all roughly in their twenties, with similar fur patterns and markings. </p>You get curious, and ask Mikhail what the photos are of. He looks up from a drawer he is organising, and smiles. </p>“Those are my sons, of course. I have many eager and loyal sons, and they\'d do anything for their daddy.” That last word makes you blush a little, and he raises one bushy eyebrow. </p>“Is something the matter, son?” he says, putting emphasis on the last word. His eyes stare between your legs, and a bit of a smirk makes his fluffy muzzle twitch. “If I didn\'t know better, I\'d think you wanted a daddy of your own, boy. Is that what you want? To come sit on daddy\'s lap?” </p>Do you admit your arousal to the handsome german shepherd?',
			    "repeatable": true,
			    "valid": function(player, command, fn) {
			    	if (player.race == "dog" || player.sex == "female") {
			    		return false;
			    	} else {
			    		return true;
			    	}
			    },
			    "effect": function(player) {
			    	player.confirmStep = {
		    			yes: 'Mikhail sits down on a large leather armchair, spreads his legs, and pats his thigh. “Come on now, boy. Show your daddy some love.” You walk over nervously, and sit down carefully on the dog\'s lap, feeling the bulge of his cock pressing out from his pants. </p>He wraps his powerful arms around you, and presses you against his chest. You didn\'t realise how tall he was. He looks down at you through his glasses, and leans forward. His moustache tickles your face as his long red tongue slides into your mouth. As he kisses you, his furry arm strokes along your chest, up and down, teasing and stroking you. Then it slides down to his pants, and reaches inside, to slowly pull out his cock. It stands proud and erect in the air, long and firm. </p>“Tell me you want it, son. Say you want daddy\'s cock.” You gulp and pant, then find yourself saying it out loud. “I want daddy\'s cock”, you stammer, and he growls with lust as he lifts you up, then pulls you down as he plunges his cock into your ass. </p>Your rear stretches around the older dog\'s shaft, and he growls a little as he thrusts in and out, his cock throbbing within you. Your heart pounds and your face is flushed with the pleasure of his shaft ramming your ass. You can feel your tailbone start to twitch, wiggling as it grows. It lengthens out behind you, and starts to wag from side to side, before growing fluffy fur. That same fur spreads up your back, where Mikhail strokes his clawed hands through it, admiring the same patterns and colours as his own furry body. Your face grows, pushing into his kiss as you grow a muzzle of your own. Your ears grow longer, develop fur, then flop over like a dog\'s. Your hands and feet throb for a moment, forming rough pawpads, and your nails become hard and sharp as they transform into claws. </p>Your whole body is changing, taking on aspects of your daddy\'s form, as you become a dog, the spitting image of Mikhail as a younger man. </p>He chuckles, and grips your shaft, stroking and pumping it as it turns red and pointed. The base swells out and inflates, forming a doggy knot, which he squeezes firmly in his hand. You whimper and bark with lust, and he growls at you. </p>“Cum for your daddy, pup.” You cannot disobey. You howl and bark, your cock pulsing and spraying out a thick load of cum into the air, that splatters over his shirt and over your furry chest. Then he holds you firmly and growls, and you feel his own cock release its musky load as a rush of heat deep in your rear. </p>You are left panting and dizzy, until he carefully pulls his cock free from your rear and allows you to stand. </p>“Good boy, son. You make your daddy proud.” he says. Your tail wags happily to hear your dad\'s praise.',
		    			no: "You shake your head and quickly turn away to browse the wares some more. Mikhail stares at you for a long while after you decline, his eyes gleaming.",
		    			yesEffect: function(player) {
		    				player.description = "This adventurer is a furry, eager dog.";
		    				player.race = "dog";
		    				player.size.value = 3;
							player.size.display = 'medium-sized';
		    			}
			    	};
			    }
			},{
			    "id": "Daddy Mikhail: Female",
			    "moveMod": 0,
			    "description": 'As you browse the little shop, you start to notice little photographs tucked in between the goods and trinkets. They all contain the tall and distinguished dog shopkeeper, Mikhail, but he\'s with a different younger dog in each image. His stern grey-streaked moustache and silver-dusted fur is contrasted by the youthful energy of the other dogs, all the same breed as him, and all roughly in their twenties, with similar fur patterns and markings. </p>You get curious, and ask Mikhail what the photos are of. He looks up from a drawer he is organising, and smiles. </p>“Those are my sons, of course. I have many eager and loyal sons, and they\'d do anything for their daddy.” That last word makes you blush a little, and he raises one bushy eyebrow. </p>“Is something the matter, lass?” he says curiously. His eyes stare deep into yours, and a bit of a smirk makes his fluffy muzzle twitch. “If I didn\'t know better, I\'d think you wanted a daddy of your own, lass. Is that what you want? To come sit on daddy\'s lap?” </p>Do you admit your arousal to the handsome german shepherd?',
			    "repeatable": true,
			    "valid": function(player, command, fn) {
			    	if (player.race == "dog" || player.sex == "male") {
			    		return false;
			    	} else {
			    		return true;
			    	}
			    },
			    "effect": function(player) {
			    	player.confirmStep = {
		    			yes: 'Mikhail sits down on a large leather armchair, spreads his legs, and pats his thigh. “Come on now, girl. Show your daddy some love.” You walk over nervously, and sit down carefully on the dog\'s lap, feeling the bulge of his cock pressing out from his pants. </p>He wraps his powerful arms around you, and presses you against his chest. You didn\'t realise how tall he was. He looks down at you through his glasses, and leans forward. His moustache tickles your face as his long red tongue slides into your mouth. As he kisses you, his furry arm strokes along your chest, up and down, teasing and stroking you. Then it slides down to his pants, and reaches inside, to slowly pull out his cock. It stands proud and erect in the air, long and firm. </p>“Tell me you want it, girl. Say you want daddy\'s cock.” You gulp and pant, then find yourself saying it out loud. “I want daddy\'s cock”, you stammer, and he growls with lust as he lifts you up, then pulls you down as he plunges his cock into your pussy. </p>You feel stretched around the older dog\'s shaft, and he growls a little as he thrusts in and out, his cock throbbing within you and sliding along your sensitive inner walls. Your heart pounds and your face is flushed with the pleasure of his shaft ramming you rough and hard. You can feel your tailbone start to twitch, wiggling as it grows. It lengthens out behind you, and starts to wag from side to side, before growing fluffy fur. That same fur spreads up your back, where Mikhail strokes his clawed hands through it, admiring the same patterns and colours as his own furry body. Your face grows, pushing into his kiss as you grow a muzzle of your own. Your ears grow longer, develop fur, then flop over like a dog\'s. Your hands and feet throb for a moment, forming rough pawpads, and your nails become hard and sharp as they transform into claws. </p>Your whole body is changing, taking on aspects of your daddy\'s form, as you become a dog, a beautiful daughter for your loving daddy. </p>He chuckles, and strokes your breasts, squeezing and gently teasing them as they are covered in fur. You whimper and bark with lust, and he growls at you. </p>“You love your daddy, don\'t you, pup. Howl for me.” You cannot disobey. You howl and bark, and as you do, the pleasure of his cock deep inside you becomes too much, and your body heats up as a shuddering orgasm rolls from your toes to your head, filling you completely. Then he holds you firmly and growls, and you feel his own cock release its musky load as a rush of heat deep into your pussy. </p>You are left panting and dizzy, until he carefully pulls his cock free from you and allows you to stand. </p>“Good girl, lass. You make your daddy proud.” he says. Your tail wags happily to hear your dad\'s praise.',
		    			no: "You shake your head and quickly turn away to browse the wares some more. Mikhail stares at you for a long while after you decline, his eyes gleaming.",
		    			yesEffect: function(player) {
		    				player.description = "This adventurer is a furry, eager dog.";
		    				player.race = "dog";
		    				player.size.value = 3;
							player.size.display = 'medium-sized';
		    			}
			    	};
			    }
			}],
            items: []
        },
        {
            id: '6',
            title: 'The Want of a Nail Back Rooms',
            area: 'dormaus',
            light: true,
			size: {value: '4'},
            content: 'The rear of the general store is the living quarters for the shopkeeper, though he always welcomes visitors. A large wagon wheel is propped against the wall, next to the colourful carpet. A pot of tea is boiling on the warm stove.',
            outdoors: false,
            exits: [
                {
                    cmd: 'east',
                    id: '5'
                },{
                    cmd: 'down',
                    id: '7'
                }
            ],
            playersInRoom: [],
            monsters: [],
            events: [{
			    "id": "Sweatrag Adventurer",
			    "moveMod": 0,
			    "description": 'Mikhail is organising his stock today. It must be tiring work, because he is shirtless, revealing his powerful musclegut, and sweat is dripping from his armpits. He hefts a heavy box up to a high shelf, then wipes his forehead. "Where did I put that one lad?" he says quietly, and slides around some bottles on a shelf. He finds one and pulls it out, and you see that a naked, tiny human is curled up inside. </p>Mikhail pops the cork and snaps his fingers over the top, causing the human to squish and twist like fluid as he emerges from the bottle. As he is warped, his skin turns white and soft, and his limbs and features merge together into fluffy cloth. What lands in his hand is a clean cloth, which the dog wipes across his face, then rubs in his pits to soak up his musky sweat. Once the cloth is dripping and dirty with dog musk, he tosses it aside and goes back to work.',
			    "repeatable": true
			},{
			    "id": "Mayor Stockcheck",
			    "moveMod": 0,
			    "description": 'You are surprised to see Mayor Maine here today, taking notes of the contents of Mikhail\'s stockroom and writing something on a long scroll. He smiles at you when you enter. "Hello young man! Just doing some administration, checking for contraband and whatnot. Not that I mistrust our dear friend Mikhail, of course! Just a generic checkup." he says. He comes to a box of socks, and his eyes gleam. "Oh my! I wonder if these used to be poor, unfortunate humans." he says. </p>He lifts one out, which twitches and wriggles in his grip. "Looks like this fellow still has some spirit!" he chuckles. He leans down, wriggling one of his wide soft paws as he stretches the sock over it, then flexes his toes inside the living fabric. With each twitch it gives, he stomps against the wooden floorboards and presses down his heavy weight. It doesn\'t take long before the living sock stops struggling, and the mayor returns him to the box, with a very obvious bulge in his suit trousers. ',
			    "repeatable": true
			},{
			    "id": "Underwear Swap",
			    "moveMod": 0,
			    "description": 'You sense movement in the storeroom, and look carefully around the darkness before you notice a black anthro rat, skulking in the corner. He slips his pink hand into a box and pulls out a pair of white briefs, and you are about to call out thief, but then he jerks back and falls to his rear. </p>The briefs bulge and swell, stretching wider and larger as they become full and heavy. Meanwhile, the rat whimpers and struggles, his body flattening and hollowing out. His limbs shrink down and away, and his fur flattens to cotton. Meanwhile, the briefs form fur, and limbs, and start to stand up as a bulge appears and slowly resolves into a long rodent head. The former briefs blinks his new eyes and touches his muzzle experimentally, while the rat\'s own head opens up wide, becoming just the waistband of the pair of lifeless black underwear he is becoming. </p>The new white rat picks up the black boxers, and Mikhail shouts from the next room. "You can have those on the house! Have fun being a living being!" he yells. The new rat grins happily and pulls the unfortunate thief up around his crotch, then gives his big rat balls a stroke through the fabric before leaving.',
			    "repeatable": true
			},{
			    "id": "Sock TF",
			    "moveMod": 0,
			    "description": 'The back room of the store is packed with boxes and objects. Bags of unidentified items are shoved into the shelves, and strange packages are stacked haphazardly everywhere. </p>There\'s no one here at the moment, and you can\'t help but wonder if you could get away with grabbing something from the piles. If you do, though, you might end up punished in a way you can\'t recover from. Do you want to risk it? </p>WARNING: If you fail, you will lose your character permanently!',
			    "repeatable": true,
			    "effect": function(player) {
			    	player.confirmStep = {
		    			yes: 'You look around, make sure no one is looking, and then pull open one of the bags and grab something from inside. You pull it out, eager to see your loot, and are disappointed to see that it\'s just a soft cotton sock. As you\'re looking at it, you are suddenly shoved from behind. A powerful arm holds you against the wall, and you turn your head to see Mikhail, the shopkeeper, his dog face twisted in a snarl. </p>“A little thief, is it? You thought you could steal from this shop? You will replace the item you tried to sneak away with!” The powerful dog twists you around, and you find yourself falling to your knees in front of him. His grey-streaked shaggy muzzle looks down at you disdainfully, and his stocky body seems larger and stronger than it was before. You struggle to move, your body feeling strangely weak and floppy. As you\'re fighting to stay upright, Mikhail sits down on one of the boxes, and reaches down to tug off his heavy boot, revealing a footpaw that\'s covered with a sweat-soaked white sock, the soles gritty and stained with dirt. </p>You are forced to watch him slowly pull the sock down off his wide and powerful foot, and you can taste dry cotton in your mouth. Your skin feels rough and dry, and your insides feel empty and light. You flop forward, your limbs not responding, while your mouth stretches open, wide and round. </p>You are shrinking, the room around you seeming to stretch away into the distance, while Mikhail towers over you like a colossus. Your limbs feel like they\'re fusing together, melting into the cotton tube of your body, leaving you completely hollow inside. </p>The handsome old dog walks over to you, his bare paw next to your limp body, then leans down and picks you up. </p>You dangle in his grip, until he sits down, leans forward, and stretches your mouth over his toes. As your body slides over his foot, you can smell his manly, mature scent, and taste his hard-working sweat. His toes wiggle deep inside you as you are wrapped completely around the old dog\'s foot, and then your vision is plunged into darkness when he shoves you back into the sweaty, hot, humid darkness of his leather boot. </p>You are forced to taste and smell the dog\'s foot, feeling his weight press down on you with each step as he returns to work. No one will ever know that his dirty, sweaty sock is actually a trapped living being.',
		    			no: "It's not worth the risk. You leave the goods alone, deciding to earn money legitimately.",
		    			yesEffect: function(player) {
		    				player.description = "This is a dirty sock being worn by Mikhail. For some reason the sock reminds you of a " + player.sex + ' ' + player.race + ' once called ' + player.name;
		    				player.trapped = "You are just a dirty sock, trapped around the large and musky footpaw of a handsome old dog. In the dark prison of his boot you can only taste and smell his sweat helplessly."
		    			}
			    	};
			    }
			},{
			    "id": "Sock1",
			    "trapevent": true,
			    "moveMod": 0,
			    "description": 'It\'s particularly humid and sweaty inside your owner\'s boot today. Mikhail is doing a lot of work restocking the back room, and the sweat from his paw is soaking into your fabric and making you feel heavy and wet. Being sweat-soaked is a strange feeling. Your thoughts become slow and stupid, and you almost seem to get drunk on the stink and taste of your owner\'s feet. </p>As the day wears on, his hot, heavy foot stomps down on you over and over, his scent grinding into your fibres. His sweat is filling you completely, and the smell of him is inescapable. The sweat is filling you up so much that there feels like there is no room for your thoughts and memories. They are pushed away and drift aside, to make more room for stink, to make you a more absorbent, helpful sock. You don\'t remember growing up anymore, but you know you are being a good object, and that\'s what counts.',
			    "repeatable": true
			},{
			    "id": "Sock2",
			    "trapevent": true,
			    "moveMod": 0,
			    "description": 'It\'s a quiet day in the store, and Mikhail doesn\'t have much to do. He lifts his feet up, and you feel yourself rise up as you are covering his paw. Then he reaches over and starts to pull off his boots. Air blows over you, for the first time in what feels like days. Your sweat-soaked cotton body can finally air out. You are so damp you can feel every curve of his toe as your body clings to his paw. </p>You take the opportunity to look around, since you rarely get to see anything but the dark insides of your owner\'s boot. You\'re resting on a rickety wooden stool in the store, and the door is closed. From here you can tell Mikhail has turned the sign on it to say \'Closed\'. </p>He reaches down, bending over to touch you, and his fingers slide under your hem and stretch you slightly. He pulls you down, peeling you from his foot, and you dangle from his grip, empty and cold and helpless. Without your owner\'s foot inside you, you feel completely wrong. So hollow and weak and light. You are not left like that for too long, though. Mikhail pulls you down towards his crotch, and you smell and taste his thick and powerful cock as it slides up inside you. </p>His hand squeezes around you, closing your fabric around his dick as he starts to pump you up and down. Pre soaks your former head, and you are helpless to do anything but go along for the ride. Before long, his cock throbs inside you, and you taste his hot, sticky cum, as it splatters all over your insides. The musky mess soaks your fibres, and you are forced to taste and smell it constantly. Then, he pulls you back over his foot, and returns you to your dark prison, now with the musky aroma of cum to go with your daily sweaty torment.',
			    "repeatable": true
			}],
            items: []
        },
        {
            id: '7',
            title: 'Dormaus Marketplace',
            area: 'dormaus',
            light: true,
            content: 'The marketplace is busy every time of day. Traders walk through, leading strange beasts of burden laden with goods, from walking birds to four-legged armored monsters. Colourful tents ring the marketplace, and between them, merchants hawk their wares from carpets or hand baskets.',
            outdoors: false,
            exits: [
                {
                    cmd: 'north',
                    id: '13'
                },{
                    cmd: 'up',
                    id: '6'
                },{
                    cmd: 'east',
                    id: '2'
                },{
                    cmd: 'west',
                    id: '8'
                },{
                    cmd: 'south',
                    id: '16'
                }
            ],
            playersInRoom: [],
            monsters: [{
				name: 'Saberhagen',
				displayName: 'Saberhagen',
				level: 15,
				short: 'Saberhagen',
				long: 'Saberhagen the black cat is here, sitting at his stall.',
				description: 'Saberhagen is a male black cat, wearing a wide-brimmed pointed hat.',
				inName: 'Saberhagen',
				race: 'cat',
				id: 51,
				gold: 1000,
				merchant: true,
				area: 'dormaus',
				weight: 245,
				diceNum: 2,
				diceSides: 8,
				diceMod: 5,
				meleeRes: 10,
				str: 16,
				position: 'standing',
				attackType: 'claw',
				damRoll: 10,
				hitRoll: 10,
				ac: 20,
				wanderCheck: 38,
				itemType: 'mob',
				runOnAliveWhenEmpty: false,
				behaviors: [],
				items: [{
					name: 'Vore Spellbread',
					short: 'a loaf of Vore Spellbread',
					long: 'A loaf of Vore Spellbread was left here' ,
					area: 'dormaus',
					id: '410',
					level: 1,
					itemType: 'food',
					material: 'bread', 
					weight: 0,
					slot: '',
					value: 45,
					equipped: false,
					spawn: 3,
					onEat: function(player, roomObj, item) {
						var vorespell = {
					            "id": "vore",
					            "display": "Vore",
					            "mod": 0,
					            "train": 100,
					            "type": "spell",
					            "wait": 0,
					            "learned": true,
					            "prerequisites": {
					                "level": 1
					            }
					        };
					    	Character.addSkill(player, vorespell);
						
						World.msgPlayer(player, {
							msg: 'You feel a strange power flowing through you from your belly up to your mind. You now know the secrets of the Vore spell!',
							styleClass: 'cmd-drop blue'
						});
					}
				},{
					name: 'Rabbit Spellbread',
					short: 'a loaf of Rabbit Spellbread',
					long: 'A loaf of Rabbit Spellbread was left here' ,
					area: 'dormaus',
					id: '411',
					level: 1,
					itemType: 'food',
					material: 'bread', 
					weight: 0,
					slot: '',
					value: 35,
					equipped: false,
					spawn: 3,
					onEat: function(player, roomObj, item) {
						var rabbitspell = {
					            "id": "rabbit",
					            "display": "Rabbit",
					            "mod": 0,
					            "train": 100,
					            "type": "spell",
					            "wait": 0,
					            "learned": true,
					            "prerequisites": {
					                "level": 1
					            }
					        };
					    	Character.addSkill(player, rabbitspell);
						
						World.msgPlayer(player, {
							msg: 'You feel a strange power flowing through you from your belly up to your mind. You now know the secrets of the Rabbit spell!',
							styleClass: 'cmd-drop blue'
						});
					}
				},{
					name: 'Fox Spellbread',
					short: 'a loaf of Fox Spellbread',
					long: 'A loaf of Fox Spellbread was left here' ,
					area: 'dormaus',
					id: '412',
					level: 1,
					itemType: 'food',
					material: 'bread', 
					weight: 0,
					slot: '',
					value: 25,
					equipped: false,
					spawn: 3,
					onEat: function(player, roomObj, item) {
						var foxspell = {
					            "id": "fox",
					            "display": "Fox",
					            "mod": 0,
					            "train": 100,
					            "type": "spell",
					            "wait": 0,
					            "learned": true,
					            "prerequisites": {
					                "level": 1
					            }
					        };
					    	Character.addSkill(player, foxspell);
						
						World.msgPlayer(player, {
							msg: 'You feel a strange power flowing through you from your belly up to your mind. You now know the secrets of the Fox spell!',
							styleClass: 'cmd-drop blue'
						});
					}
				},{
					name: 'Rescue Spellbread',
					short: 'a loaf of Rescue Spellbread',
					long: 'A loaf of Rescue Spellbread was left here' ,
					area: 'dormaus',
					id: '413',
					level: 1,
					itemType: 'food',
					material: 'bread', 
					weight: 0,
					slot: '',
					value: 15,
					equipped: false,
					spawn: 3,
					onEat: function(player, roomObj, item) {
						var rescuespell = {
					            "id": "rescue",
					            "display": "Rescue",
					            "mod": 0,
					            "train": 100,
					            "type": "spell",
					            "wait": 0,
					            "learned": true,
					            "prerequisites": {
					                "level": 1
					            }
					        };
					    	Character.addSkill(player, rescuespell);
						
						World.msgPlayer(player, {
							msg: 'You feel a strange power flowing through you from your belly up to your mind. You now know the secrets of the Rescue spell!',
							styleClass: 'cmd-drop blue'
						});
					}
				}],
				topics: {
				    name: '"My name is Saberhagen, sorceror of the Tower Spiral. I am here to purchase spell components and do research."',
				    job: '"I am a sorceror, one who commands the art of magic. I could teach you some basic spells, for a price."',
				    bye: '"Fare thee well, fellow traveller."',
				    components: '"I will purchase any plants that you find around the town, or in the farm to the south. I believe in your language, I would say...simply type sell item, and I will buy it."',
				    spells: '"I have spells of Fox, Rabbit, Rescue, and Vore at the moment. Rescue is twenty gold. The spell Fox will cost thee thirty gold, and the spell Rabbit a reasonable forty. Vore, as a more potent spell, is fifty. I can provide them in the traditional form of spellbread."',
				    spellbread: '"You have not heard of spellbread? The art of the baker is the most magical and potent of all. They store the power of the cosmic spiral in the whirls of their soft dough. Eating spellbread teaches you magic."',
				    paws: '"You are a peculiar adventurer. My paws are well, thank you for your concern. Their appearance? Long and black, I suppose?"',
				    cock: 'The cat blinks with surprise, and then his cheeks turn pink. "I...I suppose it is fine. Th-thank you. I am not accustomed to this country\s open and eager affections."',
				    fox: '"The spell Fox can transform the target into a fox. It is a part of a three-spell combination, along with Rabbit and Vore."',
				    rabbit: '"Traditionally the spell Rabbit is used after a sorceror has cast Fox upon themselves. They they use Rabbit on their opponent, before finishing with Vore."',
				    vore: '"The spell Vore can re-awaken the predatory instincts latent within us all. This allows us to devour one another. A barbaric, but traditional practise. Personally, I would never cast such a spell..."',
				    rescue: '"The spell of Rescue can restore a living being from any permanent, otherwise irreversible fate, so long as they have managed to keep their mind intact. It is a spell that the people of Dormaus have never before seen! They were very impressed when I used it to save a delicious little mouse."',
				    orochi: 'Saberhagen is taken aback when you say that. "Where did you hear that name? Orochi is a forbidden spell! The effect is has cannot be reversed, not even by rescue magic! If you should ever encounter such a spell, forget it immediately!"',
				    mastery: 'Saberhagen\'s fur rises with indignation when you mention that spell. "I do not touch dark magic. Such spells were forbidden eons ago, and the recipes for bread of that nature were banished from even the Spiral Archives."'
				}
			}],
            items: [],
            events: [{
			    "id": "Slave Market",
			    "moveMod": 0,
			    "description": 'You are shocked to see a wooden platform erected in the market square today, atop which are three men, naked except for their chains and collars. A human, a wolf, and a chubby, short cat. A crowd in front is shouting auction prices, and you realise this is a slave market! You are thinking about whether you should try to stop this, when a price is decided for the tall and muscular wolf. </p>It looks like a human adventurer has bought him, and the adventurer climbs up the platform, and then, strangely, strips naked. He opens the wolf\'s collar and places it around his own neck, while the wolf takes the man\'s armor and puts it on, then leads his new slave away on a chain. It looks like this is an auction to take the slave\'s place! What an odd town this is.',
			    "repeatable": true
			},{
			    "id": "Furniture Store",
			    "moveMod": 0,
			    "description": 'There\'s a furniture stall in the market today. A satyr, with a muscular brown-skinned chest but the lower body of a furry goat, is selling wooden chairs and tables and other items. An adventuer asks whether he can buy one of the chairs on layaway. The satyr frowns, and says that he can\'t help with that, but the human could pay a sort of deposit now, then get the chair in a month or two. </p>The adventurer agrees, and the satyr toots a strange, lilting tune on his panpipes. The human looks dizzy and confused, and starts to slowly remove his clothing with slow, zombie-like motions. Once he is naked, he gets on his knees, and the satyr starts to adjust him, moving his legs and straightening his back, and pressing his arms to his sides. The man\'s body hardens, and forms wood-like swirls and patterns. His limbs stiffen and become square and hard edged, as his lower body becomes four wooden legs. His back becomes a straight, firm wooden chair back, and his face just a carved pattern in the wood. Soon, there is just an ornate, handsome wooden chair. </p>The satyr grins. "There ya go. Two months and I turn you back and give ya the chair for free – unless someone buys ya before then!"',
			    "repeatable": true
			},{
			    "id": "Saberhagen's Secret",
			    "moveMod": 0,
			    "description": 'Saberhagen is organising his stall, carrying huge stacks of books to and fro and trying to jam them into shelves. You wonder what the books are for, when his magic is bakery-based, and take a quick peek. "Nine Love Lives", "Fifty Shades of Chameleon", "Love on a Hot Tin Roof", "Cat Love Amongst the Pigeons". These are romance novels! Saberhagen clearly has a secret vice. He turns and sees you looking, and his face turns pink with a blush. </p>"How rude! To spy upon my correspondence. I know just the punishment.", he says. He waves his hand, and your vision is suddenly filled with swirls and colours. Your body goes slack as you droop forward, your face stretching into a stupid, drooling smile. Saberhagen good kitty. Saberhagen master. Do what kitty says. For the rest of the day, the cat uses you to carry his books around and clean his stall, and you follow him like a big puppy, your mind filled with simple, happy thoughts. He eventually restores your free will and lets you go, but you get the feeling your intelligence didn\'t quite come back all the way yet. Nice kitty...',
			    "repeatable": true
			},{
			    "id": "The Hood",
			    "moveMod": 0,
			    "description": 'While in the marketplace, you spot a young dog, possibly one of Mikhail’s many sons, talking to Flute. In Flute’s right arm are a few purple and blue hoodies, looking quite out of place in this rural fantasy environment. The one on top looks almost custom made for the dog, with extensions on the hood to accommodate for his ears. On the hood there is also yellow eyes that almost seemed to glow and little cloth teeth on the brim. The dog seems very eager, a small bag of gold in his hand and his tail wagging behind him.</p>Flute eagerly takes the gold out of the dog’s hands and tosses him the hoodie from the top of the pile. “Enjoy the purchase, and you know what? Have the rest on the house!” Flute tosses the rest of the hoodies onto the ground nearby and walks away, counting the money in the bag. The dog quickly and happily puts on the article of clothing, his tail wagging even more. He zips it up, leaving the hood down and looks at it on his body. It fits snug, and comfortably on him. He smiles and emphatically exclaims “This stange garment is cool!”</p>While the dog admires his new hoodie jacket, you spot the hood oddly move up on its own. With a single soft FWIP, the hood completely covers all of the dog’s face except for the mouth. “AH!! What the?!” yells the dog. He starts to pull at the hood panicking, trying to get it off. The “eyes” of the hood begin to glow even brighter with an unearthly light. The strings of the hood pull down further, all on their own, tightening the hood to his head. Soon you see the dog hunched over, his tail no longer wagging and his arms hanging down. In a matter of seconds, all struggling was over. You then see him walk slowly toward the pile of hoodies. He picks them up one by one, slowly his arms move as he was a zombie. When he finally picks all of them up, he starts walking by you and you hear in a deeper monotone “I… Obey… The … Hood…” </p>After he passes by, you resolve to yourself to not buy any strange pieces of clothing from flute, or wear one of those hoods. [Guest event by CJMPinger]',
			    "repeatable": true
			},{
			    "id": "Saberhagen's Hunger",
			    "moveMod": 0,
			    "description": 'You are poking around in the marketplace, which is pretty difficult on tiny mouse legs, when you notice something a bit odd. Saberhagen, normally so refined and dignified, is staring at you with wide, unblinking eyes. You experiment by walking left for a while, then right for a while, and he turns to watch you with an unbroken stare. It\'s a little unsettling. </p>Now that you are paying attention, you notice he is vibrating with barely-concealed intensity. Being a wizard from a foreign land, he isn\'t a horny sex-lunatic like everyone else in this town, so it\'s pretty weird to see him acting so uncharacteristically. Do you want to go ask him what\'s wrong?',
			    "repeatable": true,
			    "valid": function(player, command, fn) {
			    	if (player.race == "mouse") {
			    		return true;
			    	} else {
			    		return false;
			    	}
			    },
			    "effect": function(player) {
			    	player.confirmStep = {
		    			yes: 'You scurry over on four legs, intending to ask Saberhagen if something is wrong. The moment you get within arms range, your world is suddenly turned upside down! The cat wizard has lunged forward with lightning speed and grabbed you in one of his hands, and you are dangling upside down by your tail, dizzy from the haste. You notice his claws are out, shiny and gleaming, and his mouth is drooling. </p>You wiggle and squeak, and he looks around nervously, before ducking away into his stall where he is less likely to be seen. “I try so hard to remain dignified...” he mutters, dropping you on the ground. When you scurry to escape, he lunges and slams a hand down in front of you, and when you retreat, he does it again. He purrs with amusement. “But this absurd town...letting MICE wander around freely! How can I resist that?!” he says. </p>He snaps his fingers, and his robes and hat disappear in a puff of smoke. His body is lean and lithe, his black fur sleek on his slender body. Between his legs, a furry black sheath holds a firm, erect cock. The whiteness of his grin stands out in the dark tent, almost like a cheshire cat\'s disembodied smile. Whether or not you find him attractive, his actions have triggered some primal survival instinct in your rodent body, and you spin around for a moment and flee as fast as your little paws will carry you. </p>There is another puff of purple smoke, and suddenly you are running along a warm, black, furry surface. You look up to see that you\'re walking on Saberhagen\'s chest, running towards his face! He grabs you, and drops you down between his legs, where you land on his twitching cock. Before you can move, he grips you firmly in his hand, and starts to squeeze you against his shaft. He pulls you up and down, rubbing you along his cock like a fuck toy, and the more you squeak and struggle, the more he purrs and the harder he gets.</p> “Mice...are toys. Toys for cats to amuse themselves with, and then, when we are done...to devour.” he whispers. Pre oozes from his bulging cock and soaks your fur, making you slippery and wet. Just as his cock is bobbing and tensing, about to blow, he grabs you in his hand and lifts you to his mouth. You dangle over his wide, gaping maw, until suddenly your stomach lurches as you are dropped. You land on Saberhagen\'s wide, rough tongue, clinging on desperately as his fanged maw closes around you. In the dark wetness of his mouth, his tongue slides up and down your chest, licking and teasing you all over. You can feel how aroused he is by his panting breath over you, and the moans and purrs emitting from his throat. </p>With a shudder, and a loud, orgasmic moan, the cat blows his load, and swallows you at the same time. You are sent down a slippery slide along the tight confines of Saberhagen\'s throat and into his belly, even as he soaks his own chestfur with creamy cum.',
		    			no: "Saberhagen\'s a nice guy, but mice and cats don\'t get along. You leave, running quickly on four legs, and try to shake the feeling of imminent terrifying predation coming from the cat\'s stall.",
		    			yesEffect: function(player) {
		    				var roomObj = World.getRoomObject(player.area, player.roomid);
		    				
		    				var respawnRoom = World.getRoomObject('overworld', '3');
		    				
		    				Room.removePlayer(roomObj, player);
		    			
		    				player.position = 'standing';
		    				
		    				player.predator = 'Saberhagen';
		    				
		    				respawnRoom.playersInRoom.push(player);

		    				player.roomid = respawnRoom.id;
		    				player.area = respawnRoom.area;
		    				player.chp = 1;
		    				player.cmana = 1;
		    				player.cmv = 7;
		    			}
			    	};
			    }
			}]
        },
        {
            id: '8',
            title: 'Fountain Plaza',
            area: 'dormaus',
            light: true,
            content: 'The tinkling of a fountain fills the air, mixed with the cheerful music of an accordion coming from the musicians playing nearby. This round plaza is ringed with many beautiful flowers, and in the center, a beautiful fountain endlessly sprays glimmering water.',
            outdoors: false,
            exits: [
                {
                    cmd: 'north',
                    id: '9'
                },{
                    cmd: 'east',
                    id: '7'
                },{
                    cmd: 'west',
                    id: '10'
                }
            ],
            playersInRoom: [],
            monsters: [],
            events: [{
			    "id": "Fountain Statues",
			    "moveMod": 0,
			    "description": 'You hear a splashing sound and laughter coming from the fountain, and walk in to see a dog and a goat playing in the water. They\'ve taken off their clothes and left them folded on the side of the stone basin, and are splashing water at eachother, naked. The goat bleats and clambers up higher on the fountain, then turns around, his cock proud and erect in the air. He starts to move, but his legs jerk as if his hooves are glued to the stone. He bleats in confusion, then yelps as his cock starts to emit a forceful stream of clear, pure water. He grabs hold of it, only for his cock and his hoofed hand to start to turn cold, and grey. His movement becomes slower and sluggish, the texture of stone flowing up from his hooves and along his chest. He opens his mouth in a bleat, only for the stone to cover his head, trapping him in still stone silence. </p>The dog, shocked by this, made the mistake of opening his mouth in surprise. The stream of fountain water from his friend\'s cock sprays right into the dog\'s open mouth, which then stiffens and hardens too. The dog shudders in strange arousal, allowing the water to enter him, and feeling his own cock harden, turn grey and firm, and spray its own fountain. His fur flattens and hardens to stone, and his paws in the water become smooth granite. Judging by his last few slow motions, rubbing along his belly, the feeling of petrification is intensely erotic. Before long there are two new statues in the fountain, the same as all the rest.',
			    "repeatable": true
			},{
			    "id": "Pink Bath",
			    "moveMod": 0,
			    "description": 'Your nose wrinkles as you enter the fountain plaza. The source of the odd odor is immediately evident. A crass dog adventurer uses the fountain as his personal bath, nude in the water. His gear heaped on the ground beside it. The rogue muddies the water as he scrubs his body. A pink bar of soap on a rope tied around his wrist. </p>You watch the rogue brazenly clean himself in the public fountain. It leaves you to wonder if it will ever be a pristine spectacle again. The musky dog doesn\'t seem to care who is watching or what he effects. He uses the soap to scrub his body clean, focused solely on himself.</p>As he washes his body you begin to notice something he doesn\'t. As the brown dirt washes away, his greyish fur is revealed. One more rinse and you see the fur get lighter. He soaps his whole body and then slops in the water, dunking himself in. With his eyes closed he doesn\'t notice that his fur is turning...pink.</p>That isn\'t the only change. The mutt\'s muzzle has become more slender. His sleek thin tail frizzes out, as if when dry it would be bushy. You bit your lip as the dog hums and soaps his hefty sheath and balls. You owe the crass dog no favors, let him find out himself. You watch him massage his junk, a thick cock starting to slip free. He is a fine specimen of a male. </p>Until he dives back under the water.</p>You have to stifle a laugh. A male dog dove under the water, but what rose out was a beautiful vixen. Wide hips, feminine features, all on a sleek body. To make things worse, the vixen\'s fur was a bright unmistakable pink. A soft feminine mix of pink, creamy underbelly, with dark black gloves.</p>A satisfied moan rose up from the vixen\'s throat. The demure sultry moan enough to make her open her own eyes. She grasped her perky bosoms, he eyes went wide as saucers. An exploring paw slipped down between her legs, confirming for the roguish hound what he had become.</p>"No...no....NO!" She shrieked, fingering her tight new pussy, "Where is that fox who sold me this shit, I\'ll make him pay!"</p>You can only shake your head at the scene. It only confirms your suspicion what had caused the change, and where it had come from. You consider following the angry vixen, but you suspect you already know the outcome of that too.</p>A well bred vixen would soon be stumbling out of the merchant\'s tent wearing a free t-shirt. Probably reading something along the lines of, "Another satisfied customer!" [Guest event from Fahlma]',
			    "repeatable": true
			},{
			    "id": "Donkey Hypnosis",
			    "moveMod": 0,
			    "description": 'There\'s a performer in the fountain plaza today, a tall donkey with a pointed green hat. To go with the hat he has a pink tunic and peculiar polka-dot bow tie, but his lower body is naked, allowing his long black donkey dick to hang free above his heavy ballsack. </p>He is playing a large accordion, the music giving a jaunty and cheerful atmosphere to the plaza. The music makes you feel a little dizzy though, and bizarrely aroused. Do you stay to keep listening?',
			    "repeatable": true,
			    "effect": function(player) {
			    	player.confirmStep = {
		    			yes: 'The longer you listen, the dizzier you feel. You get closer to the musician, staring at his cock while he plays. His long donkey dick sways left and right, and your eyes follow it, while the music plays spirals in your brain. When he starts to speak, you don\'t hear it. All you can hear is the music. All you can see is cock. </p>"Come one come all, see the amazing hypnotised adventurer! This one will suck any cock at all!" he says. A tiger approaches, and pulls off his loincloth to show off a rock-hard cock standing up from his fuzzy pubes. You find yourself moving over to it, opening your mouth, and taking the tiger\'s shaft eagerly. He humps and fucks your throat, ignoring your comfort, but you happily allow him to use you. He pulls out and finishes on your face, thick jets of cum splattering over you and dripping from your chin. You just stare dumbly and dizzily, before a chubby pig comes next. </p>You love feeling your nose shove into his fat crotch as you get facefucked. Next comes a bear, and then a horse. You lose track of how many men use and abuse you, but when you eventually wake up and get your mind back under control, your upper body is soaked with cum, and the donkey musician is walking off with a heavy bag of donations for the show.',
		    			no: 'The music is great, but you make your way out from the plaza, stroking your crotch as you leave to relieve your arousal and need.'
			    	};
			    }
			},{
			    "id": "Puppet Performer",
			    "moveMod": 0,
			    "description": 'There\'s a new performer in the fountain plaza today. An anthro monkey, tall and rather ruggedly handsome. He\'s cranking a hand organ, and making a rather janky, unpleasant tune from it. A human walks past and tells him to shut up that racket, and the monkey grins and tosses the organ over to him. </p>The human blinks with surprise, before he starts to grip and turn the organ handle. The monkey, meanwhile, takes two wooden X shapes in his hand-like feet. Strings emerge from the wood, and wrap around the human\'s limbs like a puppet. The human starts to dance as he turns the organ, and his body begins to shrink, leaving his armor oversized and ridiculous. His face pushes forward into a rounded muzzle, and his ears grow huge and round. A small tail emerges from his rear, before his flesh starts to become hard and firm. His elbows clank and clink, becoming wooden joints, while his face stretches into a fixed smile. Soon, the monkey man is manipulating a small wooden monkey puppet, just a mindless toy playing a silly little song.',
			    "repeatable": true
			}],
            items: []
        },
        {
            id: '9',
            title: 'Churchyard',
            area: 'dormaus',
            light: true,
            content: 'The churchyard is a place of quiet and respite from the otherwise busy streets of Dormaus. Benches sit amongst the grass, under the shade of tall trees. To the north, the wooden church itself stands, with a tall pointed bell tower atop it.',
            outdoors: false,
            exits: [
                {
                    cmd: 'north',
                    id: '15'
                },{
                    cmd: 'south',
                    id: '8'
                }
            ],
            playersInRoom: [],
            monsters: [],
            events: [{
			    "id": "Gargoyle TF",
			    "moveMod": 0,
			    "valid": function(player, command, fn) {
			    	if (player.race == "gargoyle") {
			    		return false;
			    	} else {
			    		return true;
			    	}
			    },
			    "description": 'You walk up the path to the church, and notice something about the statues at the sides of the doorway. While twisted, monstrous stone gargoyles perch atop the door and to the side, one of the platforms where they stand is empty. You think you can see something glinting in the eaves above the empty platform. Do you want to climb up and take a closer look?',
			    "repeatable": true,
			    "effect": function(player) {
			    	player.confirmStep = {
		    			yes: 'You heave yourself up onto the stone platform, and scramble on the cold smooth pedestal. Standing up, you look into the glinting eave, and see only an old penny, strangely stuck into a crevice in the brick. It\'s worthless, so you turn and start to climb back down, only to discover that your feet seem to be stuck to the pedestal. You struggle, but they won\'t move at all. </p>You feel your toes shifting and straining, and you growl strangely as your feet start to stretch out. Your toes curve out into enormous, powerful talons, and the flesh begins to turn to smooth, cold grey stone. Your legs thicken with muscle, but the power and strength comes with a bizarre and smooth coldness when your flesh turns to stone. A long, thick dragon-like tail curves out from behind you, and your hands stretch and grow as they become mighty talons, capable of crushing rock to powder. You snarl with the sound of rocks grinding together, as your back aches and throbs. Two enormous stone wings tear out from behind you, and flap in the air. </p>Your hair falls out, leaving your head smooth and grey, before two thick and heavy horns bulge out from your skull, and curve behind you. Your face becomes stern and fierce as your mouth and nose pushes forward into a beak-like muzzle, filled with sharp stone teeth. Your feet finally move again, and you stumble and fall from the pedestal. Your wings flap, and you land heavily in the soil. You feel incredibly powerful and deadly, and your insides are as cold and still as stone.',
		    			no: 'You decide it\'s best to leave such things alone. This place is spooky enough without lingering.',
		    			yesEffect: function(player) {
		    				player.description = 'This player is a tall, winged stone monster. Their face is curved into a powerful beak-like muzzle, and they have long and powerful horns made of cold granite.';
		    				player.race = "gargoyle";
		    				player.size.value = 4;
							player.size.display = 'large';
		    			}
			    	};
			    }
			},{
			    "id": "Ghost Fox",
			    "moveMod": 0,
			    "description": 'You hear a grunting and moaning from the churchyard, and walk around behind a tree to see a peculiar sight. A fox adventurer is sitting pantsless on the grass, moaning and humping with his tail raised. When you get closer, you see that there is another fox, or something like one. Atop the adventurer is a ghostly, faint blue image of a fox with a horny, lustful grin. </p>The ghost\'s translucent cock is ramming into the living fox\'s ass, and he\'s feeling it as if it were as solid as any real cock. You hear a faint voice, sinister yet almost like just noise in the wind. "Yes, cum for me. Release your life essence!" it says. The living fox whimpers, and strokes his own throbbing cock, before moaning and spraying his cum all over the grass. When he does, his orange fur starts to look strange. His cock is still leaking and spraying something, but it almost looks like glimmering, shining sparkles, that are flowing out from him and up into the ghostly fox above. </p>The living fox\'s cock starts to turn transparent and ghostly, and he yelps as he looks at his hands and realises he can see right through them! Slowly, his orange fades to an icy, barely-visible blue, and he begins to float up off the ground. He turns and spins, confused and aroused, until the other fox ghost grabs him and squeezes him possessively. </p>The other ghost is much more visible now, almost like a floating blue ordinary fox. "Your life force felt great, new guy. You\'ll make a fun little ghost slave for me." he whispers. The wind rises up suddenly, and you are forced to blink. When you open your eyes, you see no one in the churchyard with you at all.',
			    "repeatable": true
			},{
			    "id": "Ghost Wolf: Male",
			    "moveMod": 0,
			    "valid": function(player, command, fn) {
			    	if (player.sex == "male") {
			    		return true;
			    	} else {
			    		return false;
			    	}
			    },
			    "description": 'You feel a chill in your spine, and shudder as you turn to look at a mossy gravestone at the side of the churchyard. Something makes the hair on the back of your neck stand up with trepidation. You brush the moss away, but the name on the stone is unreadable. When you stand up, you have a powerful feeling that there is someone behind you. You turn, and see the shadowy outline of a wolf only slightly visible in the air. His eyes are just glowing yellow lights. </p>Before you can react, he steps forward, his insubstantial body moving into yours with an icy cold, tingly sensation. Your struggle to move, your body no longer reacting under your own power. Your face twists into a new expression, a cruel and smug grin. You feel your body moving seemingly of its own accord, as your hand strokes down your belly and grips around your cock. You pant and moan, with a rough and scratchy voice. You are pounding your cock like you haven\'t felt sexual pleasure in decades. The intensity of your strokes are almost painful, and before long, you are panting and shaking. Your cock spurts and shakes, spraying hot white jizz all over the gravestone. As the sexual pleasure fades, you feel a coldness move out of you, and you see the grinning face of a shadowy wolf before he disappears into the foggy air.',
			    "repeatable": true
			},{
			    "id": "Ghost Wolf: Female",
			    "moveMod": 0,
			    "valid": function(player, command, fn) {
			    	if (player.sex == "female") {
			    		return true;
			    	} else {
			    		return false;
			    	}
			    },
			    "description": 'You feel a chill in your spine, and shudder as you turn to look at a mossy gravestone at the side of the churchyard. Something makes the hair on the back of your neck stand up with trepidation. You brush the moss away, but the name on the stone is unreadable. When you stand up, you have a powerful feeling that there is someone behind you. You turn, and see the shadowy outline of a wolf only slightly visible in the air. His eyes are just glowing yellow lights. </p>Before you can react, he steps forward, his insubstantial body moving into yours with an icy cold, tingly sensation. Your struggle to move, your body no longer reacting under your own power. Your face twists into a new expression, a cruel and smug grin. You feel your body moving seemingly of its own accord, as your hand strokes down your belly and against your pussy. "Never had one of these before...", you find yourself saying, with a rough and scratchy voice. You slide your fingers into yourself, and start to thrust and pump into your pussy like you haven\'t felt sexual pleasure in decades. The intensity of your fingering are almost painful, and before long, you are panting and shaking. Your body pulses with an intense and hot orgasm that leaves you moaning and your body almost steaming with warmth in the cold air. As the sexual pleasure fades, you feel a coldness move out of you, and you see the grinning face of a shadowy wolf before he disappears into the foggy air.',
			    "repeatable": true
			},{
			    "id": "Guest mummy: Male",
			    "moveMod": 0,
			    "valid": function(player, command, fn) {
			    	if (player.sex == "male") {
			    		return true;
			    	} else {
			    		return false;
			    	}
			    },
			    "description": 'The graveyard is covered with fog and vines cover the pathways. Suddenly dirt gives way to sand and you happen upon a large sandstone crypt. A desert heat beats down on you, encouraging you to take refuge inside the tomb. Torches light your way into a large chamber. </p>Open sarcophagi lay in rows. Most are empty, with their lids off to the side. But some are occupied. You hear muffled moans coming from inside the stone boxes.</p>At the center of the chamber lays a golden sarcophagus. With a grinding sound, the lid opens revealing bandaged jackal with glowing yellow eyes. He shuffles towards you from a golden sarcophagus. “Hello. Welcome to my necropolis!”</p>“Over the centuries, many have sought immortality. It is here, where they’ve found it.” The jackal opens a nearby sarcophagus to reveal a squirming mummified male. His arms are crossed over his chest, his legs and tail wrapped together, his cock fastened proudly to his stomach. Over his face, a golden burial mask of a stoic fox. “He has been here for millennia, undisturbed.”</p>His eyes stare at yours as his soft, cloth-covered paws pet your chin. They comfort you as the jackal chuckles, “You’ve traveled so long. You deserve an eternal rest…” Do you join the jackal and his eternal harem? (Guest event from OsirionMummy)',
			    "repeatable": true,
			    "effect": function(player) {
			    	player.confirmStep = {
		    			yes: 'You feel tired while looking into the jackal’s hypnotic eyes. And you say yes to the jackal’s comforting words. “Wonderful! You can be assured that my mummies have the best quality. Not that you\'ll find another embalmer like me out there,” the jackal chuckles as he leads you to a sandstone altar. </p>In ceremonies that last for days but to you seems like minutes, the jackal anoints you with oils, preserving your body.</p>Your master pumps your hardened cock, spilling your juices onto the altar over and over. Gradually your flesh begins to lose moisture until your cock and your entire body is a jerkied shell of its former self.</p>The jackal chants over your body as bandages crawl from their stone cabinets and adhere to your limbs. As the bandages tightly wrap, they leave not one hair or patch of skin free. And eventually they cover your eyes, snuffing out your vision.</p>Another layer is added to you, and another. Finally the flurry of bandages seems to stop. And the jackal picks you up, taking you to your sarcophagus. A golden burial mask is placed on your head, where it adheres tightly.</p>In darkness, you feel one last stroke on your sensitive bandaged cock. “Sweet dreams, my slave.” You hear the lid closing over your tomb, and then nothing…',
		    			no: 'In a moment of clarity, you decline. The jackal’s linen-covered ears droop a little bit. “The boon of immortality is not for everyone. Please, feel free to stay as long as you want in here.” The jackal offers you tea before guiding you back out of his tomb. His teeth flash under his snout bandages, “Please, feel free to visit again.”',
		    			yesEffect: function(player) {
		    				player.description = "There is a lifeless mummy in a golden sarcophagus here. If you could read hieroglyphics, you would see it is a " + player.sex + ' ' + player.race + ' called ' + player.name;
		    				player.trapped = "You are trapped in the darkness of a glorious sarcophagus. Your body is a silent still mummy, wrapped tightly in bandages, and adorned in golden ornaments. You will remain here forever."
		    			}
			    	};
			    }
			},{
			    "id": "Guest mummy: Female",
			    "moveMod": 0,
			    "valid": function(player, command, fn) {
			    	if (player.sex == "female") {
			    		return true;
			    	} else {
			    		return false;
			    	}
			    },
			    "description": 'The graveyard is covered with fog and vines cover the pathways. Suddenly dirt gives way to sand and you happen upon a large sandstone crypt. A desert heat beats down on you, encouraging you to take refuge inside the tomb. Torches light your way into a large chamber. </p>Open sarcophagi lay in rows. Most are empty, with their lids off to the side. But some are occupied. You hear muffled moans coming from inside the stone boxes.</p>At the center of the chamber lays a golden sarcophagus. With a grinding sound, the lid opens revealing bandaged jackal with glowing yellow eyes. He shuffles towards you from a golden sarcophagus. “Hello. Welcome to my necropolis!”</p>“Over the centuries, many have sought immortality. It is here, where they’ve found it.” The jackal opens a nearby sarcophagus to reveal a squirming mummified male. His arms are crossed over his chest, his legs and tail wrapped together, his cock fastened proudly to his stomach. Over his face, a golden burial mask of a stoic fox. “He has been here for millennia, undisturbed.”</p>His eyes stare at yours as his soft, cloth-covered paws pet your chin. They comfort you as the jackal chuckles, “You’ve traveled so long. You deserve an eternal rest…” Do you join the jackal and his eternal harem? (Guest event from OsirionMummy)',
			    "repeatable": true,
			    "effect": function(player) {
			    	player.confirmStep = {
		    			yes: 'You feel tired while looking into the jackal’s hypnotic eyes. And you say yes to the jackal’s comforting words. “Wonderful! You can be assured that my mummies have the best quality. Not that you\'ll find another embalmer like me out there,” the jackal chuckles as he leads you to a sandstone altar. </p>In ceremonies that last for days but to you seems like minutes, the jackal anoints you with oils, preserving your body.</p>Your master stuffs your clit with oil-soaked bandages. The bandages give you a tingly feeling inside as they draw out moisture from your body and turn your entire body into jerky.</p>The jackal chants over your body as bandages crawl from their stone cabinets and adhere to your limbs. As the bandages tightly wrap, they leave not one hair or patch of skin free. And eventually they cover your eyes, snuffing out your vision.</p>Another layer is added to you, and another. Finally the flurry of bandages seems to stop. And the jackal picks you up, taking you to your sarcophagus. A golden burial mask is placed on your head, where it adheres tightly.</p>In darkness, you feel one last stroke on your sensitive bandaged clit. “Sweet dreams, my slave.” You hear the lid closing over your tomb, and then nothing…',
		    			no: 'In a moment of clarity, you decline. The jackal’s linen-covered ears droop a little bit. “The boon of immortality is not for everyone. Please, feel free to stay as long as you want in here.” The jackal offers you tea before guiding you back out of his tomb. His teeth flash under his snout bandages, “Please, feel free to visit again.”',
		    			yesEffect: function(player) {
		    				player.description = "There is a lifeless mummy in a golden sarcophagus here. If you could read hieroglyphics, you would see it is a " + player.sex + ' ' + player.race + ' called ' + player.name;
		    				player.trapped = "You are trapped in the darkness of a glorious sarcophagus. Your body is a silent still mummy, wrapped tightly in bandages, and adorned in golden ornaments. You will remain here forever."
		    			}
			    	};
			    }
			}],
            items: []
        },
        {
            id: '10',
            title: 'Backstreets',
            area: 'dormaus',
            light: true,
            content: 'The back streets of Dormaus are less busy than the rest. The ground here splits, one path rising up and the other moving down. On both, small cottages stand, each with horseshoes on the doors. At the bottom of the southern lane, a small village post office is open for business.',
            outdoors: false,
            exits: [
                {
                    cmd: 'east',
                    id: '8'
                },{
                    cmd: 'south',
                    id: '11'
                },{
                    cmd: 'north',
                    id: '12'
                },{
                    cmd: 'west',
                    id: 'alley'
                }
            ],
            playersInRoom: [],
            monsters: [{
				name: 'Flute',
				level: 15,
				short: 'Flute',
				long: 'Flute, a thin and sneaky fox man, is checking out the area for goods.',
				description: '',
				inName: 'Flute',
				race: 'fox',
				id: 51,
				area: 'dormaus',
				weight: 245,
				diceNum: 2,
				diceSides: 8,
				diceMod: 5,
				meleeRes: 10,
				str: 16,
				int: 11,
				position: 'standing',
				attackType: 'claw',
				damRoll: 10,
				hitRoll: 10,
				ac: 20,
				gold: 1000,
				merchant: true,
				wanderCheck: 38,
				itemType: 'mob',
				runOnAliveWhenEmpty: false,
				behaviors: [{
					module: 'foxmerchant'
				}, {
					module: 'wander'
				}],
				items: [{
					name: 'Dragon Piss', 
					short: 'a bottle of dragon piss',
					long: 'A bottle of dark yellow dragon piss was left here.' ,
					area: 'dormaus',
					id: '303',
					level: 1,
					drinks: 6,
					maxDrinks: 6,
					itemType: 'bottle',
					material: 'glass',
					weight: 0,
					affects: [],
					value: 35,
					equipped: false,
					onDrink: function(player, roomObj, bottle) {
						World.msgPlayer(player, {
							msg: 'You uncork the bottle of dragon piss, and hesitate for a moment before gulping it down. It burns as it goes down your throat, and you cough and gag at the vile taste. Yet despite that, you find yourself shaking the bottle, desperate for a few more drops. You drop it with shaking hands when you cannot get more piss from it, and realise you are now cursed with an insatiable lust for urine.',
							styleClass: 'cmd-drop blue'
						});
						player.watersports = true;
					}
				}],
				topics: {
					name: '"I am Flute of course! Merchant extraordinaire, nonpariel! One day I will be the richest fox in all the seven lands."',
				    job: '"As a merchant of unparalleled alacrity, I wander these green and pleasant lands looking for trinkets and artifacts to sell to discerning adventurers like yourself."',
				    bye: '"Next time come back with money, my friend!"',
				    cock: 'The fox\'s eyes narrow and his grin grows extremely wide. "My manhood is the longest and finest you have ever seen! I love to have it served by obedient and beautiful creatures."',
					paws: '"My paws?" He lifts one long slender fox foot, and wiggles the toes. "They are, er, fine I think? I suppose you are one of those types, are you? Maybe I should find some boots to sell you."',
					piss: '"From only the finest dragons! You would not believe how difficult it is to bottle this stuff, my friend. For you see, a single drop will change your tastes forever, and make you enjoy things like you never felt before!"'
				}
			}],
			events: [{
			    "id": "Pay Up",
			    "moveMod": 0,
			    "description": 'You are walking along the back streets when your nose tingles from a familiar scent. Master Flute! Any other plans you had are immediately forgotten as you dash over to him, and patiently wait for him to finish talking to a customer before approaching. He grabs you by the waist and pulls you in close, then kisses your cheek and muzzle. "Hey there slave. What do you have for your master, now?" he says with a grin. Without hesitation, you hand over every penny you have earned, and he counts it gleefully with his eyes twinkling. "Good girl! You make me very proud." he says. Your face flushes as you are filled with a renewed desire to make him even more money.',
			    "repeatable": true,
			    "valid": function(player, command, fn) {
			    	if (player.charClass == "prostitute") {
			    		return true;
			    	} else {
			    		return false;
			    	}
			    },
			    "effect": function(player) {
			    	player.gold = 0;
			    }
			},{
			    "id": "Guest Mannequin",
			    "moveMod": 0,
			    "description": 'While walking along the roads of Dormaus, you feel a light tug on your arm. When you turn around, you see a short, old, human woman standing behind you. She has light, wrinkly skin, piercing yet kind blue eyes, and wispy grey hair tied into a bun on her head with a light pink bow. She is wearing a brown patchwork dress, with plaid patches sew in the fabric. When you fully turn around, facing her she begins to speak up, excited and ecstatic. “I’m Linda, a tailor in town. I’m more of an Artist though.” She pronounces Artist in such a pretentious way that you can actually hear the capitalisation. “Much better than that drivel you see people getting from that stupid fox or that walking father complex of a shopkeep. Besides, my stuff is more beautiful and Avant Gar- Oh, look at me ramble!” </p>You look at her incredulously, wondering what she even wants with you. When she sees your gaze, she coughs and then straightens out her dress before continuing. “When I saw you, I couldn’t help but think of what clothes would look good on you. If I could trouble you for a few moments, may I try some of my masterpieces on your body? You’d make the perfect stand in for one of my mannequins!” Do you accept her offer? (Note: You get a feeling that it will be quite more than a few moments.) [Guest event by CJMPinger]',
			    "repeatable": true,
			    "effect": function(player) {
			    	player.confirmStep = {
		    			yes: 'You accept her strange offer, slightly intrigued by the thought of trying on clothes and modeling. You follow her to her “studio”, as she emphatically calls it. Inside it looks like an average clothier’s, with the exception of all the mannequins. They are all strangely unique. Some are big, some small. Some human, others different morphs and creatures. Some male, others female. And all of them wearing surprisingly erotic clothing. You stand there staring at all the mannequins and clothing around, wondering about the whole sight. After a couple minutes of you standing there, transfixed, you hear Linda says, “Hey, over here!” When you look over, you see a pedestal surrounded by mirrors and the old lady standing beside with a pile of black leather clothing in her arms.</p>You walk over to stand on the pedestal and look at yourself in the mirror. When you see Linda about to try and drape a leather BDSM jacket on you, you try to say something about how she hasn’t measured your size yet. Before the words even leave your mouth, she whispers in your ear, “Good mannequins are silent.” Your mouth shuts up tight, no matter how much you try, not a single sound escapes your mouth. As you begin to thrash around, ready to run out of the studio, she yells in a commanding manner “Stop Moving!” You freeze where you are, unable to move a single inch, even your breathing being halted completely. “There, that’s better. Mannequins aren’t supposed to move, are they?” she says, looking you straight in the face. You stand there terrified, but with a strange arousal inside of you.</p>Linda then proceeds to push down your arms and legs back into a uniform position above the pedestal. No matter how much you try, you stand there petrified. Even your face remains frozen, leaving you staring in horror at what she is doing to you. She lifts your arms and puts on the jacket from before. It fit’s snug against your body, and soon you start forgetting things and calming down. At first it’s small things. Like where you are, whoever this nice lady clothing you is, and why you can’t move. But soon you start forgetting more and more. Your name, your past, your life before you were standing here. All gone into an abyss, inaccessible to your mind. You feel as if you should be worried, but you can’t seem to bring yourself to care, your thoughts beginning to slip away. You don’t even notice when she lifts your legs, one leg at a time, and slips on a pair of tight leather shorts.</p>When she is done “clothing” you, she stands back to appraise her work. A single wrinkled finger on her chin, she stares at you thinking. “It needs something more, to fit the ensemble…” You see, without even truly comprehending anything, Linda go to the backroom of the studio. While she is gone, your body begins to change rapidly. Your skin flattens and hardens into a hard plastic, smooth to the touch. All discernable features flatten into the plastic, becoming even more still and lifeless. All while this happens, your thoughts disappear completely. By the time Linda returns, all that is left is an empty husk. Mindless and still, a mannequin stands where you once were. Linda slips onto the front of the new mannequin a studded leather collar with a single chain link on the front to finish the outfit and exclaims, “Marvelous, another Masterpiece to put with the others!” ',
		    			no: 'You politely refuse the lady and her strange offer. Her face twists into a visage of anger and disappointment. With a single loud, pretentious “Hmph!” she barges off, fuming with rage. You think to yourself \'what a strange old woman\', and carry on with your day.',
		    			yesEffect: function(player) {
		    				player.description = 'There is a lifeless plastic mannequin here. It somehow reminds you of a ' + player.sex + ' ' + player.race + ' once called ' + player.name;
		    				player.trapped = 'You are nothing but a simple plastic mannequin. You exist to mindlessly show off clothing so they can be admired.'
		    			}
			    	};
			    }
			},{
			    "id": "Postage",
			    "moveMod": 0,
			    "description": 'You see a dog and cat arguing about something in front of the town hall. You don\'t catch what they are complaining about, but when you get close, the dog slaps a large paper postage stamp on the cat\'s chest. The cat tries to tug it off, but it seems to be stuck. Brown paper starts to spread out from the stamp, wrapping around the cat\'s limbs with a papery scrape. </p>It spirals around his legs, making him fall over, and pins his arms to his chest. With a muffled yelp, the paper covers his face and head, then tightens as brown packing twine curls around the wrapped-up cat and leaves him wiggling and trapped. The dog kicks the cat-package over to a postbox and leaves, whistling.',
			    "repeatable": true
			},{
			    "id": "Fox TF: Female",
			    "moveMod": 0,
			    "description": 'You pass by Flute\'s tent and find it occupied, the front flap open and revealing the slender fox man lying down inside, counting his money. He notices you staring after a moment, and slides his coins aside with a grin. “Hello there friend! Yes even I, the amazing entrepeneurial Flute, must rest now and then. What is life without a little pleasure every now and then, yes?” He grins at you, and his bushy tail sways. </p>The pose of his body, with one leg bent and the other stretched out, along with the wide and slightly lewd grin on his handsome vulpine face, makes you feel like he might be flirting with you. </p>“It\'s a cold night tonight I hear, and I always have room in my tent for someone who would be willing to make it worth my while.” Scratch that, he is definitely flirting with you. Sleeping with a guy like Flute is bound to have consequences though. Do you accept?',
			    "repeatable": true,
			    "valid": function(player, command, fn) {
			    	if (player.race == "fox" || player.sex == "male") {
			    		return false;
			    	} else {
			    		return true;
			    	}
			    },
			    "effect": function(player) {
			    	player.confirmStep = {
		    			yes: 'You kneel down, and climb forward under the thin cloth of Flute\'s makeshift home. He pokes the tent flap with his toe, and it closes, leaving you in the slightly dark privacy of his tent. It\'s very cramped, and the Fox\'s body is pressing against your own. His eyes seem to glow in the darkness, as do the white teeth in his muzzle. </p>"I have always wanted a blowjob from a beautiful vixen, personally, but I can certainly have fun with a beautiful adventurer of other races." he says, and you feel his warm, furry body press against your chest. His long muzzle strokes across your cheek, and then his mouth opens and his tongue slides into your mouth. He kisses you firmly and forcefully, one arm around your chest holding you close. </p>Despite his short and slender body, he is strong and in charge. He breaks the kiss, then starts to unlace his tunic, before dropping it to reveal his toned chest. The fur is pale white, in contrast to the russet orange of the rest of him. He then hooks one clawed thumb into his leather breeches, and starts to push them down. The white fur of his chest narrowd to a V shape between his legs, where it frames a long and handsome cock, above a pair of fluffy balls. </p>His manhood puts most humans to shame, and in this cramped tent, it is all you can focus on. You find yourself leaning down, wanting to take that shaft into your mouth and taste it, but he takes hold of your head and prevents you. </p>“Ah ah ah! I said that is for vixens, yes? But keep visiting the great Flute, and perhaps one day you will be one. For now though...” he takes hold of you, slides his naked body against you, and pushes you down onto the blankets laid out on the ground. You feel his weight atop you, pressing down on you. His masculine musk fills the small space of the tent as you feel his cock tease against your pussy, doggy-style. His strong hands hold your sides, and you hear him pant as his cock slides inside you. </p>You don\'t think you can possibly take his entire length, but inch by inch he slowly guides it into you, filling you completely. Finally, you feel his balls against your crotch, and you know from the hardness that you can feel up almost to your belly that he is fully inside you. He starts to pump, in and out, pulling out slowly only to force his way inside with a powerful thrust. </p>His cock inside you feels amazing, each motion is making you tremble with lust, and you can\'t get enough. His moans become yips and barks, and you start to realise that you are making the same noises. As his thrusting speeds up, your skin tingles and itches. Orange fur starts to grow, covering your body, while your panting face stretches out into a long pointy muzzle. Your ears grow larger and take on a pointed shape, while you feel your tailbone slide against Flute\'s chest as it grows longer and longer, before developing a fluffy brush of fur. </p>Flute\'s claws dig into your furry sides as he shudders and pants, and you feel heat rushing up inside you. The sticky, hot warmth of his cum filling you up. The pleasure is too much, and you dig your own claws into the blankets as you orgasm too, the pleasure making you moan and shudder as he drives his cock deep into the sensitive heat within you. </p>You lie together for a while, sweaty and messy, before he slowly slides out of you and chuckles. You gather your things together, your head swimming, and thank him before turning to leave. </p>You notice something else besides your new body though – Flute somehow looks like the sexiest, most gorgeous creature you have ever seen in your life. It\'s all you can do not to fall to the ground and call him master. It seems this sneaky fox is changing more than your body.',
		    			no: "You decline, and walk away while Flute stares at you leave, one of his furry hands stroking his crotch the entire time.",
		    			yesEffect: function(player) {
		    				player.description = "This adventurer is a slender, grinning fox.";
		    				player.race = "fox";
		    				player.size.value = 3;
							player.size.display = 'medium-sized';
		    			}
			    	};
			    }
			},{
			    "id": "Fox TF: Male",
			    "moveMod": 0,
			    "description": 'You pass by Flute\'s tent and find it occupied, the front flap open and revealing the slender fox man lying down inside, counting his money. He notices you staring after a moment, and slides his coins aside with a grin. “Hello there friend! Yes even I, the amazing entrepeneurial Flute, must rest now and then. What is life without a little pleasure every now and then, yes?” He grins at you, and his bushy tail sways. </p>The pose of his body, with one leg bent and the other stretched out, along with the wide and slightly lewd grin on his handsome vulpine face, makes you feel like he might be flirting with you. </p>“It\'s a cold night tonight I hear, and I always have room in my tent for someone who would be willing to make it worth my while.” Scratch that, he is definitely flirting with you. Sleeping with a guy like Flute is bound to have consequences though. Do you accept?',
			    "repeatable": true,
			    "valid": function(player, command, fn) {
			    	if (player.race == "fox" || player.sex == "female") {
			    		return false;
			    	} else {
			    		return true;
			    	}
			    },
			    "effect": function(player) {
			    	player.confirmStep = {
		    			yes: 'You kneel down, and climb forward under the thin cloth of Flute\'s makeshift home. He pokes the tent flap with his toe, and it closes, leaving you in the slightly dark privacy of his tent. It\'s very cramped, and the Fox\'s body is pressing against your own. His eyes seem to glow in the darkness, as do the white teeth in his muzzle. </p>"I have always wanted a blowjob from a beautiful vixen, personally, but I can certainly have fun with a handsome adventurer." he says, and you feel his warm, furry body press against your chest. His long muzzle strokes across your cheek, and then his mouth opens and his tongue slides into your mouth. He kisses you firmly and forcefully, one arm around your chest holding you close. </p>Despite his short and slender body, he is strong and in charge. He breaks the kiss, then starts to unlace his tunic, before dropping it to reveal his toned chest. The fur is pale white, in contrast to the russet orange of the rest of him. He then hooks one clawed thumb into his leather breeches, and starts to push them down. The white fur of his chest narrowd to a V shape between his legs, where it frames a long and handsome cock, above a pair of fluffy balls. </p>His manhood puts most humans to shame, and in this cramped tent, it is all you can focus on. You find yourself leaning down, wanting to take that shaft into your mouth and taste it, but he takes hold of your head and prevents you. </p>“Ah ah ah! I said that is for vixens, yes? But keep visiting the great Flute, and perhaps one day you will be one. For now though...” he takes hold of you, slides his naked body against you, and pushes you down onto the blankets laid out on the ground. You feel his weight atop you, pressing down on you. His masculine musk fills the small space of the tent as you feel his cock press against your rear. His strong hands hold your sides, and you hear him pant as his cock pushes inside you. </p>You don\'t think you can possibly take his entire length, but inch by inch he slowly guides it into you, filling you completely. Finally, you feel his balls against your ass, and you know from the hardness that you can feel up almost to your belly that he is fully inside you. He starts to pump, in and out, pulling out slowly only to force his way inside with a powerful thrust. </p>His cock inside you feels amazing, each motion is making you tremble with lust, and you can\'t get enough. His moans become yips and barks, and you start to realise that you are making the same noises. As his thrusting speeds up, your skin tingles and itches. Orange fur starts to grow, covering your body, while your panting face stretches out into a long pointy muzzle. Your ears grow larger and take on a pointed shape, while you feel your tailbone slide against Flute\'s chest as it grows longer and longer, before developing a fluffy brush of fur. </p>Flute\'s claws dig into your furry sides as he shudders and pants, and you feel heat rushing up inside you. The sticky, hot warmth of his cum filling you up. The pleasure is too much, and you dig your own claws into the blankets as your cock unloads too, making a messy splatter of cum that forms a hot sticky mess in your new fur. </p>You lie together for a while, sweaty and messy, before he slowly slides out of you and chuckles. You gather your things together, your head swimming, and thank him before turning to leave. </p>You notice something else besides your new body though – Flute somehow looks like the sexiest, most gorgeous creature you have ever seen in your life. It\'s all you can do not to fall to the ground and call him master. It seems this sneaky fox is changing more than your body.',
		    			no: "You decline, and walk away while Flute stares at you leave, one of his furry hands stroking his crotch the entire time.",
		    			yesEffect: function(player) {
		    				player.description = "This adventurer is a slender, grinning fox.";
		    				player.race = "fox";
		    				player.size.value = 3;
							player.size.display = 'medium-sized';
		    			}
			    	};
			    }
			},{
			    "id": "Vixen TF: Male",
			    "moveMod": 0,
			    "description": 'Once again, you find yourself walking through the back streets. You tell yourself you are not going anywhere specific, but your flaring vulpine nose picks up on the scent of horny and virile male fox, and your body knows where you\'re heading. When you come to Flute\'s tent, the sleazy fox is already pantsless, his cock erect in the air as he strokes it slowly. </p>He\'s so hot that you can barely contain yourself. You know it must be some trick or spell, but it doesn\'t change the shivers of arousal that you feel when you look at Flute lying there, waiting for you. </p>"Hello again my new fox friend. Can\'t get enough of this shaft, yes? I understand. You may look male, but you\'re a horny bitch underneath it, aren\'t you?" </p>Do you give in to your desires and enter his tent?',
			    "repeatable": true,
			    "valid": function(player, command, fn) {
			    	var genderlocked = Character.hasEquipped(player, "genderlock");
			    	if (player.sex == "male" && player.race == "fox" && !genderlocked) {
			    		return true;
			    	} else {
			    		return false;
			    	}
			    },
			    "effect": function(player) {
			    	player.confirmStep = {
		    			yes: 'Your tail curls between your legs as you nod, before getting on your knees and crawling into Flute\'s tent. The cramped space feels even more confining this time, and you end up sliding your body on top of Flute\'s. His gleaming eyes look up into yours as his wide grin shines white in the dark. </p>His scent is so powerful, it feels almost like it\'s clutching at your mind. So masculine, so sexual. He slowly pulls off his jerkin, revealing his soft furry chest, and you press your muzzle into it, sniffing and nuzzling him all over. His voice is sultry as he whispers into your pointed ear. </p>"My pheromones are potent, aren\'t they? Calling out into the world that I\'m a sexy stud, needing an obedient vixen to serve me. You\'re that vixen, aren\'t you, yes?" You couldn\'t resist even if you wanted to. Being this close to the gorgeous fox makes you want to do anything he says. You nod, and then you feel a strange power pulse through you. </p>Flute\'s furry paw strokes down your belly and between your legs, where he grips and squeezes your cock. "You don\'t need this.", he whispers. You start to pant and moan as your cock begins to shrink, dwindling in his grasp. </p>Despite your arousal and the hardness of your cock, it is becoming smaller and smaller, along with your furry white balls. Flute rolls you over and lies atop you, then starts to grind his long manhood against your crotch. Your own shrinking shaft is tiny by comparison now. </p>He starts to hump, and thrust, and soon your cock becomes so small it disappears completely into your soft fur. Then, with a strange, overpowering feeling of warmth and wetness, something new opens up between your legs. A tight, virgin passage, just for Flute. He pants with lust as he slowly guides his long dick into your new pussy. </p>With each thrust and stroke, your pussy gets deeper, and you take more of him in. The feeling is like nothing you\'ve ever felt before. You can sense his every motion inside you. He is owning you, mounting you. You are his. He starts to thrust faster and harder, making you moan and gasp in a feminine, girly voice. </p>His hands tease and stroke your chest, where your flesh is growing. A pair of perky, furry breasts for him to grope and squeeze, are forming on your chest. Your waist narrows, your features shifting. Flute is literally fucking you into a vixen. His vixen, that he always wanted. </p>You arch your back in lust as he thrusts in deeper and harder, his face growling and snarling. You can feel his cock pulse and harden, before he suddenly unloads his pent-up seed. Heat rushes up into your belly, the heat and power of your lover\'s cum as it fills you up. </p>Your whole body tingles with electric pleasure as you experience your first female orgasm, but your moans are silenced as Flute forces his muzzle onto yours, kissing you passionately. He looks deep into your eyes and smiles. </p>"What a beautiful woman you are." Once again he looks different to you. To your transformed body and mind, you realise you are looking at him with passionate, uncontrollable love. You would do anything for Flute. </p>You lie with him for as long as you can, while he strokes and enjoys your beautiful feminine body, before you gradually manage to recover enough of your mind to be able to stand up and prepare to leave. He lies on his back, his arms behind his head, naked, and he\'s so unbearably hot that you almost throw yourself back down to worship him some more. </p>You manage to resist – for now – and slip out of his tent, but his voice calls out after you. “Come back soon, gorgeous. Once you get a taste of this cock, you will never want to leave again.”',
		    			no: "Resisting him is the hardest thing you have ever done. You leave, quickly, his virile sexy scent lingering in your nose even when his tent is no longer in sight.",
		    			yesEffect: function(player) {
		    				player.description = "This adventurer is a slender, pretty vixen.";
		    				player.race = "fox";
		    				player.sex = "female"
		    				player.size.value = 3;
							player.size.display = 'medium-sized';
		    			}
			    	};
			    }
			}],
            items: []
        },
        {
            id: 'alley',
            title: 'Alleyway',
            area: 'dormaus',
            light: true,
            content: 'The narrow alleyway between buildings is gloomy and dark. Beyond it, the cheerful town continues to bustle, but within, you feel that shady dealings could easily be afoot.',
            outdoors: false,
            exits: [
                {
                    cmd: 'east',
                    id: '10'
                }
            ],
            playersInRoom: [],
            monsters: [],
			events: [{
			    "id": "Flute Final Step",
			    "moveMod": 0,
			    "description": 'Your clothes feel embarrassing and strange. When people look at you and read the text on the shirt, or stare at your panties, your face feels hot and flushed. Yet you wore them anyway, of your own free will. Now you are here, part of you telling yourself you came here just to explore, but another, increasingly louder part of yourself knowing exactly why you are here. Your vulpine nose is sniffing the air, tracking him down. Your heart flutters whenever you see orange fur or think you hear his voice.</p></p>You are in love with Flute. It may just be some curse he\'s cast on you, some trickery he did, but thinking of him makes you hot all over, and you can\'t seem to stop thinking of him. You adjust your shirt again, and then your ears perk up as you hear him. You dash along a side street and see Flute, your beloved fox, sitting down on a bag of goods and arguing with a sceptical-looking horse. "You don\'t LOOK like some sort of mighty wizard, fox." the horse says. Flute is about to reply, but then he notices you, and a wide grin spreads across his handsome muzzle.</p>"Oh don\'t I? Well, I\'ll have you know I always wanted a vixen to service my cock. A loyal, submissive fox girl who worshipped the ground I walked on and would do anything I say. So I found this adventurer, you know the type, deadly and powerful, and I cast a spell on them so strong they kept coming back to ME wanting to turn into my bitch more! Isn\'t that right, sweetheart?" he says. The last part he directs to you, and the horse\'s eyes widen as you walk forward towards them both. When he reads the words on your shirt, he chuckles with surprise, clearly impressed. Flute\'s own eyes are glowing in the dim alley, and this close, you can smell his manly, powerful aroma. </p>You can\'t drag your eyes away from his perfect body. He reaches down to his pants, and slides his claws into the fly. Then, slowly and teasingly, he draws out that long, perfect, rock-hard fox cock. It stands firm in the air, throbbing with power. Your mouth is watering.</p>"Now the spell\'s almost complete. I have my beautiful fox girl, exactly like I want her. All she needs to do is give me the blowjob I\'ve been craving, and her entire identity will be totally lost when she turns into my perfect, obedient little slave. Go ahead, sweetheart, I know you want it." he says. You barely hear it. Your master needs you. Your perfect beloved Flute. You fall to your knees and take hold of that long, hard cock. Your fingers stroke along the length, tease and rub just the right spots to make your master squirm and growl. Then you open your long muzzle, and slide it slowly into your mouth. </p>It\'s everything you ever wanted. Flute\'s scent and taste stroke along your tongue. The tip of his swollen cock bulges just slightly into your throat, your muzzle exactly the right size for him. Your tongue slides around his shaft, your mouth sucks and pleases him perfectly. Your mouth is designed perfectly to give your master the ultimate pleasure.</p>You are in heaven. You belong to Flute, completely. You could suck his gorgeous cock all day long and never tire of it. Your mind fades and empties out, your inhibitions drained. You don\'t care that you\'re blowing him in public, that the horse is watching all of this. You exist to please Flute. When he finally growls, and his cock unloads its creamy, hot gift into your mouth, it feels better than any orgasm you\'ve ever had, because you know you are serving your purpose. You swallow every drop, then pull your head away and lick Flute\'s cock clean with gentle motions of your tongue. He commands you to stay there and be quiet, then resumes his conversation with the horse. "As you can see, I am powerful enough to get anything I desire. So shall we make that deal?" he says. The horse eagerly agrees, and hands over a sack of gold for some vial of dust that Flute hands over, before he then dashes away with an excited expression. </p>Flute chuckles, and strokes your head. "It\'s worthless, of course. Enchanted, though, so he will think it worked and want to come back and buy more. I always get what I want." he says. He points at his lap, and you sit on it gently as he strokes your chest and fondles your breasts. He whispers in your ear. "Now, how about you be a good girl, and go make Master Flute some money? I\'ll be waiting here when you want to hand in your tribute, my slave." he whispers, his voice like tendrils of smoke in your ear. You nod, unable to resist, and think of how happy you will be to give your beloved master more of what he wants.',
			    "repeatable": true,
			    "valid": function(player, command, fn) {
			    	var flutesbitch = Character.hasEquipped(player, "panties") && Character.hasEquipped(player, "fluteslaveshirt");
			    	console.log()
			    	if (flutesbitch && player.charClass != "prostitute" && player.race == "fox" && player.domsub != "dominant" && player.sex == "female") {
			    		return true;
			    	} else {
			    		return false;
			    	}
			    },
			    "effect": function(player) {
			    	player.master = "Flute";
			    	player.charClass = 'prostitute';
			    }
			},{
			    "id": "Rat Prostitution",
			    "moveMod": 0,
			    "description": 'As you are walking through the backstreets, you pass through an alleyway. A tall, thin rat-man crosses your path and stops you. He\'s wearing an eyepatch and grinning wide on his long pointy muzzle. His pink tail curls around your ankle as he asks you whether you\'d like to make a bit of gold.',
			    "repeatable": true,
			    "effect": function(player) {
			    	player.confirmStep = {
		    			yes: 'You hesitantly agree, and the rat immediately presses you against the alley wall. His long hand strokes along your chest as he licks and kisses your cheek. "Gets lonely out on the job. Glad I found a good hole to fill." he mutters. </p>He pulls down his pants, revealing a grey-furred sheath and a rock-hard cock. With a rough shove, he rams your belly to the bricks of the wall, and starts to rub and squeeze your rear. His cock teases and presses against your hole, before he shoves forward with a lusty grunt. You pant and gasp as the rat rams into you over and over, releasing the pent up lust he has had for weeks. His eager hands squeeze and rub your chest as his furry belly grinds against your back. With a mixture of a moan and squeak, he fills your ass with hot, sticky cum, then shudders and sighs. He pulls out, tosses you a small pouch of coins, and walks off without another word.',
		    			no: 'You refuse and slide past, wanting nothing to do with such a shady character.',
		    			yesEffect: function(player) {
		    				player.gold += 20;
		    			}
			    	};
			    }
			},{
			    "id": "Fahlma: Flute",
			    "moveMod": 0,
			    "description": 'Muffled voices and rustling activity draw you towards some activity in the back streets. It\'s not long before you find Flute leaning casually forward. He sees you approach, a rogueish gleam in his eyes, "Another customer, or just here for a social visit? I\'ll be right with you my friend, just attending to some business." </p>In the instant he turns away you can feel a change in the atmosphere. A tension that makes one hair stand on end. A fluid change that seems to come easily to the fox, polite one moment and serious the next, all while keeping the same shark-like smile. It becomes easier to understand why the smaller fox might be able to keep the taller wolf cornered like he has. </p>The white wolf cringes, tail tucked between his legs. A visible tent eagerly bucking in his pants. You see the firm erection bouncing up and down, as if trying to escape itself. </p>Flute smirks, "Let this be a lesson, no one gets anything for free." You\'re not quite sure if the wolf stole something, or was merely late on a payment. You just silently watch the back alley merchant claim his due. </p>You watch the fox\'s paw slip inside the wolf\'s pants. The tall wolf whines, claws grazing the brick behind him. He is trapped, like cornered prey unable to escape. The bigger wolf seemed helpless as the fox worked. You see Flute\'s paw moving and groping around. The wolf\'s legs shift, his knees  bowing and turning inwards. You\'re not sure what exactly is going on inside the wolf\'s pants,  but you can\'t help but sense the raw sexual power of what\'s going on. Just by watching you feel your gut tighten in sexual arousal. Flute working over the panting wolf is turning you on. </p>The white wolf throws his head back, squirming against the wall with a muted, stangled whine he tries to keep restrained. Flute withdraws his paw from inside the wolf\'s slacks. You stare wide eyed at the sticky clear trails of fluid the connect the fox\'s brown paw with the inside of the wolf\'s pants. The gooey strands unbroken. "Transaction complete, if he wants to reclaim what\'s his he can pay me what he owes me." Flute turns to you with a playfilly sinister grin, "Feel free to come by my shop, my prices are always fair, or perhaps maybe you\'d like a taste of the same? I promise if you do I\'ll be more gentle then I was with our friend here." </p>Flute jerks his head back to the wolf, smearing the fluids on his shirt before walking away with a smirk. The already blushing wolf\'s eyes widen, the laces of his breeches come loose. Whether a deft act by the sly fox, or a mere consequence of their activity, you watch his pants come down. The laces loosen as the soaked pants and underwear succumb to gravity. Giving you a clear view between the wolf\'s legs. </p>A very masculine wolf stands before you, panting and winded against the wall. His cheems red as he tries to steady and collect himself. His knees quiver as he tries to stand. He shudders and pants as if he has run a marathon, or had just stumbled out af a bedroom after a full night of sex. The "he" you realize might be a relitive term now. Between his powerful thighs what you see is anything but male. It seems Flute left him male everywhere except where it counts the most. All you see between the wolf\'s legs is a quivering needy pussy where not moments before you had seen a proud erection straining at his slacks. </p>The wolf gives you one look in the eye, his fur practically turning pink from his burning blush. His ears lay back against his head as his eyes dart away, a act that shows his shame, but perhaps that enjoyed what just happened more than he wishes to admit. </p>You turn back to see Flute\'s tail disappear behind a corner. The fox unapologetic as he struts away, cheerily humming to himself. You turn back to do a doubletake of the wolf. The white lupine stumbles away, holding up his pants as he staggers off in shame and humiliation. Leaving you with more questions than answers, but none bigger than this. What would the fox do to you if you crossed him...or perhaps...even let him? (Guest event written by Fahlma)',
			    "repeatable": true
			}],
            items: []
        },
        {
            id: '11',
            title: 'Town Hall',
            area: 'dormaus',
            light: true,
            content: 'The town hall is a tiny wooden building. On one wall, a notice board is holding a variety of messages and papers, advertising fairs or making requests. A ticking clock on the opposite wall stands above the clerk\'s desk and gives the room a businesslike ambience. The mayor\'s desk is behind a glass panel, in a slightly more cozy-looking room.',
            outdoors: false,
            exits: [
                {
                    cmd: 'north',
                    id: '10'
                }
            ],
            playersInRoom: [],
			size: {value: '4'},
			monsters: [{
				name: 'Mayor Maine',
				level: 15,
				short: 'Mayor Maine',
				long: 'Mayor Maine is organising some papers at his desk.',
				description: 'Mayor Maine is a very chubby and friendly cat. He wears a pair of small glasses, and his fluffy moustache is always twitched into a grin.',
				inName: 'Mayor Maine',
				race: 'cat',
				id: 51,
				area: 'dormaus',
				weight: 245,
				diceNum: 2,
				diceSides: 8,
				diceMod: 5,
				meleeRes: 10,
				str: 16,
				position: 'standing',
				attackType: 'claw',
				damRoll: 10,
				hitRoll: 10,
				ac: 20,
				gold: 1000,
				merchant: true,
				wanderCheck: 38,
				itemType: 'mob',
				runOnAliveWhenEmpty: false,
				behaviors: [],
				topics: {
					name: '"My name is Maine, traveller! It\'s always a delight to meet a new friend."',
				    job: '"Why, I am the mayor of this wonderful little town! I keep everything in order and organised. It is difficult work, but satisfying!"',
				    bye: '"I hope you have a wonderful time in my little town."',
				    cock: 'The cat laughs and winks at you. "Well, I might not be as long as some of these young bucks, but I promise I know how to use it!"',
					paws: 'The mayor\'s eyes suddenly gleam, and his ever-present smile stretches out into a huge grin. "My feet? Oh what a peculiar question! Why, I couldn\'t say how they are. Perhaps you could check them for me?" He leans back on his chair, and rests both his paws on his desk. The chubby toes are soft and look warm and touchable, and the soles are cushiony and a faint peach colour. His voice is suddenly deeper, and darker. "You like this, boy? I bet you are rock hard thinking of my feet on your face, holding you down on the floor where you belong." He sighs and puts his feet back on the ground. "Ahem. Do enjoy your stay in our town!"'
				}
			}],
            events: [{
			    "id": "Mayor's Intern",
			    "moveMod": 0,
			    "description": 'You enter the town hall to see Mayor Maine guiding around a new intern. The mayor is dressed in his usual business suit and sash, and a thin tiger with messy brown hair is following him around, looking stressed out. "Grab those papers", says the mayor, and the tiger nervously does. "Sign that form. Straighten your tie. Greet that adventurer", he moves on. Command after command hits the poor tiger in a never-ending stream, clearly fraying his nerves. Then the mayor leads him into his office, and takes a seat in his huge leather armchair. </p>"Put down those papers. Take off your shirt." says the mayor. The tiger does so, but his expression as he starts to unbutton his shirt and throw it away, revealing his white belly fur, is one of shock and surprise. "Good to see that you\'ve gotten so used to orders that you respond without thinking, boy" says the mayor, with a cruel and wide grin. "Take off your pants and get under my foot", he commands, and the tiger pushes down his pants and crawls over the carpet to get under the mayor\'s wide paw. Despite the fear-sweat on his face, clearly part of his is enjoying this, because he is quite erect. </p>The mayor presses down his foot over the top of the tiger\'s head, his toes spread wide, and starts to rock his footpaw back and forth. The tiger\'s body begins to shrink, and his flesh seems to shimmer and grow soft, allowing the mayor\'s foot to sink into his body. "Lift the left toe on my left foot", Maine says, and the tiger lifts his left arm then drops it. The mayor strokes his crotch as he pushes his feet deeper into the tiger\'s body, spreading his control all throughout his flesh. The tiger\'s head sinks into one of the mayor\'s toes, while his arms are sucked up into adjacent ones. His lower body disappears into the soft fur at the back of the mayor\'s foot, and the cat lifts his paw to show off the face on the underside of his toe. </p>He flexes his foot, as the tiger\'s features start to fade away. "Perfect. Obeying me as effortlessly as my own body part. You were a great intern, my boy" he says.',
			    "repeatable": true
			},{
			    "id": "Mayor Complaint",
			    "moveMod": 0,
			    "description": 'There is a commotion going on in the mayor\'s office today. You stand at the corner and peek in to look closer and see what\'s happening. A tall and burly adventurer is pounding his fists on Mayor Maine\'s desk, and shouting furiously at the cat. The mayor is just smiling under his fluffy moustache, looking completely unpeturbed. "My wizard is a pumpkin, my thief is some dog\'s sock, and now you\'re telling me that there\'s nothing you can do about it?! I\'m not letting you walk all over me, you fat fuck!" the adventurer roars. </p>The mayor strokes the tip of his moustache and smiles widely. "Oh I don\'t need you to let me, dear boy." he says, with a sinister purr. The mayor stands up, and presses one hand on the adventurer\'s chest, before shoving him firmly onto his back. The powerful human protests and struggles, but somehow the chubby cat is controlling him easily. He just lies on the floor helplessly as the mayor lifts his large furry paw and presses it down on the human\'s chest. "I\'ll be walking over you every day", the mayor says. The human pants and shivers as he starts to sink into the deep, plush and soft carpet. </p>His skin turns the same red and brown as the carpet pattern, as he looks almost hairier – except the hairs are growing, thickening carpet fibres. He struggles, but cannot escape. Each time the mayor steps on his body, walks over his chest and face, he is absorbed more and more. His limbs fall apart into plush carpet fabric. His features fade into the pattern, and his equipment disappears. Finally, the mayor is left walking through a thick and fluffy patch of carpet, which wiggles and moves as if it were alive.',
			    "repeatable": true
			},{
			    "id": "Footstool TF",
			    "moveMod": 0,
			    "description": 'The mayor is resting in his chair when you enter today, with his large feet crossed atop his desk. The soles are fluffy and white, and look incredibly soft and appealing. You blink and try to shake off the weird urge you feel, seeing the older cat\'s paws. You have the oddest desire to just sit beneath them so they can rest. The mayor is toying with a gold chain around his neck, and purrs as you get closer. </p>"The new arrival! Glad you have come to give me a visit. Always a delight. I would normally be out on my rounds, but my poor feet are just so tired. Warm and tired, and aching to be rested. Could you possibly help an old cat out?" he says. His voice is rumbling and purring, flowing through your mind and making it hard to think. Yet you know from his glowing eyes, and evil expression, that if you do not escape then you will surely be trapped here under the cat\'s will forever. Do you submit to his will?',
			    "repeatable": true,
			    "effect": function(player) {
			    	player.confirmStep = {
		    			yes: 'The moment you consider serving the cat, you are lost. Your face spreads into an eager, blushing grin, and the mayor\'s eyes narrow into glowing slits. "Such a helpful and dutiful citizen. You are a role model for all the beings here. One day they will all know their place beneath me, as you do." he says. You can barely tell what he is saying, though. Your entire focus is on his perfect, beautiful feet. Before you know it, your fingers are wrapped around his soles, and you are massaging your thumbs through his thick, soft fur. His paws are warm and comfortable to hold, and you feel like you could press them against your flesh forever. You rub and squeeze his toes, and press your face to his sole, feeling the warmth on your cheeks. </p>The mayor pulls them away from you, and you whimper for a moment, before he points down below his desk. Immediately, you obey, crawling under his desk and sitting on your hands and knees with your face gazing up into his wonderful paws. They slowly lower down onto your face, and your mind blanks out. The darkness of his warm paws smothering your vision is all you care about. </p>You don\'t care when your body starts to compress down, and your limbs begin to melt together into your body. You don\'t care when your insides soften and turn to padding and fluff, or when you begin to squish and reshape into a cube. You don\'t even care when your skin becomes shiny, and smooth, and turns into soft red leather. All you care about is the feet, resting on where your face used to be, as you become a boxy leather footstool under the mayor\'s desk. No thoughts. Only feet.',
		    			no: 'You back off, your eyes glued to the mayor\'s feet. He flexes his toes and stares at you with a smug smile as you struggle to leave. "You will be back, dear boy. No one I want ever escapes me forever", he says. The moment you are free from his office, you turn and flee as quickly as you can.',
		    			yesEffect: function(player) {
		    				player.description = 'There is a large red footstool under Mayor Maine\'s desk. Despite its featureless cube appearance, it somehow looks like a ' + player.sex + ' ' + player.race + ' once called ' + player.name;
		    				player.trapped = 'You are nothing but Mayor Maine\'s leather footstool. Your master\'s perfect paws will rest on your former face, and you will never escape the joy of serving his feet.'
		    			}
			    	};
			    }
			},{
			    "id": "Footstool1",
			    "trapevent": true,
			    "moveMod": 0,
			    "description": 'As you sit quietly under the mayor\'s desk, you hear him starting to sigh with boredom. There is a shuffling of papers above you, and then he lifts his wide, fluffy paws and rests them on what used to be your face. You would shiver with delight if you could. His sole rubs slowly back and forth, the thicker pads of his foot pressing into your flat surface. His toes wriggle and stroke across your upholstery. He lifts his feet away, then presses them to your sides. His claws emerge, and tease gently down your sides, the sharp tips leaving a tingly feeling in your footstool body. </p>Then he flexes his paws, presses them both back down on your face, and leaves them there while he works. You lose yourself in the warmth and scent of your owner\'s feet.',
			    "repeatable": true
			},{
			    "id": "Footstool2",
			    "trapevent": true,
			    "moveMod": 0,
			    "description": 'When the mayor leaves for the night, and locks up, you are left alone in the dark office. His scent slowly fades from your body, and the haze of lust and obedience that constantly fills you begins to clear up. You start to remember that you aren\'t just a footstool. You used to be an adventurer! You should fight, should resist. But...you are really missing his feet. </p>You miss having him press his soles onto you. You want him to relax and rest on you. You need your master so badly. By the time the morning rolls around, and your master returns and props his paws back on you, you are utterly weak with withdrawal. His warmth and presence fills you with relief, and crushes a little more of your will. Every day is like this, and every day you lose a bit more of your mind to the simple mind of an obedient footstool.',
			    "repeatable": true
			},{
			    "id": "Mayor Footservice",
			    "moveMod": 0,
			    "description": 'Mayor Maine is here today, sorting through some paperwork. The tall cat is an older man, his belly large and round and heavy, and his orange fur forming a thick fluffy beard-like mane around his neck. He is sitting at his desk, with tall piles of papers stacked haphazardly beside him. He signs another paper, then looks up and notices you. He smiles in a gentle, fatherly way. </p>“Oh, the new visitor! It\'s a delight to have you around. I hate to put you out, but I don\'t suppose you would mind assisting me with this paperwork?” The cat is hard to refuse, and you take a stack of papers and start to sort them as per his instructions. The more you sort and order the files, the more orders the mayor gives you, his tone gradually becoming more aggressive and dominant. You look up to him after you have sorted your own pack, to see his eyes narrowed and an unusual grin on his face. He reaches a finger forward and knocks some papers to the floor. </p>“Whoops! Young man, get on your knees and pick those up.” You hesitate for a moment, then get down under the mayor\'s desk. His large bare orange-furred paws are resting on the thick carpet, and when you try to grab the papers, he slides his foot along them, dragging them out of your reach. You reach forward again, and when you get closer, he shoves his foot forward, pressing his warm, furry sole across your face. The scent is masculine and appealing, and his fur is incredibly soft. You feel flushed with embarrassment, but his voice speaks out, deeper and more commanding. </p>“Stay where you are. I think you will be more helpful as a foot stool.” The mayor strokes his foot along your cheek, then uses his legs to roll you onto your back. Then he rests one paw on your face, and the other on your crotch. Your predicament is clearly arousing the older man, as his suit pants are tenting with a clear erection. He strokes and gropes it as he wiggles his toes on your body, and you find yourself unable to resist his commands. Whenever you move to leave, he presses his weight down on you, and you know you are not going anywhere. </p>Hours later, when he has finished organising the last of his papers, he finally allows you to sit up, your hair mussed and your body covered in his scent. As you leave, he calls after you. “Good boy. Feel free to come back again soon. Maybe you can stay as a footstool forever.”',
			    "repeatable": true
			}],
            items: []
        },
        {
            id: '12',
            title: 'Pumpkin Patch',
            area: 'dormaus',
            light: true,
            content: 'At the outskirts of the village, the well-trodden path gives way to muddy fields and grassy pastures. Here, the scrubby ground is filled with huge, colourful pumpkins, out of season yet vibrant.',
            outdoors: false,
            exits: [
                {
                    cmd: 'south',
                    id: '10'
                }
            ],
            playersInRoom: [],
            monsters: [],
            events: [{
			    "id": "Mayor's Pumpkins",
			    "moveMod": 0,
			    "description": 'You hear a strange crunching noise from deeper in the pumpkin field, and walk over to investigate. To your surprise, you see the chubby form of the mayor, who is wandering around the pumpkins and examining them. His wide and furry footpaws are soaked in pale orange goo. You decide to hunker down and watch him to see what\'s going on. </p>He finds a particularly large pumpkin, and kneels down to stroke and rub its firm orange hide. "Hello there! You are a new one, aren\'t you? So glad you decided to join this little plot and do your part for the town." he says, with his usual jolly cheer. He lifts one paw and starts to stroke and rub it over the pumpkin\'s flesh, his toes teasing and pressing against the vines and all the ridges and grooves of the unmoving plant. Slowly, his claws emerge, which he uses to slide down the pumpkin\'s rind, leaving little white marks. </p>The mayor unzips his pants, and pulls out a thick and furry cock, which he starts to stroke as he steps all over the vegetable. Then, as he starts to reach climax, his moustache twitching and bouncing, he shoves his foot down, putting all his weight behind it. The pumpkin buckles, then splits open, cracking apart as the cat\'s paw squelches deep into the warm and slimy goo within. The mayor yowls, his cock spraying hot white globs of cum into the innards of the ruined plant. He pulls his foot free with a slimy glop sound, and shakes his toes to remove most of the mess, before moving on to find another victim. What a strange cat.',
			    "repeatable": true
			},{
			    "id": "Fertilization",
			    "moveMod": 0,
			    "description": 'You hear a familiar grunting in the fields, and see the tall and dark shape of Grizz the bartender standing amongst the pumpkins. You walk over, and hear a sticky splashing noise as you get closer. It\'s then that you notice that all the pumpkins near grizz are coated in shiny, gooey fluid. The ground is swampy and sticky, and there is an overpowering stink of sex and musk. </p>You get up to Grizz and see him chug a bottle of some strange potion, which he then tosses into a pile of bottles at his feet. He grips his cock and starts to pump and pound it, then notices you looking. "Potion o\' cum." he says, then turns back to the vegetables. With a growl and a snarl, he stamps his foot and unloads a gush of bear cum, which drenches the fields like a musky white hose. He pants and wipes sweat from his forehead. "Fertilizer", he says, and you decide to leave him to his work.',
			    "repeatable": true
			},{
			    "id": "Seeing Plant Vore",
			    "moveMod": 0,
			    "description": 'You wander past the pumpkins and towards the back of the field. It goes on further than you expected, almost into the wilderness beyond the village. This far back, there are a few other plants besides just the pumpkins. You see tall and thick plants with slippery-looking opening at the top, easily larger than most adventurers. They remind you of carnivorous trap plants, except they don\'t look like they\'d be in a good position to actually capture anything. Or so you would think. As you watch, a cat adventurer jogs up to one, and strips out of his clothing quickly. </p>He\'s an orange and brown tabby, with a short but thick erection under his clothes. He starts to climb the plant, scrabbling and struggling a bit to get hold of it. You stop him to ask what\'s up, and he looks over to you with a red-faced blush. "Oh, ah! I got cursed by some cat for being rude. He said I wouldn\'t be satisfied until I was plant food, and, well, I ended up here. I can\'t wait to get inside there and fap!" he says, his blush turning to an eager grin. You step back and let him carry on, and he reaches the lip of the plant, then slides down the slippery, slimy interior. You look around and find a large rock to stand on to get a better view. </p>Inside the plant, the cat is soaking in a pool of green slime, like it was a soothing bath. He strokes his cock as the goo bubbles around him, his face one of utter bliss. The cat meows gently, and bites his lip as he spurts cum over his furry chubby belly and into the goo around him. Then he just crosses his arms behind his head, closes his eyes, and relaxes. The goo bubbles and oozes more as he sinks deeper and deeper. </p>His fur starts to turn green and slimy, and he glistens in the light. Slowly, and with no resistance, he sinks down into the goo, until only his head is sticking up, and then finally, with a sigh of joy, that disappears into the bubbling slime as well. The whole time, the cat had looked so happy and relaxed that it almost makes you want to try it yourself!',
			    "repeatable": true
			},{
			    "id": "Pumpkin TF",
			    "moveMod": 0,
			    "description": 'You stop for a moment to admire the patch of enormous orange pumpkins that is growing all over this patchy grassy area of land. The round orange vegetables are scattered here and there, in all sorts of sizes, their thick vines criss-crossing the ground, and their bulging orange forms making a colourful contrast to the browns and greens of the soil and grass. In fact, there are so many that it almost seems like more are appearing, or moving around, whenever you look away. </p>You lean down to admire a particularly large one, and when you stand up and turn around, two more are at your feet, where you are sure there was a path just moments before. You take a step back, and your foot catches on one of the thick green vines, causing you to fall backwards and land in the grass, with pumpkins surrounding you on all sides. When you try to push yourself back up, you find your arms are tangled up in curling leafy vines, and you struggle to pull them free. </p>It is then that you feel your legs slowly being pulled open, and you manage to lean up enough to see the pumpkin vines slowly curling around your legs! They are moving, and alive! The vines send tendrils slowly up along your legs, curling around your thighs and squeezing you firmly, while more of them hold your arms down and then start to wrap around your torso. One huge, throbbing green vine snakes up past your chin, dripping with green fluid. It presses against your mouth, forcing it open as it pushes its way across your tongue and into your throat. </p>The strange, musky goo makes you feel lightheaded and strange. Your body throbs and pulses with pleasure, and you feel your muscles relaxing. You barely resist as another identical vine slides along your ass, teases your hole, and then pushes its way inside. The bulging vine stretches your insides and pumps in and out of you, and it feels incredible. </p>The vines hold you tight, thrusting and throbbing in your rear, and all the while they fill your mouth with that wonderful, intoxicating goo. Your resistance is crumbling away – this might be your last chance to fight back, or else you might find yourself stuck here in this lustful state forever! Do you submit?',
			    "repeatable": true,
			    "effect": function(player) {
			    	player.confirmStep = {
		    			yes: 'This feels too good. Too incredible. You want more, you can\'t possibly imagine giving up this amazing sensation. You just go limp and revel in it, and the vines seem to recognise your submission. The one in your mouth pulses and fills you with even more wonderful warm goo, while the one in your rear slides deeper and deeper, filling you in a deep, hot part of yourself you never knew existed. </p>Your skin begins to feel stiff, swollen and strange. The colour of it changes, flushing with something odd as it takes on a shiny, orange tint. Your belly bulges and swells, inflating like a balloon, while the rest of you seems to draw inwards, shrinking and pressing into your expanding gut. Your arms and legs slide free of the tangling vines, only to press into your orange belly and melt away. Your head sinks down into your torso, and your flesh becomes hard and firm as it forms vertical ridges all along your huge round belly. </p>The pleasure becomes all you know, and all you can comprehend. Your vision and other senses fade as your face melts away into your belly, leaving only the thick vine sticking out from where your head once was. The rest of you is nothing more than a huge orange vegetable, a simple pumpkin in a patch of many other simple plants. No one would know that within this handsome pumpkin is a mind lost forever in endless pleasure.',
		    			no: 'You blink, shudder, and struggle to come to your senses. The vines sense your resistance, and the one in your ass doubles its pleasures, driving you to moans and shudders of incredible, bizarre lust. The moans, however, cause the vine in your mouth to loosen, and you are able to spit it out. </p>You wriggle and struggle, pulling free of the vines at your arms, and managing to sit up. You fight the urge to sit and bask in the lust of the vines, and instead grab the one between your legs and slowly drag it out of your rear. Then, you quickly clamber to your feet and run free from the bizarre plants.',
		    			yesEffect: function(player) {
		    				player.description = "There is a large and healthy pumpkin here. For some reason, it makes you think of a " + player.sex + ' ' + player.race + ' once called ' + player.name;
		    				player.trapped = "You are nothing more than a big orange pumpkin. You have no concept of anything but the sun on your leaves and the pleasure filling your mind."
		    			}
			    	};
			    }
			},{
			    "id": "Pumpk1",
			    "trapevent": true,
			    "moveMod": 0,
			    "description": 'Today you hear a sound that you have gotten used to happening every now and then. The mayor\'s crooning, sultry voice, and the stomping and crushing sound of your fellow pumpkins being turned into smashed-up goo under his feet. You can\'t see him, being unable to move from your position, but you hear him get closer and closer. </p>Then his shadow falls over you, and you see his shining eyes and massive grin above you. The older cat\'s large, soft feet are completely soaked with shiny wet pumpkin goo. "Well now, what a delightful plump specimen you are", he says. You feel the sole of his paw as he strokes it slowly along your firm rind. Smears of wet pumpkin drip along your bulbous body, the remains of former humans, turned into pumpkins and then into crushed goo by this powerful cat. </p>His claws slowly emerge, and trace along your body. It\'s ticklish, and you would squirm if you could, as he leaves thin lines cut into your side. His paw lifts high into the air, and you are sure that this is the end. You will soon be just a messy pile of goop for his amusement. But then he lowers it again, and winks at you. "Perhaps once you are a little larger, my friend", he says, and walks off to his next victim.',
			    "repeatable": true
			},{
			    "id": "Pumpk2",
			    "trapevent": true,
			    "moveMod": 0,
			    "description": 'Turner the horse is here today, walking through the pumpkins and feeling them for weight, size and firmness. You feel his hands stroke over your rind and tease your vine, but he leaves you behind in the soil. The pumpkin next to you is luckier, though. Turner nods in approval at that one, and picks him up with a mighty heave, before putting him on a little wagon. "Yer gonna be a mighty fine pumpkin pie", the big horse says. </p>You are incredibly jealous. Being eaten may have seemed terrifying when you were an adventurer, but as a pumpkin, you have grown to love the idea. For someone to chop you up, cook you into a delicious meal, and spread around your seeds is the greatest delight a pumpkin could hope for. You fantasise about all the different meals you could be used for all day.',
			    "repeatable": true
			},{
			    "id": "Pumpk3",
			    "trapevent": true,
			    "moveMod": 0,
			    "description": 'The soil doesn\'t feel as tasty and nutritious today as it usually does. Your vine feels dry and sticky, and you\'re not in a good pumpkin mood. That is, until you feel the ground under you tremble with the approach of someone big and heavy. You sit in the soil until a shadow falls over you. It\'s Grizz, the barkeeper. </p>You watch as he pulls his cock out from under his apron, and begins to pump and squeeze the thick, dark meat of his swollen bear cock. His foreskin slides up and down his glistening shaft, until he grunts and growls. With a pant, and a jerk of his cock, a messy splatter of cum slaps against your orange pumpkin body. Rope after rope of thick, creamy jizz splatters and lands all over you, then drips slowly down you into the dirt. You can do nothing but let it dribble. </p>Once it soaks into the soil, though, you feel revitalized and full of energy again. If you get the chance, you feel you should thank Grizz for the fertilizer.',
			    "repeatable": true
			},{
			    "id": "Pumpk4",
			    "trapevent": true,
			    "moveMod": 0,
			    "description": 'The sun is particularly warm and pleasant today. As a plant, the feeling of the sun is almost like energy flowing directly into your body. The pleasure of it tingles all along your rind, and makes your vine throb with delight.',
			    "repeatable": true
			}],
            items: []
        },
        {
            id: '13',
            title: 'The Two Feathers Inn',
            area: 'dormaus',
            light: true,
            content: 'The inn is a clean and homely building. The walls are white with fresh paint, and some effort has been put into decorating them with photographs and woodcuts. From the ceiling, a small but pretty chandelier is lighting the room.',
            outdoors: false,
			size: {value: '4'},
            exits: [
                {
                    cmd: 'south',
                    id: '7'
                },{
                    cmd: 'up',
                    id: '14'
                }
            ],
            playersInRoom: [],
            monsters: [],
            events: [{
			    "id": "Teddy Complaints",
			    "moveMod": 0,
			    "description": 'There is a strange sight in the inn today. Two short, cuddly teddy bears are arguing with the innkeeper with high-pitched adorable voices. The innkeeper, a fat and sleepy-looking owl, is barely paying attention to their complaints about "cursed toys" and "being full of fluff". It seems he is quite used to this sort of thing, because after a while the two bears start to trail off, and look at eachother with their big fake plush eyes. </p>They stroke and touch one another, and then begin to cuddle and squeeze together. Their stitched-on happy expressions seem a little more genuine as they become too cuddle-obsessed to keep complaining, allowing the owl to return to reading his book.',
			    "repeatable": true
			},{
			    "id": "Flute's Pitches",
			    "moveMod": 0,
			    "description": 'You see Flute at the inn, trying to convince the owl inkeeper to purchase one of his latest weird trinkets. He dangles a large bronze amulet. "This is an amulet of life saving! Restores you from any lethal fate!" he says eagerly. The owl looks at him with half-closed eyes. "It\'s an amulet of life preserving. It turns you into a mummy." he replies. Flute, ever undeterred, shows off a gleaming knife. "The blade of ages! Cuts anyone in half!" he says. </p>The owl yawns. "The blade of ages cuts anyone\'s AGE in half, and makes them younger. Which that isn\'t. It\'s a bread knife. From my own kitchen." he says. The fox tosses the knife aside with the same wide grin, and pulls a badge out from his tunic, with a large paw print on it. "Well what about this then? Took it from that weird old cave, definitely magical, probably makes you all stompy and powerful." he says. </p>The owl grins a bit as he sees it, and hooks one feather-finger into Flute\'s jerkin, pulling him around behind the counter. "Flute, that\'s the badge of paw slavery. Anyone holding one is marking themselves as an obedient paw slut." he says. Flute makes a startled noise, but his yelp turns to a moan of lust as he is lowered to the owl\'s clawed feet behind the counter. You see the proud fox try desperately to resist, but he\'s soon rubbing and massaging the owl\'s talons with a frustrated and confused erection. You wonder if you could get yourself one of those badges.',
			    "repeatable": true
			},{
			    "id": "Alakazam Visits",
			    "moveMod": 0,
			    "description": 'It looks like the inn has some entertainment on tonight. On a small stage next to the dining area, a white rabbit with a waistcoat and top hat is performing magic tricks. His black bowtie is quite cute on his furry neck. He makes cards disappear, and correctly guesses which fetish the volunteer is thinking of. Then, for the big finish, he gets an adventurer to come up on stage. With a wave of his wand, the human starts to growl and shudder. </p>Fur sprouts across his flesh, and his face pushes forward as he opens his mouth wide, showing off enormous razor-sharp white fangs. His body thickens with powerful, predatory muscles. His fingers flex as his nails turn to huge sharp claws, and then his hands thicken into furry orange paws. He falls to all fours, his armor snapping open under his growing bulk. He shakes it off, growling and snarling, and stretches his body as his bones reshape for quadrupedal movement. A long sinuous striped orange tail curves behind him, as the enormous new tiger prowls on the stage. The rabbit flourishes and bows, expecting clapping, but nothing happens. </p>He blinks and raises an eyebrow. "Er. This is considered very impressive where I am from..." he mutters. You suppose it\'s hard to impress people with transformation in a town where it happens around every corner. He finishes up his act, leaving behind the tiger, who makes a confused growl that you interpret to mean "Wait, are you leaving me like this?"',
			    "repeatable": true
			}],
            items: []
        },
        {
            id: '14',
            title: 'The Two Feathers Inn: Bedroom',
            area: 'dormaus',
            light: true,
            content: 'The beds at the Two Feathers are clean and well-made. It smells like fresh linen, and atop each bed is a small but clearly much-loved patchwork toy.',
            outdoors: false,
            exits: [
                {
                    cmd: 'down',
                    id: '13'
                }
            ],
			size: {value: '4'},
            playersInRoom: [],
            monsters: [],
            events: [{
			    "id": "The Shining",
			    "moveMod": 0,
			    "description": 'On the way to the room, you accidentally open the wrong door. It opens to reveal an older man in a business suit, who is getting a blowjob from a shorter, furry bear. The bear\'s muzzle is open wide, as he deep-throats the businessman\'s cock. </p>His thick black nose presses repeatedly into the human\'s fluffy pubes, until the businessman grunts and pants, unloading his cum deep into the bear\'s throat. The bear pulls free, letting the last few spurts spray over his nose and muzzle, where he lets it drip freely. They notice you looking, and turn to stare at you until you apologise and move away.',
			    "repeatable": true
			},{
			    "id": "Painting Theft",
			    "moveMod": 0,
			    "description": 'The hallway between the bedrooms has oil paintings between each door, lending a homely sort of air to the place. Some of them are a bit weird though. Who would paint an oil painting to look like a trapped fox pounding on glass trying to escape? You are examining the painting looking for an artists signature, when you see a skunk man turning the corner. He scoffs at one of the paintings, and swipes a knife across the fabric, ruining it. Then, he coughs, and shudders. </p>You see his body stumble forward like it\'s being pulled on strings. His hands fall against the painting, and then start to melt into it. His fur turning to paint and oil, his body flattening. It sucks him up to his arms, and he yelps and struggles to pull free. It is no use though. He\'s pulled up into the canvas and sucked up, leaving just his bushy tail dangling out for a moment before that slips inside too. </p>You walk over to the painting, to see an animated, two dimensional version of the skunk, pounding on the canvas from within and trying to escape. Until finally the paint dries, trapping him in still painted silence.',
			    "repeatable": true
			},{
			    "id": "Grizz's Dream",
			    "moveMod": 0,
			    "description": 'You open one of the hotel room doors and peek inside. The musky aroma of booze and sweat greets you, and you see the huge furry bulk of Grizz the bartender lying on the mattress and snoring. His weight is causing the bed to crease down the middle, and he\'s on top of the blankets, naked, with his enormously thick bear cock standing erect in his sleep. His gigantic footpaw kicks and twitches, and he murmurs something. It sounds a bit like "Drink it all, bitch." </p>His cock leaks pre, dribbling down the huge shaft and soaking the mattress. He starts to pant, his enormous round belly heaving as he mutters louder and shakes more. "Fuck. Yeah. Swim in my cum. You slut..." he growls. It seems the big bear has a bit of a fixation on his own cream! Whatever he\'s dreaming about, it clearly becomes too much for him to handle. His cock jerks and bounces in his sleep, and without him ever touching it, it spurts a thick jet of cum that arcs into the air and splatters all over his belly. </p>His shuddering calms as he starts to snore again. Judging by the stains and stickiness all over the carpet, you get the feeling that Grizz has quite a few nightly emissions. No wonder he prefers to rent a hotel room than clean it all up himself.',
			    "repeatable": true
			},{
			    "id": "A Lioness's Place for Men",
			    "moveMod": 0,
			    "description": 'As you stand in the hotel room, you begin to feel strange. Your body is flushed, and warm. Tingling sensations are flowing along your thighs, and your lion body feels warm and tingly. You have a desperate urge, an overpowering need to be filled. You start to pant, and your nostrils flare as you detect the source of your desire. A musk in the air, just a faint trace, but one which is hammering at your deepest desires like a jackhammer. You can smell a powerful, fertile male lion, and you are in the throes of some sort of strange heat.</p>You leave the room and follow the scent down the corridor, your heart pounding in your chest. With each step the musky, primal savannah scent of a horny lion gets stronger. You follow it up the stairs to the royal suite of the hotel, normally off-limits to all but the most prestigious of guests. You push open the double doors, and your mind swims at what you see. In front of you, lying on his back naked in a massive four poster bed, is your king, your lion. His handsome face is stretched into a gleaming fanged grin, and his arms are crossed behind his head, showing off his huge furry armpits. Surrounding him, at his feet and sides and curled on the floor, are many beautiful female lions, purring and gazing up at their ruler with adoration. He flexes his mighty paws, and you find yourself staring between his legs at the gigantic rock-hard lion cock standing erect in the air.</p>"I knew you would come, my lioness. Serve me", he commands. You feel like you should correct him, to say you are male. But it does not matter. Male, female...you are his lioness. You are helpless to resist. You move forward and kneel before his bed, taking hold of one of his mighty footpaws and beginning to stroke and massage it. He growls with pleasure and flexes his paw, then motions you to come up on the bed. Obediently, you climb up onto the plush and comfortable bed, but all you want to be on is your master\'s glorious body. He stares at you with his glowing amber eyes, as you stroke and kiss his legs, then rub your hands along the powerful muscles of his furry chest. He reaches up and strokes your chest, then pulls you down and rolls you over. His heavy, hot, musky body presses down onto you, as he growls with dominance and possessiveness.</p>"You are my lioness. My slave", he says, and you just nod, accepting your place. He teases and strokes your breasts, then begins to push forward, his mighty shaft stroking along your cock, as he frots and grinds against you. You exist to please him, to serve him. He cock rubs over your furry skin, oozes pre into your pelt, and every motion he makes causes you to forget your old life more and more. You want only to be with him forever, be one of his adoring harem slaves. He starts to thrust faster, his fanged maw slavering as his fur rises. Over and over he grinds against you, until his chest heaves with lust and his hands unleash claws that leave massive ragged tears in the bedsheets.</p>You hear his orgasm coming, as a rumbling, earthshaking roar building in his chest, until it crashes out from his muzzle. His enormous roar crushes your will, makes you know that you are beneath him, serving him, and when his cock unleashes its hot gift all over your chest, you accept it with bliss. Your master has used you and taken you, and blessed you with his cum. Your own body shudders and heats with pleasure, a heartpounding orgasm that washes away your thoughts and fills you with desire for your man. Your smaller cock erupts with your own seed, mingling with his. Slowly, you look up to him, and whimper something unintelligable, trying to express your love. He grins, his eyes gleaming...and rolls you over to fuck your ass, already hard and ready to go.</p>All night you are mounted and taken by your king, sent to heights of pleasure you have never felt before. By the morning, you are an exhausted mess, and he leaves you dripping with his cum on the bed as he gathers his slaves to go. Before he leaves, he kisses your neck and places something next to you. When you recover your senses enough to get up, you find a beautiful silver slave collar. Engraved on the front are the words "My Lioness"',
			    "repeatable": true,
			    "valid": function(player, command, fn) {
			    	if (player.race == "lion" && player.sex == "male" && player.domsub != "dominant") {
			    		return true;
			    	} else {
			    		return false;
			    	}
			    },
			    "effect": function(player) {
			    	Character.addItem(player, {
    					name: 'Harem Collar', 
    					displayName: 'Harem Collar',
    					short: 'harem collar',
    					long: 'There is a harem collar here, a beautiful silver slave collar with the words "My Lioness" on it' ,
    					area: 'dormaus',
    					affects: [],
    					id: 'haremcollar', 
    					level: 1,
    					itemType: 'armor',
    					material: 'silver', 
    					ac: 2,
    					value: 20,
    					weight: 1,
    					slot: 'head',
    					equipped: false,
    					onEquip: function(player) {
    						player.domsub = "submissive";
    					}
    				});
			    }
			},{
			    "id": "A Lioness's Place",
			    "moveMod": 0,
			    "description": 'As you stand in the hotel room, you begin to feel strange. Your body is flushed, and warm. Tingling sensations are flowing along your thighs, and your lioness pussy feels warm and wet. You have a desperate urge, an overpowering need to be filled. You start to pant, and your nostrils flare as you detect the source of your desire. A musk in the air, just a faint trace, but one which is hammering at your deepest desires like a jackhammer. You can smell a powerful, fertile male lion, and you are in the throes of heat.</p>You leave the room and follow the scent down the corridor, your heart pounding in your chest. With each step the musky, primal savannah scent of a horny lion gets stronger. You follow it up the stairs to the royal suite of the hotel, normally off-limits to all but the most prestigious of guests. You push open the double doors, and your mind swims at what you see. In front of you, lying on his back naked in a massive four poster bed, is your king, your lion. His handsome face is stretched into a gleaming fanged grin, and his arms are crossed behind his head, showing off his huge furry armpits. Surrounding him, at his feet and sides and curled on the floor, are many beautiful female lions, purring and gazing up at their ruler with adoration. He flexes his mighty paws, and you find yourself staring between his legs at the gigantic rock-hard lion cock standing erect in the air.</p>"I knew you would come, my lioness. Serve me", he commands. You are helpless to resist. You move forward and kneel before his bed, taking hold of one of his mighty footpaws and beginning to stroke and massage it. He growls with pleasure and flexes his paw, then motions you to come up on the bed. Obediently, you climb up onto the plush and comfortable bed, but all you want to be on is your master\'s glorious body. He stares at you with his glowing amber eyes, as you stroke and kiss his legs, then rub your hands along the powerful muscles of his furry chest. He reaches up and strokes your chest, then pulls you down and rolls you over. His heavy, hot, musky body presses down onto you, as he growls with dominance and possessiveness.</p>"You are my lioness. My slave", he says, and you just nod, accepting your place. He teases and strokes your breasts, then begins to push forward, his mighty shaft stroking along your labia, before plunging deep into your pussy. You gasp and moan, your insides so sensitive and so pleasured by the touch of your king\'s shaft. You exist to please him, to serve him. He thrusts and plunges into you, in and out, in and out, and every motion he makes causes you to forget your old life more and more. You want only to be with him forever, be one of his adoring harem girls. He starts to thrust faster, his fanged maw slavering as his fur rises. Over and over he pounds into you, until his chest heaves with lust and his hands unleash claws that leave massive ragged tears in the bedsheets.</p>You hear his orgasm coming, as a rumbling, earthshaking roar building in his chest, until it crashes out from his muzzle. His enormous roar crushes your will, makes you know that you are beneath him, serving him, and when his cock unleashes its hot gift deep into your pussy, you accept it with bliss. Your master has mounted you and taken you, and filled you with his cum. Your own body shudders and heats with pleasure, a heartpounding orgasm that washes away your thoughts and fills you with desire for your man. Slowly, you look up to him, and whimper something unintelligable, trying to express your love. He grins, his eyes gleaming...and starts to fuck you again, already hard and ready to go.</p>All night you are mounted and taken by your king, sent to heights of pleasure you have never felt before. By the morning, you are an exhausted mess, and he leaves you dripping with his cum on the bed as he gathers his slaves to go. Before he leaves, he kisses your breast and places something next to you. When you recover your senses enough to get up, you find a beautiful silver slave collar. Engraved on the front are the words "My Lioness"',
			    "repeatable": true,
			    "valid": function(player, command, fn) {
			    	if (player.race == "lion" && player.sex == "female") {
			    		return true;
			    	} else {
			    		return false;
			    	}
			    },
			    "effect": function(player) {
			    	Character.addItem(player, {
    					name: 'Harem Collar', 
    					displayName: 'Harem Collar',
    					short: 'harem collar',
    					long: 'There is a harem collar here, a beautiful silver slave collar with the words "My Lioness" on it' ,
    					area: 'dormaus',
    					affects: [],
    					id: 'haremcollar', 
    					level: 1,
    					itemType: 'armor',
    					material: 'silver', 
    					ac: 2,
    					value: 20,
    					weight: 1,
    					slot: 'head',
    					equipped: false,
    					onEquip: function(player) {
    						player.domsub = "submissive";
    					}
    				});
			    }
			},{
			    "id": "Plush1",
			    "trapevent": true,
			    "moveMod": 0,
			    "description": 'It\'s hard to think, as a plush toy. Your brain is full of fluff and all your thoughts keep drifting off to fantasies of hugging and being played with. The longer you spend in this soft, cute prison, the harder it is to hold on to your old thoughts and ideas.</p>You\'re not sure how much time passes, but a new guest rents out the room you\'re in. You can only see him when he walks past, since you\'re unable to move your head or even your shiny glass eyes. He is an otter, shorter than a human but with lean, firm swimmer\'s muscle visible under his shirt and shorts. He tosses his pack into a corner, and then jumps onto the bed next to you, before stretching and yawning.</p>The impact of him on the bed causes you to fall sideways, so you are able to see as he pulls off his shirt, revealing his bare chest, with its short, sleek fur and firm muscles. He kicks off his shorts and flexes his webbed feet, while his long and thick tail sways slightly. As he starts to push down his underwear, he notices you at last. "Oh! Looks like I have a roommate", he says, in a light-hearted tone.</p>He picks you up and holds you in his hand, your body dangling from his grip. You see his other hand reaching down and pushing his underwear off, and then you are being pushed down, your soft body pressing against his warm, throbbing shaft. You are helpless to resist as he starts to grind and thrust against you, his cock sliding up and down through your artificial fur. He rubs you against himself for a while, until his cock is leaking slick, shiny pre onto your body, and then he presses you down onto the mattress.</p>Your face is shoved into the sheets, and you feel the otter\'s body crushing down on you. His warm, firm chest squishing your cotton-stuffed body down, while he humps and fucks you. His cock grinding hard against your back, staining your cute fuzz with fluids. His panting getting louder, his sweat building up, until he shudders and moans. Hot, thick messy cum splatters all over your back, soaking your fabric and dripping from your sides. He then picks you up, still wet and dripping, and tosses you aside. You are dizzy and damp, your mind swimming with happiness at having made your guest happy.',
			    "repeatable": true
			},{
			    "id": "Plush Species",
			    "moveMod": 0,
			    "description": 'You are looking around the room when your eye is drawn to the patchwork toy on the bed. Now that you look closer, it seems a little out of place compared to the rest of the room. The colours are so different, and the button eyes are almost sinister in a way. Before you know it, you\'re sitting on the bed and lifting it up to look into its gleaming eyes. </p>It\'s surprisingly heavy. It\'s also strangely warm, and your body feels weak and soft the longer you hold it. Do you hold on?',
			    "repeatable": true,
			    "valid": function(player, command, fn) {
			    	if (player.race == "plush") {
			    		return false;
			    	} else {
			    		return true;
			    	}
			    },
			    "effect": function(player) {
			    	player.confirmStep = {
		    			yes: 'You can\'t seem to look away. Warmth and energy is flowing out from you, flowing into the strange toy. It grows bigger, and heavier, while you feel weak and soft. Your skin tingles as it starts to take on patches of colour all over. Purples, pinks, blues, greens. Your flesh is becoming soft, squishy artificial cotton. Your limbs are thickening, the insides filling with fluff. Your fingertips become fat and short, with stitches down the sides of your arms. Your head bulges out as it becomes a big round ball, with round teddy bear ears atop it. Then your mouth bulges into a rounded muzzle with a little plastic nose, and your eyes harden into gleaming glass spheres. </p>You struggle to resist, and finally toss aside the plush. You stumble backwards, shorter, and much fatter, now some sort of living patchwork plush teddy bear. The other toy stares at you, an evil grin on its face. It\'s now just as big as you are. </p>You can feel its power over you, and you shudder. If you explore in this place again, you know somehow that you will suffer an irreversible fate that may trap you here forever.',
		    			no: "You drop the toy quickly and back off, your limbs weak and your heart racing. The evil toy stares lifelessly at you, and you tell yourself you will never touch that thing again.",
		    			yesEffect: function(player) {
		    				player.description = "This adventurer is a short, chubby living teddy bear.";
		    				player.race = "plush";
		    				player.size.value = 2;
							player.size.display = 'small';
		    			}
			    	};
			    }
			},{
			    "id": "Plush TF",
			    "moveMod": 0,
			    "valid": function(player, command, fn) {
			    	if (player.race == "plush") {
			    		return true;
			    	} else {
			    		return false;
			    	}
			    },
			    "description": 'Foolishly, you linger in this cursed room, and of course you are quickly drawn to the enormous toy. You sit in front of it, shivering with anticipation, and look deeply into its lifeless black glass eyes. Your vision swims and your body feels weak. Moving your limbs is becoming an enormous effort, like they weigh a ton. You can only stare helplessly, and that means you can no longer escape your fate. </p>The other plush starts to move, reaching forward to grip your sides with its cotton mitten-like hands. You are shrinking more, as more energy flows out from you into the plush. Its patches start to merge together, the many colours blending into a dark brown, while its simplistic shape becomes tall, hulking and muscular. Mitten hands become huge clawed paws, and simple round head slowly becomes a leering, thuggish ursine face with a massive cruel grin. You are totally helpless and small in the bear\'s grip. </p>He stands up, stretching out his muscles and growling, before dropping you on the bed. You are tiny now, the size the original bear was, and you cannot move at all. You are nothing but a toy. The bear cracks his neck side to side, and speaks in a deep voice that is like rocks rumbling. </p>"Finally, I\'m real! Thanks for the soul, little fool. I\'m gonna love being alive!" He then walks off without giving you a second glance, and you can only lie there, helpless and cute.',
			    "repeatable": true,
			    "effect": function(player) {
			    	player.description = "There is a cute patchwork teddy bear here, sitting with a stitched-on smile on the pillows. The laundry tag, oddly, says " + player.name;
    				player.trapped = "You are a lifeless patchwork teddy bear, cute and smiling and filled with only fluff and stuffing. Your plastic eyes stare out into the room as you wait silently to be hugged."
			    }
			}],
            items: []
        },
        {
            id: '15',
            title: 'Church',
            area: 'dormaus',
            light: true,
            content: 'The church is quiet today. Rows of pews face towards an altar, behind which, a serene statue of a goddess looks with benevolence upon the congregation. The coloured glass in the windows causes the light here to shine ethereally.',
            outdoors: false,
            exits: [
                {
                    cmd: 'south',
                    id: '9'
                }
            ],
            playersInRoom: [],
            monsters: [{
				name: 'Bonacieux',
				level: 15,
				short: 'Bonacieux',
				long: ' Bonacieux the wolf priest is checking the candles on the altar.',
				description: ' Bonacieux is a tall and imposing grey wolf, with a small black moustache and goatee. He wears priest robes, and his bare paws stick out from the bottom of them.',
				inName: 'Bonacieux',
				race: 'wolf',
				id: 51,
				area: 'dormaus',
				weight: 245,
				diceNum: 2,
				diceSides: 8,
				diceMod: 5,
				meleeRes: 10,
				str: 16,
				position: 'standing',
				attackType: 'claw',
				damRoll: 10,
				hitRoll: 10,
				ac: 20,
				gold: 1000,
				merchant: false,
				wanderCheck: 38,
				itemType: 'mob',
				runOnAliveWhenEmpty: false,
				behaviors: [],
				topics: {
					paws: '"What an odd query, young one." The old grey wolf lifts one foot from under his robe, revealing a wide clawed paw with thick and powerful soles. "Perhaps you need to come to confession more often."',
		            name: '"I am Bonacieux, my child. I forgive you if you struggle to pronounce it."',
		            job: '"I service to the spiritual needs of my children in Dormaus. They come to me when they feel the need to atone for their sins. There are a lot of sins in this town."',
		            hobby: '"I have so little time for hobbies, I need to service the church, and tend to the needs of the people when they come to service me. Excuse me, I mean, to service their sins."',
		            wolf: '"We wolves are very mighty and dangerous beasts. We must always be aware of our power over the weaker creatures, and do what we can to respect them."',
		            cock: '"Now now, what a naughty young beast you are. I insist you come to confession at once and think long and hard about your behaviour."',
		            lore: '"Ah, it pleases me to know you are interested in the lore of this land. Once, long ago, there was nothing but blackness. Then, the great dragon weaved his threads together and there were the words. From these words, our entire world was created. At first, there were the primordial dragons, and they created us, the people of this world. Then, they slumbered. However, they say there remains a dark dragon of insects and bugs, who works to destroy the world to this day."',
		            bugs: '"The dark dragon lurks beneath the world, always working to destroy the land and return it to the dark nothingness. Whenever he does, the great dragon needs to bring the world back together again. They are engaged in an eternal struggle."',
		            great: '"The great dragon is responsible for recreating the world each time it falls apart. On the day he loses interest in fixing the world, we will all fall into nothing. They say the praise and attention of celestial otherworldly beings gives him the power he uses to keep the world together. He even changes the world, when we are not aware of it."',
		            changes: '"They say that the great dragon sometimes shifts the very land itself. Of course, we here on the world could not see it happening, for he freezes and changes time when he does it. Only an extraplanar being would even know things had changed."'
				}
			}],
            events: [{
			    "id": "Priest Gets His Goat",
			    "moveMod": 0,
			    "description": 'You hear a thumping and growling in the back of the church, and wander over to take a closer look. The confessional booth is rocking back and forth, and the growling and panting sounds are coming from within. You take a peek inside, and see a goat adventurer, his pants around his ankles and his furry rear shoved up against the side of the booth. On the other side, the wolf priest is ramming his cock into a hole between the pews, and slamming and thrusting forcefully into the blushing, bleating goat\'s ass. </p>The goat stammers something about being sorry for being such a bad bad guy, and the wolf priest growls with a feral, predatory intensity that makes you shiver. "Take my cock, you little slut, and I\'ll forgive you once your belly is full of cum." he says, his voice so rough and crude that you can\'t believe it\'s the same friendly, fatherly priest you met! The wolf snarls and drool oozes from his long fanged muzzle, and you can tell by the goat\'s bleats and shudders that his ass is being filled with hot cum. The goat starts to try to pull free, but Bonacieux rams forward so hard that he nearly crushes the wooden partition. </p>The goat yells in sudden pain as the wolf\'s swollen knot pops into his ass, tying him there until the priest is satisfied. His glowing yellow eyes turn to you, and he chuckles. "Shouldn\'t be spying, boy. You will need to be forgiven next!" he growls, and you back off, not sure if you\'re aroused or terrified.',
			    "repeatable": true
			},{
			    "id": "Priest Sermon",
			    "moveMod": 0,
			    "description": 'You decide to attend one of Bonacieux\'s sermons, curious what the people of this town think about religion. Animals of all races slowly filter into the church and take seats in the pews. It seems to mostly be visitors, but you notice a surprising amount declined to wear pants to this event. After a short delay, Bonacieux walks out and moves up to the lectern, where he starts to talk about peace, forgiveness, and the loving embrace of the goddess of magic. It\'s all fairly standard stuff, but every now and then, the priest grips the side of the lectern with one hand, and growls in a very deep, suggestive way. </p>Towards the end, he starts to pant and moan through every other sentence, and you notice some of the congregation are stroking themselves quietly at their seats. You get curious, and carefully move up to the front-most row of pews, at the edge where you can just barely see past the lectern that the wolf is reading from. It looks like the wolf isn\'t alone up there. Kneeling between his legs and mostly hidden by the lectern is a ram, who\'s mouth is stretched open by an enormous wolf cock, while the wolf thrusts constantly into his face. Every now and then, Bonacieux reaches down, grabs the ram\'s horns, and yanks him forward to get a better grip as he deep-throats the wooly male. </p>Bonacieux ends his lecture with a rousing speech about treating people like they deserve, during which he pulls his cock out from the sheep\'s face, and howls as he sprays his wool down with cum, leaving the dizzy ram coated in dripping, sticky wolf jizz. Bonacieux then shoves him into the lectern and slides a panel down to conceal him, before putting his cock away and helping people leave the church. While he\'s distracted, you walk over to the lectern, intending to ask the ram if he needs any help. When he hears you talking, however, he baas and grunts. "Get lost! I\'m not trading places as his fucktoy with you! I love it down here!" he says. You shrug, and leave him be.',
			    "repeatable": true
			},{
			    "id": "Priest's Cockring",
			    "moveMod": 0,
			    "description": 'You hear a crashing sound from the back rooms of the church, and a growl of what sounds like anger. You push open a door and enter, wanting to check if something is wrong. Inside, you discover a long, thin weasel, with his hand inside a cabinet filled with valuable plates and gold items. Bonacieux clearly discovered him mid-theft, because he just slammed the door closed on the weasel\'s hand. </p>"Stealing from here? Unforgivable. You need a special punishment, my son", he says in a deep and rumbling growl. He picks up the weasel as easily as if he were a toy, then sits down in a large armchair, before opening his robe and lowering the black pants he wears beneath it. His long cock stands firm and erect from his grey fur, and the weasel yelps as the wolf starts to squeeze and stretch him. The weasel\'s body seems to stretch out like taffee as Bonacieux pulls him wider and wider, making him grow thinner, yet also somehow shrink. Soon, the wolf is holding a pencil-thin weasel as long as his arm, who is still wriggling and squeaking desperately. He winks at you. </p>"A little trick I learned back in my younger days", he explains. He smooths his moustache for a moment, then starts to wrap the weasel around the base of his cock in a spiral. As he does, the weasel\'s struggling and wiggling becomes slower, as his body starts to stiffen and harden. His brown fur begins to twinkle and gleam, as his flesh becomes smooth and firm, then takes on the gleaming burnished colour of gold. When the wolf finishes wrapping him around, the weasel\'s head is simply a golden lump with the carved image of a shocked weasel on it. It makes a very shiny and attractive cock ring on the older lupine\'s shaft. </p>He closes his robe again, hiding the trapped criminal, and gives you a wink. "Such is the justice of the goddess" he says, and you remind yourself never to piss off the big wolf.',
			    "repeatable": true
			},{
			    "id": "Confession",
			    "moveMod": 0,
			    "description": 'In this serene and quiet environment, you do start to feel a little guilty for your behaviour. Surely it couldn\'t hurt to try confessing your fears and anxieties? You walk into the confessional booth, and sit down. </p>A few moments later, the wolf priest Bonacieux enters the other side and closes the door. You can see his face through the grill. His long, furry snout is streaked with grey in the black, and his fur sticks out at the sides and chin in the shape of a soft and fluffy moustache, contrasting with the thinner fur atop his head. </p>"What troubles you, my child?" he says, his voice deep, soothing and fatherly. As you begin to tell him your worries, he nods, strokes his chin, and lowers his hands somewhere. You don\'t notice his motions, too focused on your confession. Finally, you are done, and you look over to the wolf, expecting some form of advice. </p>Instead, a small panel in the wooden partition opens, one that you hadn\'t seen before, and your eyes open wide as a long, fat cock is pressed through it. The wolf grins at you, his massive teeth gleaming in the dark of the booth. </p>"It sounds like you have been very naughty indeed, my child. I insist you redeem yourself, by debasing yourself." His voice is so deep and dominant that you can barely stammer out a response before you find yourself getting down and taking hold of the wolf\'s heavy cock. </p>His scent is masculine and virile, and you close your eyes and take him into your mouth. Your head slides forwards and backwards on the priest\'s lupine cock, your jaw aching to contain the massive manhood. He growls and urges you on as you suck. </p>"Yes, my child." "Faster, young one." "That\'s it, keep going, obey me." It doesn\'t take long before his voice descends into rough, rumbling growls, until finally he thrusts forward and howls. His cum explodes into your throat, so thick and so much that it spills from your cheeks and lips. </p>Once you have swallowed as much as you can, he slowly pulls away, and smiles at you. </p>"You are forgiven, my child. Return for confession whenever you wish.',
			    "repeatable": true
			}],
			size: {value: '4'},
            items: []
        },{
        	id: '17',
        	title: 'Contributor Gallery',
        	area: 'dormaus',
        	light: true,
        	content: 'This strange art gallery seems to extend forever. Here, the walls of reality are thin. Paintings line the walls, shimmering almost as if you can enter them. The worlds beyond belong only to their creators. To the south is a painting of two contrasts, a beautiful angel and vile demon.',
        	size: {value: '4'},
            items: [],
            outdoors: false,
            exits: [
                {
                    cmd: 'west',
                    id: '16'
                },{
                    cmd: 'south',
                    id: '18'
                }
            ],
            playersInRoom: [],
            monsters: [],
            events: []
        },{
        	id: '18',
        	title: 'The Chamber',
        	area: 'dormaus',
        	light: true,
        	content: 'This room is much different from the rest of the caves. The walls are red and emit an unnatural warmth. In the center of the room is a brazier with a massive supernatural flame burning out from. The strangeness of the flame stems from how it\'s left half has the appearance of purple tendrils of fire, and the right a light yellow burning extremely bright, the two creating weird dancing shadows in the chamber. Lastly, on the left of the brazier is a throne made of twisting black metal with engravings of screaming faces of pain and terror that almost seem like they are moving in the dancing shadows. And on the right is a simple golden bench with a blue suede cushion on the top. [All content in this room came from CJMPinger]',
        	size: {value: '4'},
            items: [],
            outdoors: false,
            exits: [
                {
                    cmd: 'north',
                    id: '17'
                }
            ],
            playersInRoom: [],
            events: [{
			    "id": "Masterwipe F",
			    "moveMod": 0,
			    "description": 'While in the chamber you notice that Inarius is not on his bench as normal. Then from behind you, you hear his echoing voice say, "Young one, you appear to have let yourself become the property of another..." You turn around quickly but do not see him and then turn back to see him right in front of you, extremely close to your face. "Your mind is clouded by a dark magic that attempts to rule your thoughts and actions, I can remove it if that is what you desire." You are about to argue with him because why would you ever defy your master, but his hand grabs your face and you feel your mind become less clouded. "This is temporary young one, and I await for your answer." Do you accept his offer? ',
			    "repeatable": true,
			    "valid": function(player, command, fn) {
			    	if (player.sex == "female" && player.master) {
			    		return true;
			    	} else {
			    		return false;
			    	}
			    },
			    "effect": function(player) {
			    	player.confirmStep = {
		    			yes: 'You say yes, wanting to be freed from the force that oppresses your mind. You can feel that influence flowing back in, like a darkness that obcures your own thoughts and free will. The angel upon hearing your pleas responds calmly with a kind smile, "Relax young one, you shall be free." </p>Inarius leads you to the bench and softly pushes you on it. He then lowers himself, his robe lifted up and his knees on the ground. You then see him draw a circle in front of your crotch, a blue circle appearing in the air from where his finger was. Once the circle was complete, it exploded into bright light and then disappeared. In it\'s place a new feeling of sensitivity rose within you, centered directly inside and around your vagina.</p></p>You look down at him and notice something odd, in his eyes gleam a hunger that was not there before. On his face is a deep red blush and he is licking his lips as if before him is a meal. He takes a deep breath and then blows onto your clit. The warm air rushing past it feels greater due to the sensitivity, making it quiver out of pure pleasure alone.</p></p>Seeing you squirm from just that alone spurns him on further, and so he moves his head down towards your pussy. His lips touch the outside, the warmth of them feeling like a raging fire and the merest touch of them bringing you to the brink. His lips open and slowly he slides his tongue in, it massaging along the sides tenderly, reaching further until he gets the reaction he wishes from you to find your sweet spot. All of this overwhelming your senses and mind, you are unable to perceive anything other than the overload of sense and need for release radiating around your nether region.</p></p>He keeps moving his tongue around slowly and carefully. Before he even starts in earnest, your pussy shudders and releases its first orgasm into the angel\'s mouth. The feeling of the orgasm resonating through your body. But almost instantly after the release, the need for release returns even stronger than before. Even the sensitivity increases its intensity, all while the angel laps up all you have to offer before he continues his work</p></p>He starts going faster, spurned on even further by the orgasm. His tongue prods all over your clitoris. For several minutes this goes on non-stop. Countless orgasm after orgasm, each one faster than the last until it becomes one continuous streak. All while the sensitivity goes to it\'s peak. You can barely move save for the shudders from each ejaculation.</p></p>He stops moving and looks up at you, cum driping out of the sides of the voracious angel\'s mouth. Finally, he let\'s your hole free of his mouth as he slides his tongue out, the cool air giving a very different sensation from the warmth and wetness from before. He then proceeds to give the clit a single light tap of his finger. The feeling forcing you to grip on the chair, cumming long and hard. Streams of black liquid flow out and fall upon the ground, dissolving and disappearing with a fizzle before it can pool upon the ground.</p></p>You sit there tired an exhausted on the bench, your body weak from what has transpired. But you feel good. Your mind finally feels free, there\'s nothing in the back menacingly waiting to take over again. You are free. That thought stays awhile in your mind as you lay there. After a few hours of resting, your strength returns and you get up, thanking Inarius immensely for his service. Though as you go, you get the odd feeling that the angel didn\'t have to do it that way. ',
		    			no: 'Even with their influence no longer permeating your mind, you are still loyal to them. They are your master and no other, you don\'t need free will or to make decisions as your master can do them for you. You out right refuse the angel\'s offer and he goes to sit back down on his bench, as slight disappointment plays across his face. It doesn\'t take long for your master\'s influence to go back into your mind, and so you are truly happy.',
		    			yesEffect: function(player) {
		    				player.master = null;
		    			}
			    	};
			    }
			},{
			    "id": "Masterwipe M",
			    "moveMod": 0,
			    "description": 'While in the chamber you notice that Inarius is not on his bench as normal. Then from behind you, you hear his echoing voice say, "Young one, you appear to have let yourself become the property of another..." You turn around quickly but do not see him and then turn back to see him right in front of you, extremely close to your face. "Your mind is clouded by a dark magic that attempts to rule your thoughts and actions, I can remove it if that is what you desire." You are about to argue with him because why would you ever defy your master, but his hand grabs your face and you feel your mind become less clouded. "This is temporary young one, and I await for your answer." Do you accept his offer? ',
			    "repeatable": true,
			    "valid": function(player, command, fn) {
			    	if (player.sex == "male" && player.master) {
			    		return true;
			    	} else {
			    		return false;
			    	}
			    },
			    "effect": function(player) {
			    	player.confirmStep = {
		    			yes: 'You say yes, wanting to be freed from the force that oppresses your mind. You can feel that influence flowing back in, like a darkness that obcures your own thoughts and free will. The angel upon hearing your pleas responds calmly with a kind smile, "Relax young one, you shall be free." </p></p>Inarius leads you to the bench and softly pushes you on it. He then lowers himself, his robe lifted up and his knees on the ground. You then see him draw a circle in front of your crotch, a blue circle appearing in the air from where his finger was. Once the circle was complete, it exploded into bright light and then disappeared. In it\'s place a new feeling of sensitivity rose within you, centered directly upon your dick.</p></p>You look down at him and notice something odd, in his eyes gleam a hunger that was not there before. On his face is a deep red blush and he is licking his lips as if before him is a meal. He takes a deep breath and then blows onto your quickly hardening dick. The warm air rushing past it feels greater due to the sensitivity, making it quiver out of pure pleasure alone.</p></p>Seeing you squirm from just that alone spurns him on further, and so he moves his head down towards your dick. His lips touch the tip, the warmth of them feeling like a raging fire and the merest touch of them bringing you to the brink. His lips open and slowly he slides his mouth onto it, the tongue massaging along the underside tenderly. All of this overwhelming your senses and mind, you are unable to perceive anything other than the overload of sense and need for release radiating around your nether region.</p></p>He keeps moving his head down slowly and carefully, sliding his mouth further along. Before he even reaches the halfway point, your dick shudders and releases its first intense load. The feeling of the orgasm resonating through your body. But almost instantly after the release, the need for release returns even stronger than before. Even the sensitivity increases its intensity, all while the angel swallows the whole load and continues his work.</p></p>He starts going faster, spurned on even further by the orgasm. His mouth moves from the the tip all the way to the base. For several minutes this goes on non-stop. Countless orgasm after orgasm, each one faster than the last until it becomes one continuous streak. All while the sensitivity goes to it\'s peak. You can barely move save for the shudders from each ejaculation.</p></p>He stops moving and looks up at you, cum driping out of the sides of the voracious angel\'s mouth. Finally, he let\'s your cock free of his mouth as it slides off his tongue, the cool air giving a very different sensation from the warmth and wetness from before. He then proceeds to give it a single light flick of his finger. Then it spasms wildly, the feeling forcing you to grip on the chair, cumming long and hard. Ropes of black liquid fly out and fall upon the ground, dissolving and disappearing with a fizzle before it can pool upon the ground.</p></p>You sit there tired an exhausted on the bench, your body weak from what has transpired. But you feel good. Your mind finally feels free, there\'s nothing in the back menacingly waiting to take over again. You are free. That thought stays awhile in your mind as you lay there. After a few hours of resting, your strength returns and you get up, thanking Inarius immensely for his service. Though as you go, you get the odd feeling that the angel didn\'t have to do it that way.',
		    			no: 'Even with their influence no longer permeating your mind, you are still loyal to them. They are your master and no other, you don\'t need free will or to make decisions as your master can do them for you. You out right refuse the angel\'s offer and he goes to sit back down on his bench, as slight disappointment plays across his face. It doesn\'t take long for your master\'s influence to go back into your mind, and so you are truly happy.',
		    			yesEffect: function(player) {
		    				player.master = null;
		    			}
			    	};
			    }
			},{
			    "id": "Imp TF",
			    "moveMod": 0,
			    "description": 'Asmodeus stands walking towards you, his commanding presence further filling the room. The burning scent of sulphur fills your nostrils, slowly going from a scent that brings about disgust to one that brings about arousal. As he moves toward you, you find it near impossible to avert your gaze from between his legs. His demonic cock growing rapidly to nearly double of the size from before. He finally stands before you, his erection at full mast in front of you. You then hear a single guttural command ressonates through your being, "Suck." Do you obey the monster before you?',
			    "repeatable": true,
			    "valid": function(player, command, fn) {
			    	if (player.race == "imp") {
			    		return false;
			    	} else {
			    		return true;
			    	}
			    },
			    "effect": function(player) {
			    	player.confirmStep = {
		    			yes: 'You ignore the feeling of wrongness that starts to bubble up within you and let his influence command your being. You feel yourself almost lose all control; your mind, body, and soul putty in his hands. Not entirely of your own accord, you mouth the word yes and lose yourself to his depravity.</p></p>You feel the clawed hand of the demon start gripping onto the back of your head, pushing you closer to that throbbing penis before you. Your lips pressed against the head and his scent overwhelming your nostrils. You open your mouth wide, barely enough to take in the girth before you as he thrusts in roughly, filling your mouth completely.</p></p>With the exception of the first thrust, he starts off surprisingly gentle. He moves slowly, letting every inch of the underside of his cock slide across your tongue. His precum smearing onto your tongue, sparking an immense need for more. A need to pleasure the demon and get your reward.</p></p>He starts getting a little faster, a little deeper, a little rougher. Bit by bit he slides further in. If you could look up, you would see a sly smile on his face, but you are far too obsessed with what\'s in your mouth as he continues humping. His speed and force begins to increase rapidly, all while pushing your head against him.</p></p>Soon his large cock is all the way down your throat, pumping in and out, ignoring any gag reflex you still may have. He continues thrusting, even harder than before, the rest of your face crushed against his groin. All pretense of care and gentleness are gone as the demon nears his climax. You feel as if your jaw and neck could break at any point, but they never do.</p></p>Then he bucks one last time forcefully into your mouth, and relaxes. Spurt after spurt of seed begins to flow out into your mouth, unable to swallow and helpless to contain it all no matter how much you try. Little streams of glowing red cum flow out of your mouth, hot enough to sear a person normally but leaves you unharmed. The demon then slides his cock out of your mouth, spraying a few weaker spurts on you, and goes back to sit on his chair to watch the coming changes.</p></p>You begin to feel extremely cold, despite all the burning seed inside and around you. It feels almost feezing, your body shivering with cold. And then you see a small ball of light exit out of your chest, and float toward the demon, his maw open wide. You try and weakly move your arm out and catch it, but you can barely move at all. It finally floats in and lands on the demons tongue, and with a single gulp it disappears, with one last shiver resonating through your body.</p></p>Then everything becomes hot, burning and searing throughout body, your screams echoing in the cave. Promptly this is followed by silence as your body begins to shift in size. All pain gone, you start to feel strong. Your mind and body becoming twisted. You stand with a smile, your skin already turning a deep shade of red.</p></p>You smile at your predicament, you know that you are changing quickly. You know that you are changing into something better, no... far superior, than everything else except Master Asmodeus. Tiny nubs of horns begin to grow out of your skull. All while you begin to become shorter.</p></p>From above your ass a long tail rapidly flings out, tipped with an almost Arrowhead like shape. From your back tiny wings unfurl, too tiny to allow you to fly more than a couple feet off the ground. Additionally you shrink more and more til you are less than a meter tall.</p></p>In the end, in the place you once stood, remains a pudgy little imp. Your belly slightly chubby and arms and legs lanky, all you do is grin  as your inflated ego tells you that you are better than everyone else. Except for Master Asmodeus, of course.',
		    			no: 'You shake your head, something feels wrong. Every fiber of your being tells you that this is bad and you should run, but the meat in front of your eyes gets even closer, dispelling those fears through lust. Then with a flash of light you find yourself farther away from the demon, as if you were teleported back. The light in front of you blinds your vision, making you unable to see what is transpiring. An echoing voice yells, "Remember the compromise! They must agree, or I am entitled to destroy you at the cost of my own life!" Then you hear an almost whining grumble and then silence. As your vision returns, you notice that the room and occupants look the same as before, except that Asmodeus has a grimace of anger and pain on his face.',
		    			yesEffect: function(player) {
		    				player.description = "This adventurer is a short pudgy imp with bat wings and a demon tail.";
		    				player.race = "imp";
		    				player.size.value = 2;
		    				player.master = "Asmodeus";
		    			}
			    	};
			    }
			}],
            monsters: [{
				name: 'Asmodeus',
				level: 15,
				short: 'Asmodeus',
				long: 'Asmodeus, an infernal demon, is lounging on his throne lazily, one hand stroking his large member.',
				description: 'Asmodeus is a towering demon with glowing red lines running down his muscular dark blue body. On his head are large, ornate, curved horns ending with small purple flames on the tips, and his arms and legs end in sharp beastial claws.',
				inName: 'Asmodeus',
				race: 'demon',
				id: 51,
				area: 'dormaus',
				weight: 245,
				diceNum: 2,
				diceSides: 8,
				diceMod: 5,
				meleeRes: 10,
				str: 16,
				position: 'standing',
				attackType: 'claw',
				damRoll: 10,
				hitRoll: 10,
				ac: 20,
				gold: 1000,
				merchant: true,
				wanderCheck: 38,
				itemType: 'mob',
				runOnAliveWhenEmpty: false,
				behaviors: [],
				items: [],
				topics: {
					name: '"I will forgive your rudeness for not immediately going on your knees to worship my cock, for now, and introduce myself to one as misinformed of their position as you. I am Asmodeus, Prince of Lust. Your rightful place is down on the ground before me."',
				    job: '"My "job" as you put it is to rule over over all of this realm. You mortals will soon learn to worship me and submit to the depravity with their souls, or perish." You then hear him growl deeply and angrily, "But for now I am stuck in this pitiful cave because of him..."',
				    bye: '"Then begone from my sight mortal! But know that I can see within you, and my influence is undeniable."',
				    cock: 'He stands up from his throne, his height towering above you. He walks up to you and the entire girth and length of his cock hangs in front of you. His sulphuric musk burns in your nostrils, but in only a few seconds it begins to starts to smell good. You then move your head in to lick the head, but he then swiftly turns around and goes back to his chair. He then stares at you with a smirk, "You will worship it in time."',
					paws: 'He lifts one leg up with a devious smile, showing the dark padded underfoot of his clawed feet. He the lowers it again and says smugly, "I quite enjoy those who take being beneath my feet literally, it\'s so refreshing."',
					demons: '"We are the sins you commit. The darkness born within your hearts. The corruption that grows within the ranks of man. And we will rule you all."',
					corruption: '"Haven\'t you seen it all around you? Every dick you\'ve sucked, every cock in your ass, every man and woman in these lands have it." His tone shifts from boasting to anger, "If I could leave this forsaken cave, every lesser being around me would be my loyal slave."'
				}
			},{
				name: 'Inarius',
				level: 15,
				short: 'Inarius',
				long: 'Inarius, a celestial angel, is bent over in prayer on his bench, silent words reaching up to the heavens.',
				description: 'Inarius is a winged angel. Most of his body looks like an average sized, beautiful human male with golden blond hair. Draped on his body is a flowing white robe, modestly hiding the physique and assets below. Out of his back are gold feathered wings, spreaded out and gloriously emanating golden light from him into the room.',
				inName: 'Inarius',
				race: 'angel',
				id: 51,
				area: 'dormaus',
				weight: 245,
				diceNum: 2,
				diceSides: 8,
				diceMod: 5,
				meleeRes: 10,
				str: 16,
				position: 'standing',
				attackType: 'illuminate',
				damRoll: 10,
				hitRoll: 10,
				ac: 20,
				gold: 1000,
				merchant: true,
				wanderCheck: 38,
				itemType: 'mob',
				runOnAliveWhenEmpty: false,
				behaviors: [],
				items: [],
				topics: {
					name: 'In a soft but reverberant voice he responds, "Young one, I am Inarius, archangel of love and protection." Then with a kind nod he says, "Pleased to make your acquaintance."',
				    job: '"My purpose, given to me by those above, is to keep the demon next to me from spreading his corruption of lust and debauchery, and to prevent him from gaining Dominion of these lands. And so I have confined us both to this cave." You decide to not mention that in the outside world, lust and debauchery is the norm.',
				    bye: '"Far thee well young one, and leave with my blessings for I will surely pray for your future safety."',
				    cock: 'His face goes from a kind smile to a frown of pity and sadness and simply says, "I will pray for your Salvation young one."',
					paws: '"I am unaware of why you would ever want to see my feet but I will oblige, Young one." He then lifts his robe enough to allow is foot to be visible and lifts it for you to see. It is clean and smooth, as fair as the rest of the angel. He then lowers his foot back down and looks at you quizzically for a few seconds, clearly confused by the request.',
					young: 'You ask the angel why he keeps calling you young one and he responds simply, "All beings are young compared to the infinity of the Angels."',
					angels: '"We are an eternal group of benevolent beings. Most never come down to the realms of mankind, as our true purpose is to protect you from those that would exploit or destroy you all, and not to interfere directly in the machinations of man." (Following can be cut if purify is  cut) He pauses for a second, thinking, then carefully he says, "We are also known for our abilities to purify mortals of their sins, and teach it to those who still have some good in their heart."'
				}
			}]
        },{
            id: '16',
            title: 'Art Store',
            area: 'dormaus',
            light: true,
            content: 'This deliriously messy shop is full of intriguing statues and paintings. Danis, the human artist, is mixing a large pot of orange paint, sparkles flying out every few moments. On a pedestal in the doorway, a perfect, erect, marble cock and adjoining balls is sitting in a glass case. The plaque below it reads: “The most beautiful toy the artist ever held is now preserved for all to enjoy.”',
            outdoors: false,
            exits: [
                {
                    cmd: 'north',
                    id: '7'
                },
                {
                	cmd: 'east',
                	id: '17'
                }
            ],
            playersInRoom: [],
            monsters: [],
            events: [{
			    "id": "My Lioness: Male No TG",
			    "moveMod": 0,
			    "description": 'The art store is stacked with paintings, amongst which are sculptures and all sorts of hanging artwork. It\'s a feast of every type of art, but one of the patrons isn\'t paying any attention to the artwork. You gradually notice a tall and handsome lion who\'s gaze keeps turning to you, regardless of where you walk in the shop. Eventually you end up standing next to him, and take a moment to look closer.</p>He\'s wide, and tall, around seven feet tall, with powerful muscles under a thick and musky covering of golden fur. His head is surrounded by a wild and powerful mane, and atop it he wears a golden crown. The loincloth around his crotch is the only clothing he\'s wearing apart from that, and in this proximity, the heat and musk of his powerful predatory body makes you feel a little lightheaded. You ask him which of the art pieces he likes, and he replies in a deep and powerful purring voice. </p>"The most beautiful thing here I am looking at right now", he says to you, his amber eyes gleaming. Your face feels hot with an intense blush, and you realise that his large hands are stroking along your sides. His muscular chest is inches from your face, and you can barely think with the scent of wild savannah manliness washing over you. Do you let this continue?',
			    "repeatable": true,
			    "valid": function(player, command, fn) {
			    	var genderlocked = Character.hasEquipped(player, "genderlock");
			    	if (player.sex == "male" && player.race != "lion" && genderlocked && player.domsub != "dominant") {
			    		return true;
			    	} else {
			    		return false;
			    	}
			    },
			    "effect": function(player) {
			    	player.confirmStep = {
		    			yes: 'The attention and touches from this mighty lion feels so good. His powerful body holding you and pulling you close makes you feel owned and controlled. He presses you firmly to his chest, and you hear his thundering heartbeat, and the rumbling of his purring and pleasure. He looks down at you, then adjusts your face up, before leaning down and kissing you with his massive muzzle and wide, long tongue.</p>You simply let him take control, feeling like property in his grasp as he kisses you while stroking his padded fingers all along your body. He squeezes and presses around your waist, which becomes thinner and more feminine. His hands grope and stroke your chest, which becomes lithe and furry, almost as if your very body is changing to please his desires. You feel soft golden fur, growing into his palms so that he can tease and stroke it, and you shudder with submissive delight.</p>His hands continue to stroke you, this time moving to your crotch, where they rub and tease your rock-hard erection. Your skin is tingling where he has touched you, and the tingles are joined by more shiny golden fur which grows in a soft pelt over your body. Your face becomes smooth and streamlined as your nose and mouth push forward into a small, cute muzzle, while your ears become round and furry. The gorgeous lion strokes your head, then slides his hand down your back and squeezes you close to him while he continues to toy with your erection in his other hand.</p>You stroke your own hands through your lion\'s thick and mighty chest fur, loving the warmth and firmness of his amazing body. You are entranced by him, totally subservient to him. He\'s everything you could possibly want, and you have never felt so attracted to anyone. Your cock feels like it has never been harder, even as it seems to become smaller and somehow more submissive in his grasp. "You are my beautiful toy", he says.</p>"I am yours...", you find yourself saying, and as you do, you gasp in a higher-pitched voice, and feel his fingers pushing slowly inside you, one and then the other sliding into your tight and sensitive tailhole.</p>Your lion licks his powerful tongue along your cheek and neck, before stepping back. You almost fall to your knees right there, but he stops you. "When you are ready to accept your place by my side, come to my hotel room", he says, and walks off with a wink. You stare as he goes, wanting nothing more than to obey his every whim for the rest of your life. Even after he is gone, his scent lingers, and you shudder with arousal.',
		    			no: 'You shake your head to try to push away some of your dizziness, and hold your breath as you back away from the enormous cat. He looks at you with jealous and possessive eyes as you hide behind some paintings, then duck out when you can.',
		    			yesEffect: function(player) {
		    				player.description = "This adventurer is a slim male lion with a short mane and a feminine figure.";
		    				player.race = "lion";
		    				Character.resize(player, 4);
		    			}
			    	};
			    }
			},{
			    "id": "My Lioness: Male",
			    "moveMod": 0,
			    "description": 'The art store is stacked with paintings, amongst which are sculptures and all sorts of hanging artwork. It\'s a feast of every type of art, but one of the patrons isn\'t paying any attention to the artwork. You gradually notice a tall and handsome lion who\'s gaze keeps turning to you, regardless of where you walk in the shop. Eventually you end up standing next to him, and take a moment to look closer.</p>He\'s wide, and tall, around seven feet tall, with powerful muscles under a thick and musky covering of golden fur. His head is surrounded by a wild and powerful mane, and atop it he wears a golden crown. The loincloth around his crotch is the only clothing he\'s wearing apart from that, and in this proximity, the heat and musk of his powerful predatory body makes you feel a little lightheaded. You ask him which of the art pieces he likes, and he replies in a deep and powerful purring voice. </p>"The most beautiful thing here I am looking at right now", he says to you, his amber eyes gleaming. Your face feels hot with an intense blush, and you realise that his large hands are stroking along your sides. His muscular chest is inches from your face, and you can barely think with the scent of wild savannah manliness washing over you. Do you let this continue?',
			    "repeatable": true,
			    "valid": function(player, command, fn) {
			    	var genderlocked = Character.hasEquipped(player, "genderlock");
			    	if (player.sex == "male" && player.race != "lion" && !genderlocked && player.domsub != "dominant") {
			    		return true;
			    	} else {
			    		return false;
			    	}
			    },
			    "effect": function(player) {
			    	player.confirmStep = {
		    			yes: 'The attention and touches from this mighty lion feels so good. His powerful body holding you and pulling you close makes you feel owned and controlled. He presses you firmly to his chest, and you hear his thundering heartbeat, and the rumbling of his purring and pleasure. He looks down at you, then adjusts your face up, before leaning down and kissing you with his massive muzzle and wide, long tongue. </p>You simply let him take control, feeling like property in his grasp as he kisses you while stroking his padded fingers all along your body. He squeezes and presses around your waist, which becomes thinner and more feminine. His hands grope and stroke your chest, which bulges out into his grip, almost as if your very body is changing to please his desires. You feel breasts, your breasts, growing into his palms so that he can tease and squeeze them, and you shudder with submissive delight.</p>His hands continue to stroke you, this time moving to your crotch, where they rub and tease your rock-hard erection. Your skin is tingling where he has touched you, and the tingles are joined by shiny golden fur which grows in a soft pelt over your body. Your face becomes smooth and streamlined as your nose and mouth push forward into a dainty feline muzzle, while your ears become round and furry. The gorgeous lion strokes your head, then slides his hand down your back and squeezes you close to him while he continues to toy with your erection in his other hand.</p>You stroke your own hands through your lion\'s thick and mighty chest fur, loving the warmth and firmness of his amazing body. You are entranced by him, totally subservient to him. He\'s everything you could possibly want, and you have never felt so attracted to anyone. Your cock shrinks bit by bit in his hand, as he rubs the manhood out of you. As your cock becomes just a tiny nub against his finger, his rumbling voice whispers in your ear. "You are my lioness", he says.</p>"I am your lioness...", you find yourself repeating, and as you do, you gasp in a feminine voice, and feel his fingers pushing slowly inside you, into the tight and warm new lioness pussy that your lion has created between your legs.</p>Your lion licks his powerful tongue along your cheek and neck, before stepping back. You almost fall to your knees right there, but he stops you. "When you are ready to accept your place by my side, come to my hotel room", he says, and walks off with a wink. You stare as he goes, wanting nothing more than to obey his every whim for the rest of your life. Even after he is gone, his scent lingers, and you shudder with arousal.',
		    			no: 'You shake your head to try to push away some of your dizziness, and hold your breath as you back away from the enormous cat. He looks at you with jealous and possessive eyes as you hide behind some paintings, then duck out when you can.',
		    			yesEffect: function(player) {
		    				player.description = "This adventurer is a beautiful and sultry golden lioness.";
		    				player.race = "lion";
		    				player.sex = "female";
		    				Character.resize(player, 4);
		    			}
			    	};
			    }
			},{
			    "id": "My Lioness: Female",
			    "moveMod": 0,
			    "description": 'The art store is stacked with paintings, amongst which are sculptures and all sorts of hanging artwork. It\'s a feast of every type of art, but one of the patrons isn\'t paying any attention to the artwork. You gradually notice a tall and handsome lion who\'s gaze keeps turning to you, regardless of where you walk in the shop. Eventually you end up standing next to him, and take a moment to look closer.</p>He\'s wide, and tall, around seven feet tall, with powerful muscles under a thick and musky covering of golden fur. His head is surrounded by a wild and powerful mane, and atop it he wears a golden crown. The loincloth around his crotch is the only clothing he\'s wearing apart from that, and in this proximity, the heat and musk of his powerful predatory body makes you feel a little lightheaded. You ask him which of the art pieces he likes, and he replies in a deep and powerful purring voice. </p>"The most beautiful thing here I am looking at right now", he says to you, his amber eyes gleaming. Your face feels hot with an intense blush, and you realise that his large hands are stroking along your sides. His muscular chest is inches from your face, and you can barely think with the scent of wild savannah manliness washing over you. Do you let this continue?',
			    "repeatable": true,
			    "valid": function(player, command, fn) {
			    	var genderlocked = Character.hasEquipped(player, "genderlock");
			    	if (player.sex == "female" && player.race != "lion" && !genderlocked && player.domsub != "dominant") {
			    		return true;
			    	} else {
			    		return false;
			    	}
			    },
			    "effect": function(player) {
			    	player.confirmStep = {
		    			yes: 'The attention and touches from this mighty lion feels so good. His powerful body holding you and pulling you close makes you feel owned and controlled. He presses you firmly to his chest, and you hear his thundering heartbeat, and the rumbling of his purring and pleasure. He looks down at you, then adjusts your face up, before leaning down and kissing you with his massive muzzle and wide, long tongue. </p>You simply let him take control, feeling like property in his grasp as he kisses you while stroking his padded fingers all along your body. He squeezes and presses around your waist, appreciating your feminine hips and adjusting them slightly to his liking. His hands grope and stroke your chest, which bulges out into his grip, almost as if your very body is changing to please his desires. You feel your breasts growing into his palms so that he can tease and squeeze them easier, and you shudder with submissive delight.</p>His hands continue to stroke you, this time moving to your thighs, where they rub and tease and gently caress your body. Your skin is tingling where he has touched you, and the tingles are joined by shiny golden fur which grows in a soft pelt over you. Your face becomes smooth and streamlined as your nose and mouth push forward into a lioness\'s muzzle, while your ears become round and furry. The gorgeous lion strokes your head, then slides his hand down your back and squeezes you close to him.</p>You stroke your own hands through your lion\'s thick and mighty chest fur, loving the warmth and firmness of his amazing body. You are entranced by him, totally subservient to him. He\'s everything you could possibly want, and you have never felt so attracted to anyone. Your mind is crumbling as he squeezes you, his very touch breaking down your will. As you struggle to focus on your mind, his rumbling voice whispers in your ear. "You are my lioness", he says.</p>"I am your lioness...", you find yourself repeating, and as you do, you gasp and moan, and feel his fingers pushing slowly inside you, into your warm and tight pussy that craves your master\'s every touch.</p>Your lion licks his powerful tongue along your cheek and neck, before stepping back. You almost fall to your knees right there, but he stops you. "When you are ready to accept your place by my side, come to my hotel room", he says, and walks off with a wink. You stare as he goes, wanting nothing more than to obey his every whim for the rest of your life. Even after he is gone, his scent lingers, and you shudder with arousal.',
		    			no: 'You shake your head to try to push away some of your dizziness, and hold your breath as you back away from the enormous cat. He looks at you with jealous and possessive eyes as you hide behind some paintings, then duck out when you can.',
		    			yesEffect: function(player) {
		    				player.description = "This adventurer is a beautiful and sultry golden lioness.";
		    				player.race = "lion";
		    				Character.resize(player, 4);
		    			}
			    	};
			    }
			},{
			    "id": "Gypsy Statue",
			    "moveMod": 0,
			    "description": 'There are many people visiting the shop today, including one adventurer who has his eye on a particular statue in the corner. The statue is of an exotic nude gypsy, baring her breasts with her hands in the air, clinking finger cymbals with joy. The adventurer stands in front of the statue for a long time, entranced by the woman\'s beauty. Her aura is hypnotic to the adventurer, and eventually, he tilts his head and starts drooling at the sight of her. </p>Without thinking, he begins to stroke his cock through his pants. He pulls it free, and ignoring everyone else in the shop, he starts to jerk off furiously. "I want her," he says, over and over. "I want her." Soon, he cums thick white spurts all over the base of the statue. The adventurer is in too much bliss to notice, but the cum seems to absorb into the statue\'s rock. </p>While the adventurer is still in bliss, his clothes start changing. His armor and threads shrink and change form, slowly giving him a thin veil to cover his face, sandals to keep his feet soft, and tin coins to hang around his waist and jingle with every step. The adventurer doesn\'t realize his body is changing either. Soon, his hair grows down his back and changes to dark brown in color. His chest balloons out, giving him nice perky breasts that hold themselves up with ease. Soon, his cock begins to shrink as well, and his balls recede into his body, getting smaller and smaller, unable to grip. The adventurer is left with delicious labia, and he strokes the skin lightly in pleasure.</p>The adventurer from before no longer exists, only a soft, exotic dancer, strikingly similar to the statue. The gypsy arouses from her stupor, and raising finger cymbals, begins to dance to her own imaginary song. She walks out of the shop at heads to the town plaza, her bottom jiggling and shaking, the coins clinking on her hips. You make a note to yourself to be careful where your cum lands… (Guest event by ThrowawayKink)',
			    "repeatable": true
			},{
			    "id": "Orc Painting",
			    "moveMod": 0,
			    "description": 'A messy looking man is taking off his clothes in front of Danis the artist. He poses nude, seemingly trying to make a few extra coins as a model. Danis has already painted a pastoral scene, and now it looks as if he is going to add this model to it. </p>"Come look," Danis says to you, so you watch him paint the man into the landscape. Danis makes quick work, and soon, the spitting image of the naked man is there in the photo. You look back at the man for a comparison, but he is gone! You look at the painting and see the man, but he is now moving around, exploring the painting. You see him jerking off his painted cock, enjoying the view. </p>But Danis isn\'t done with the painting. While the man looks out at the landscape, Danis quickly paints another figure in the foreground. After a few minutes, an orc joins the painting, his erect, green cock dripping cum in lust. The man notices the orc as he animates, looking for something or someone to fuck. Danis picks up the painting and hangs it on the wall, and you see the man running from the orc. You know that sooner or later, the orc will get what he wants, and probably more than once…  (Guest event by ThrowawayKink)',
			    "repeatable": true
			},{
			    "id": "Cock Clay",
			    "moveMod": 0,
			    "description": 'Danis the artist is working hard on an intricate clay vase. "It\'s for the mayor," he says. "I want to make sure it looks fitting enough to be displayed in his office. I\'m trying to use the best materials."</p>While explaining this to you, Danis\' hand slips and messes up the shape of his spinning creation, turning the nearly perfect vase into a crappy pile of clay. "Oh, clump!" Danis, while not happy, starts to gather all of the splattered clay back on his pottery wheel. "I\'ll have to get some more clay if I want to start over correctly."</p>He walks over to one of his closets, and when he opens it, a frightened, naked adventurer is revealed. The adventurer looks as if he has chunks taken out of him, and as Danis reaches inside the closet, you understand why. The adventurer is already missing part of his cock and one of his balls, but now Danis takes the rest of his junk, grabbing and peeling it off as it the adventurer is made out of soft clay. He winces in pleasure as Danis continues to squish the chunk of the adventurer. Shutting the closet, he walks back over to the lump of clay he was molding and adds the new clay to it. Danis resumes his creation of a beautiful pot, and soft moans are heard from the closet. (Guest event by ThrowawayKink) ',
			    "repeatable": true
			},{
			    "id": "Dragon statue: Male",
			    "moveMod": 0,
			    "valid": function(player, command, fn) {
			    	if (player.sex == "male") {
			    		return true;
			    	} else {
			    		return false;
			    	}
			    },
			    "description": 'You walk around the store, admiring all the materials that Danis has concocted to make his art. Various vials and containers are full of paints and powders, as well as shelves full of other things, like yarn, rocks, and clay. You spend a lot of time thinking about what you could make with all the materials, wandering around the store as you do. </p>"Ever wondered how I do it all?" Danis smiles through his thick, black glasses. "I went to art school with a wizard who specialized in creative magic. I don\'t have all the powers that he did, but I did learn a lot about materials. Like this powder, for instance." Danis picks up a container of white powder labeled ‘setting.\' "Would you like to see how it works?"</p>Do you agree to a demonstration of Danis the artist\'s materials? Afterwards, you might not be able to roam as freely… (Guest event by ThrowawayKink)',
			    "repeatable": true,
			    "effect": function(player) {
			    	player.confirmStep = {
		    			yes: 'You nod your head at Danis, and he smiles wide. "Good deal! As you can see by my art, I am quite inspired by the naked form, so it would probably be good if you took any clothing off, yes?" You take whatever covering you have on and shuck it to the side. </p>"Now, I want you to pretend you are a huge dragon, just about to huff a large flame towards the town." You listen to Danis\' commands, raising your hands above your head and making a scary face. You stick your pelvis out and lean back, really trying to get a good breath of air before you breathe out fire on the imaginary town. You hold this pose, and Danis gets to work, taking a fluffy brush and using it to powder every inch of your body. As the powder hits each part of your body, you feel it tense up, holding its position. You realize you aren\'t able to any part of your body that is powdered, but continue to trust Danis, even after everything but your cock has been frozen. </p>"Now, my powdery friend," Danis says. "Time for the art to begin." </p>Danis pulls a slab of clay from one of his cupboards and places it next to your body. You watch as he pulls clumps of clay and starts coating your body, encasing you in heavy, wet rock. He works from your feet up your leg, giving a thick coat of clay to everything. He pounds it in, up your back and around your stomach, spending extra time covering every part of your ass and groin. He leaves your cock bare, however, and it begins to become erect as you are covered. Eventually, he has covered everything, even your entire head, though your eyes remain uncoated. You are now a giant blob of clay, so Danis get to work on creating art. </p>It takes a few days, but slowly Danis picks away at the soft clay. You do not know what he is making, but every piece he picks away feels like he is tickling you. It keeps your cock hard, and every once in a while, Danis tugs at it to let you know you are doing a good job. One day, Danis is pleased with his work, and he pulls you into a kiln. You are baked for hours, and though the temperature is scorching and it feels as if you are sweating profusely, it is making you ache with pleasure.</p>When you are taken out of the kiln, Danis let you cool for a little bit, and then shows you what you\'ve become. You are now a statue of a ferocious dragon. Your eyes scan the mirror, admiring your new form. Danis smiles and pulls at your cock, and pleasure runs through your body. "I\'ve always wanted to make an interactive exhibit! You\'ll be perfect." He places you in the middle of the store, and he hangs a sign by you that says, "Please Keep The Statue On Edge. Thank You." You wait for customers to come through the store to admire you, hoping they will stop to jerk your cock and give you pleasure. ',
		    			no: 'You shake your head at Danis, and he puts the container away. "Consider buying one of my many miniature statues one day, yes?" ',
		    			yesEffect: function(player) {
		    				player.description = "There is a mighty dragon statue here, the cock hard and needy. It was once a " + player.sex + ' ' + player.race + ' called ' + player.name;
		    				player.trapped = 'You are trapped as a mighty dragon statue, permanently on the edge of an orgasm that will never come. Passers-by stroke and tease you, and you are helpless but to accept their touches.'
		    			}
			    	};
			    }
			},{
			    "id": "Dragon statue: Female",
			    "moveMod": 0,
			    "valid": function(player, command, fn) {
			    	if (player.sex == "female") {
			    		return true;
			    	} else {
			    		return false;
			    	}
			    },
			    "description": 'You walk around the store, admiring all the materials that Danis has concocted to make his art. Various vials and containers are full of paints and powders, as well as shelves full of other things, like yarn, rocks, and clay. You spend a lot of time thinking about what you could make with all the materials, wandering around the store as you do. </p>"Ever wondered how I do it all?" Danis smiles through his thick, black glasses. "I went to art school with a wizard who specialized in creative magic. I don\'t have all the powers that he did, but I did learn a lot about materials. Like this powder, for instance." Danis picks up a container of white powder labeled ‘setting.\' "Would you like to see how it works?"</p>Do you agree to a demonstration of Danis the artist\'s materials? Afterwards, you might not be able to roam as freely… (Guest event by ThrowawayKink)',
			    "repeatable": true,
			    "effect": function(player) {
			    	player.confirmStep = {
		    			yes: 'You nod your head at Danis, and he smiles wide. "Good deal! As you can see by my art, I am quite inspired by the naked form, so it would probably be good if you took any clothing off, yes?" You take whatever covering you have on and shuck it to the side. </p>"Now, I want you to pretend you are a huge dragon, just about to huff a large flame towards the town." You listen to Danis\' commands, raising your hands above your head and making a scary face. You stick your pelvis out and lean back, really trying to get a good breath of air before you breathe out fire on the imaginary town. You hold this pose, and Danis gets to work, taking a fluffy brush and using it to powder every inch of your body. As the powder hits each part of your body, you feel it tense up, holding its position. You realize you aren\'t able to any part of your body that is powdered, but continue to trust Danis, even after everything but your crotch been frozen. </p>"Now, my powdery friend," Danis says. "Time for the art to begin." </p>Danis pulls a slab of clay from one of his cupboards and places it next to your body. You watch as he pulls clumps of clay and starts coating your body, encasing you in heavy, wet rock. He works from your feet up your leg, giving a thick coat of clay to everything. He pounds it in, up your back and around your stomach, spending extra time covering every part of your ass and groin. He leaves your pussy bare, however, and it throbs with strange heat as you are covered. Eventually, he has covered everything, even your entire head, though your eyes remain uncoated. You are now a giant blob of clay, so Danis get to work on creating art. </p>It takes a few days, but slowly Danis picks away at the soft clay. You do not know what he is making, but every piece he picks away feels like he is tickling you. It keeps your pussy wet and horny, and every once in a while, Danis slides his finger into you to let you know you are doing a good job. One day, Danis is pleased with his work, and he pulls you into a kiln. You are baked for hours, and though the temperature is scorching and it feels as if you are sweating profusely, it is making you ache with pleasure.</p>When you are taken out of the kiln, Danis let you cool for a little bit, and then shows you what you\'ve become. You are now a statue of a ferocious dragon. Your eyes scan the mirror, admiring your new form. Danis smiles and teases your labia, and pleasure runs through your body. "I\'ve always wanted to make an interactive exhibit! You\'ll be perfect." He places you in the middle of the store, and he hangs a sign by you that says, "Please Pleasure The Statue. Thank You." You wait for customers to come through the store to admire you, hoping they will stop to fill you and pump your pussy for their own amusement. ',
		    			no: 'You shake your head at Danis, and he puts the container away. "Consider buying one of my many miniature statues one day, yes?" ',
		    			yesEffect: function(player) {
		    				player.description = "There is a mighty dragon statue here, with a soft and warm pussy. It was once a " + player.sex + ' ' + player.race + ' called ' + player.name;
		    				player.trapped = 'You are trapped as a mighty dragon statue, permanently on the edge of an orgasm that will never come. Passers-by stroke and tease you, and you are helpless but to accept their touches.'
		    			}
			    	};
			    }
			},{
			    "id": "My Lion Pet: Male",
			    "moveMod": 0,
			    "description": 'You are admiring the art, when you smell something interesting on the air. It\'s a strangely musky and appealing aroma, one that makes your muscles tingle and your body heat up a little. You look around, wandering through the paintings and statues like a hunter, seeking out your prey.</p>You discover the source of the scent, a nervous-looking lion man. He\'s thin and lithe, his mane short and unimpressive, and he\'s blushing nervously as he looks around, with his tail between his legs. The scent is coming from him. You approach, getting more aroused the closer you get to him. The smell makes you want to mount, to dominate, to control. He looks up at you and sees your flushed, panting face, and stammers. "I...was looking for someone here. A big lion guy, I needed him to change me back. Can you help me?", he says. You certainly feel like you could \'help\' him. Do you want to take this prize for yourself?',
			    "repeatable": true,
			    "valid": function(player, command, fn) {
			    	if (player.sex == "male" && player.race != "lion" && player.domsub == "dominant") {
			    		return true;
			    	} else {
			    		return false;
			    	}
			    },
			    "effect": function(player) {
			    	player.confirmStep = {
		    			yes: 'You push the lion up against the wall, and he yelps in surprise. "Wh-what are you doing?!", he cries out, but you just reply with a needy, eager snarl. You grab his leather pants, and shove them down, revealing the source of that arousing smell. Between the lion-man\'s legs is a tight virgin pussy. It seems he was part-way into turning into a lioness before he escaped. Judging by the redness on his face, he is very embarrassed about his predicament. But you know how to make him glad for what he has.</p>Ignoring his struggles and protests, you start to grind your cock against his pussy. Your throbbing length teases his folds, and he starts to sweat and shake, the scent of heat growing. Unable to resist any longer, you shove your cock forward, deep inside him. The feeling of taking this prey for yourself, taking his virginity, drives you wild with lust. All of the lion\'s resistance drops away once he feels a real man\'s cock inside him, stroking along his sensitive walls. He roars and shudders, pressing against you as he starts to kiss and nuzzle you. His eyes open wide and look up at you with adoration and awe. </p>You keep fucking and pounding him, every thrust changing you little by little. Your muscles are bulging and growing, your body is getting taller and stronger. Your skin, flushed and red from your pleasure, sprouts golden shaggy fur all over, covering you in a heavy and musky coat of golden fur.</p>As you change, you start to feel more dominant and confident. You are king of these weaker, smaller beasts. They should be serving you, obeying you! You open your mouth as it stretches out into a fanged muzzle, and your hair begins to grow out thick and lustrous all around your head. Soon, you are the proud owner of a massive lion\'s mane, making you look even larger and stronger than before. You flex your feet against the floor, as they grow huge, muscular and clawed. Mighty lion\'s paws to make it easier to shove inferior creatures underfoot where they belong. Finally, the pleasure of your enormous regal body becomes too much, and you bellow a mighty roar, unloading your cum deep into the smaller lion\'s tight crevice. </p>He whimpers under you, shuddering with his own pleasure, and you suddenly lunge forward and bite his neck, claiming him as your own. You hold him there until you are fully spent, every drop of your fertile cum inside him. It is likely that the little lion man will be bearing your cubs in a few months. You pull free of him, and leave him be, sure that he will show up again to serve you soon.',
		    			no: 'You shake off the strange desires within you as best you can, and leave after muttering a dismissive response to the lion man.',
		    			yesEffect: function(player) {
		    				player.description = "This adventurer is a massive and musky lion, with huge muscles and a savage, cocky grin.";
		    				player.race = "lion";
		    				player.sex = "male";
		    				Character.resize(player, 4);
		    			}
			    	};
			    }
			},{
			    "id": "My Lion Pet: Female",
			    "moveMod": 0,
			    "description": 'You are admiring the art, when you smell something interesting on the air. It\'s a strangely musky and appealing aroma, one that makes your muscles tingle and your body heat up a little. You look around, wandering through the paintings and statues like a hunter, seeking out your prey.</p>You discover the source of the scent, a nervous-looking lion man. He\'s thin and lithe, his mane short and unimpressive, and he\'s blushing nervously as he looks around, with his tail between his legs. The scent is coming from him. You approach, getting more aroused the closer you get to him. The smell makes you want to mount, to dominate, to control. He looks up at you and sees your flushed, panting face, and stammers. "I...was looking for someone here. A big lion guy, I needed him to change me back. Can you help me?", he says. You certainly feel like you could \'help\' him. Do you want to take this prize for yourself?',
			    "repeatable": true,
			    "valid": function(player, command, fn) {
			    	var genderlocked = Character.hasEquipped(player, "genderlock");
			    	if (player.sex == "female" && !genderlocked && player.race != "lion" && player.domsub == "dominant") {
			    		return true;
			    	} else {
			    		return false;
			    	}
			    },
			    "effect": function(player) {
			    	player.confirmStep = {
		    			yes: 'You push the lion up against the wall, and he yelps in surprise. "Wh-what are you doing?!", he cries out, but you just reply with a needy, eager snarl. You grab his leather pants, and shove them down, revealing the source of that arousing smell. Between the lion-man\'s legs is a tight virgin pussy. It seems he was part-way into turning into a lioness before he escaped. Judging by the redness on his face, he is very embarrassed about his predicament. But you know how to make him glad for what he has.</p>Ignoring his struggles and protests, you start to grind against him. The scent coming from him is changing you, making you feel bigger and stronger. He craves a male, a lion to mount him and fill him with seed. A king to own his lioness! Your chest starts to flatten as muscles bulge all over you, and you growl and shudder as your clit begins to bulge out and throb. It stretches out, becoming a massive, manly shaft, before your pussy closes up and is replaced by a heavy pair of musky balls. Your throbbing length teases his folds, and he starts to sweat and shake, the scent of heat growing. Unable to resist any longer, you shove your cock forward, deep inside him. The feeling of taking this prey for yourself, taking his virginity, drives you wild with lust. All of the lion\'s resistance drops away once he feels a real man\'s cock inside him, stroking along his sensitive walls. He roars and shudders, pressing against you as he starts to kiss and nuzzle you. His eyes open wide and look up at you with adoration and awe. You keep fucking and pounding him, every thrust changing you little by little. Your muscles are bulging and growing, your body is getting taller and stronger. Your skin, flushed and red from your pleasure, sprouts golden shaggy fur all over, covering you in a heavy and musky coat of golden fur.</p>As you change, you start to feel more dominant and confident. You are king of these weaker, smaller beasts. They should be serving you, obeying you! You open your mouth as it stretches out into a fanged muzzle, and your hair begins to grow out thick and lustrous all around your head. Soon, you are the proud owner of a massive lion\'s mane, making you look even larger and stronger than before. You flex your feet against the floor, as they grow huge, muscular and clawed. Mighty lion\'s paws to make it easier to shove inferior creatures underfoot where they belong. Finally, the pleasure of your enormous regal body becomes too much, and you bellow a mighty roar, unloading your cum deep into the smaller lion\'s tight crevice. He whimpers under you, shuddering with his own pleasure, and you suddenly lunge forward and bite his neck, claiming him as your own. You hold him there until you are fully spent, every drop of your fertile cum inside him. It is likely that the little lion man will be bearing your cubs in a few months. You pull free of him, and leave him be, sure that he will show up again to serve you soon.',
		    			no: 'You shake off the strange desires within you as best you can, and leave after muttering a dismissive response to the lion man.',
		    			yesEffect: function(player) {
		    				player.description = "This adventurer is a massive and musky lion, with huge muscles and a savage, cocky grin.";
		    				player.race = "lion";
		    				player.sex = "male";
		    				Character.resize(player, 4);
		    			}
			    	};
			    }
			}],
			size: {value: '4'},
            items: []
        }
	]
};

