window.onload = function() {
	'use strict';
	var ws = io.connect('', {transports: ['websocket']}),
	terminal = document.getElementById('terminal'),
	node = document.getElementById('cmd'),
	rowCnt = 0,
	canSend = true,	
	aliases = {	
		n: 'move north',
		e: 'move east',
		w: 'move west',
		s: 'move south',
		u: 'move up',
		d: 'move down',
		north: 'move north',
		east: 'move east',
		west: 'move west',
		south: 'move south',
		up: 'move up',
		down: 'move down',
		fl: 'flee',
		fol: 'follow',
		uf: 'unfollow',
		gr: 'group',
		l: 'look',
		sca: 'scan',
		i: 'inventory',
		sc: 'score',
		o: 'open',
		op: 'open',
		stats: 'score',
		eq: 'equipment',
		equip: 'wear',
		we: 'wear',
		re: 'remove',
		q: 'quaff',
		c: 'cast',
		k: 'kill',
		adv: 'kill',
		attack: 'kill',
		murder: 'kill',
		re: 'rest',
		sl: 'sleep',
		h: 'help',
		wh: 'who',
		whe: 'where',
		af: 'affects',
		aff: 'affects',
		ooc: 'chat',
		shout: 'chat',
		sh: 'chat',
		slist: 'skills',
		skill: 'skills',
		desc: 'description',
		r: 'recall',
		wake: 'stand',
		g: 'get',
		tr: 'train',
		prac: 'practice',
		nod: 'emote nods solemly.',
		laugh: 'emote laughs heartily.',
		wo: 'worth',
		re: 'recall',
		gi: 'give',
		wield: 'wear',
		dr: 'drop'
	},
	isScrolledToBottom = false,
	playerIsLogged = null,
	display = function(r, addToDom) {
		var i = 0;
	
		if (addToDom) {
			terminal.innerHTML += '<div class="row">' + r.msg + '</div>';

			rowCnt += 1;

			if (rowCnt >= 160) {
				for (i; i < terminal.childNodes.length; i += 1) {
					terminal.removeChild(terminal.childNodes[i]);
				}

				rowCnt = 0;
			}

			isScrolledToBottom = terminal.scrollHeight - terminal.clientHeight <= terminal.scrollTop + 1;

			if (!isScrolledToBottom) {
				terminal.scrollTop = terminal.scrollHeight - terminal.clientHeight;
			}
		}

		return parseCmd(r);
	},
	parseCmd = function(r) {
		if (r.msg !== undefined) {
			r.msg = r.msg.replace(/ /g, ' ').trim();

			ws.emit(r.emit, r);
		}
	},
	checkAlias = function(cmdStr, fn) { 
		var keys = Object.keys(aliases),
		i = 0,
		cmd,
		msg,
		keyLength = keys.length,
		cmdArr = cmdStr.split(' ');

		cmd = cmdArr[0].toLowerCase();

		msg = cmdArr.slice(1).join(' ');

		for (i; i < keyLength; i += 1) {
			if (keys[i] === cmd) {
				if (msg === '') {
					return fn(aliases[keys[i]]);
				} else {
					return fn(aliases[keys[i]] + ' ' + msg);
				}
			}
		}

		return fn(cmd + ' ' + msg);
	};

	document.onclick = function() {
//		node.focus();
	};

	document.addEventListener('reqPassword', function(e) {
		e.preventDefault();
		
		node.type = 'password';
		node.placeholder = 'Login password';
	}, false);

	document.addEventListener('onLogged', function(e) {
		e.preventDefault();
		
		node.type = 'text';
		node.placeholder = 'Enter a Command -- type \'help commands\' for a list of basic commands';
	}, false);
	
	document.getElementById('colorbutton').onclick = function (e) {
		var mudwrapperElement = document.getElementById('mudwrapper');
		if (mudwrapperElement.className.indexOf('colorswitch') == -1) {
			mudwrapperElement.className += ' colorswitch';
		} else {
			mudwrapperElement.className = mudwrapperElement.className.replace('colorswitch', '');
		}
	};
	
	var submitFunction = function (e) {
		var messageNodes = [],
		msg = node.value.toLowerCase().trim(),
		capmsg = node.value.trim(),
		msgObj = {
			msg: checkAlias(msg, function(cmd) {
				return cmd;
			}),
			capmsg: capmsg,
			emit: 'cmd'
		};

		if (e) {
			e.preventDefault();
		}

		if (canSend) {
			display(msgObj);
			
			node.value = '';
//			node.focus();

			canSend = false;
			
			return false;
		} else {
			return false;
		}
	};

	document.getElementById('console').onsubmit = function (e) {submitFunction(e)};
	
	document.getElementById('chatconsole').onsubmit = function (e) {
		document.getElementById('cmd').value = "say " + document.getElementById('saycmd').value;
		document.getElementById('saycmd').value = "";
		submitFunction(e);
	};
	
	document.getElementById('northbutton').onclick = function (e) {
		document.getElementById("cmd").value = "north";
		submitFunction(e)
	};
	document.getElementById('southbutton').onclick = function (e) {
		document.getElementById("cmd").value = "south";
		submitFunction(e)
	};
	document.getElementById('westbutton').onclick = function (e) {
		document.getElementById("cmd").value = "west";
		submitFunction(e)
	};
	document.getElementById('eastbutton').onclick = function (e) {
		document.getElementById("cmd").value = "east";
		submitFunction(e)
	};
	document.getElementById('upbutton').onclick = function (e) {
		document.getElementById("cmd").value = "up";
		submitFunction(e)
	};
	document.getElementById('downbutton').onclick = function (e) {
		document.getElementById("cmd").value = "down";
		submitFunction(e)
	};
	
	document.getElementById('explorebutton').onclick = function (e) {
		document.getElementById("cmd").value = "explore";
		submitFunction(e)
	};
	document.getElementById('lookbutton').onclick = function (e) {
		document.getElementById("cmd").value = "look";
		submitFunction(e)
	};
	document.getElementById('scorebutton').onclick = function (e) {
		document.getElementById("cmd").value = "score";
		submitFunction(e)
	};
	document.getElementById('whobutton').onclick = function (e) {
		document.getElementById("cmd").value = "who";
		submitFunction(e)
	};
	document.getElementById('inventorybutton').onclick = function (e) {
		document.getElementById("cmd").value = "inventory";
		submitFunction(e)
	};
	document.getElementById('helpbutton').onclick = function (e) {
		document.getElementById("cmd").value = "help";
		submitFunction(e)
	};
	
	document.getElementById('yesbutton').onclick = function (e) {
		document.getElementById("cmd").value = "yes";
		submitFunction(e)
	};
	document.getElementById('nobutton').onclick = function (e) {
		document.getElementById("cmd").value = "no";
		submitFunction(e)
	};
	
	node.focus();
	
	ws.on('msg', function(r) {
		display(r, true);

		if (r.evt && !r.evt.data) {
			r.evt = new CustomEvent(r.evt);
			
			if (r.data) {
				r.evt.data = r.data;
			}

			document.dispatchEvent(r.evt);
		}
	});

	setInterval(function() {
		canSend = true;
	}, 175);
};
