'use strict';
var Cmd = require('../src/commands').cmd,
Room = require('../src/rooms').room,
World = require('../src/world').world;

/*
	Flute wanders around Dormaus and occassionally onto the main road.
*/
module.exports = { 
	exclimations: [
		'Time waits for no fox!',
		'I wonder if I could sell one of these?',
		'Trinkets for sale! Doodads and knicknacks!',
		'Always got goods for adventurers on the go!'
	],
	moveDirections: ['up'],
	wanderCheck: 3,
	onAlive: function(mob, roomObj) {
		var roll = World.dice.roll(1, 40);

		if (mob.position === 'standing') {
			if (roll === 5) {
				Cmd.emote(mob, {
					msg: 'stares <span class="grey">skyward</span> in thought.',
					roomObj: roomObj
				});
			} else if (roll === 1 && roomObj.playersInRoom.length) {
				// Most of the time we just proclaim something
//				Cmd.say(mob, {
//					msg: mob.exclimations[parseInt(Math.random() * ((this.exclimations.length)))],
//					roomObj: roomObj
//				});
			}
		}
	}
};
