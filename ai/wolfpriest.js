'use strict';
var Cmd = require('../src/commands').cmd,
Room = require('../src/rooms').room,
World = require('../src/world').world;

/*
	Bonacieux is a wolf priest with a secret dirty side
*/
module.exports = { 
	exclimations: [
		'Luck be with you, traveller.',
		'I hope you are well, young one.',
		'So much work to do, always...',
		'Come to confession whenever you seek release, my child.'
	],
	moveDirections: ['north', 'east', 'west', 'south'],
	wanderCheck: 3,
	onAlive: function(mob, roomObj) {
		var roll = World.dice.roll(1, 40);

		if (mob.position === 'standing') {
			if (roll === 5) {
				Cmd.emote(mob, {
					msg: 'hums a short mantra.',
					roomObj: roomObj
				});
			} else if (roll === 1 && roomObj.playersInRoom.length) {
				// Most of the time we just proclaim something
				Cmd.say(mob, {
					msg: mob.exclimations[parseInt(Math.random() * ((this.exclimations.length)))],
					roomObj: roomObj
				});
			}
		}
	}
};
